-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 12, 2016 at 08:10 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `demo`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addCategory`(IN `p_name` VARCHAR(255), IN `p_status` ENUM('1','0'), IN `p_parent_id` INT(3), IN `p_created_by` INT(3))
    NO SQL
BEGIN
INSERT INTO d_category
(
    name,
    status,
    parent_id,
    created_by
 )
values
(
    p_name,
    p_status,
    p_parent_id,
    p_created_by
); 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteCategory`(IN `p_id` INT(3))
    NO SQL
BEGIN
DELETE FROM d_category
WHERE id = p_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editCategory`(IN `p_name` VARCHAR(255), IN `p_status` ENUM('1','0'), IN `p_parent_id` INT(3), IN `p_created_by` INT(3), IN `p_id` INT(3))
    NO SQL
BEGIN
UPDATE d_category
SET
  name=p_name,
  status=p_status,
  parent_id=p_parent_id,
  created_by=p_created_by
WHERE id=p_id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `d_banners`
--

CREATE TABLE IF NOT EXISTS `d_banners` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `d_banners`
--

INSERT INTO `d_banners` (`id`, `name`, `status`, `image`) VALUES
(12, 'McBookAir', '1', 'MacBookAir-1140x3801.jpg'),
(13, 'iPhone', '1', 'iPhone6-1140x3801.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `d_category`
--

CREATE TABLE IF NOT EXISTS `d_category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(4) NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `d_category`
--

INSERT INTO `d_category` (`id`, `name`, `parent_id`, `created_by`, `created_date`, `status`) VALUES
(12, 'Sportswear', 0, 27, '2016-08-12 10:13:00', '1'),
(13, 'NIKE', 12, 27, '2016-08-12 10:13:34', '1'),
(14, 'ADIDAS', 12, 27, '2016-08-12 10:14:07', '1'),
(15, 'PUMA', 12, 27, '2016-08-12 10:14:47', '1'),
(16, 'MENS', 0, 27, '2016-08-12 10:15:12', '1'),
(18, 'GUCCI', 16, 27, '2016-08-12 10:15:48', '1'),
(19, 'CHANEL', 16, 27, '2016-08-12 10:16:41', '1'),
(20, 'DIOR', 16, 27, '2016-08-12 10:17:03', '1'),
(21, 'ARMANI', 16, 27, '2016-08-12 10:17:31', '1'),
(22, 'PRADA', 16, 27, '2016-08-12 10:18:01', '1'),
(23, 'WOMENS', 0, 27, '2016-08-12 10:18:49', '1'),
(24, 'FENDI', 23, 27, '2016-08-12 10:19:43', '1'),
(25, 'DIOR', 23, 27, '2016-08-12 10:20:10', '1'),
(26, 'KIDS', 0, 27, '2016-08-12 10:22:11', '1'),
(27, 'GUESS', 23, 27, '2016-08-12 10:22:55', '1'),
(28, 'CLOTHING', 0, 27, '2016-08-12 10:38:35', '1'),
(29, 'BAGS', 0, 27, '2016-08-12 10:38:51', '1'),
(30, 'SHOES', 0, 27, '2016-08-12 10:39:15', '');

-- --------------------------------------------------------

--
-- Table structure for table `d_configuration`
--

CREATE TABLE IF NOT EXISTS `d_configuration` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `d_configuration`
--

INSERT INTO `d_configuration` (`id`, `name`, `value`) VALUES
(2, 'pagelength', '5');

-- --------------------------------------------------------

--
-- Table structure for table `d_coupon`
--

CREATE TABLE IF NOT EXISTS `d_coupon` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `percent_off` float(12,2) NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_by` int(3) NOT NULL,
  `modify_date` datetime NOT NULL,
  `status` enum('1','0') NOT NULL,
  `no_of_uses` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `d_coupon`
--

INSERT INTO `d_coupon` (`id`, `code`, `percent_off`, `created_by`, `created_date`, `modify_by`, `modify_date`, `status`, `no_of_uses`) VALUES
(1, 'KFB9076G', 10.00, 27, '2016-08-11 10:57:08', 0, '0000-00-00 00:00:00', '0', 5),
(5, 'AFB976G', 50.00, 27, '2016-08-11 11:38:30', 0, '0000-00-00 00:00:00', '0', 5),
(6, 'GHIKL90', 10.67, 27, '2016-08-11 11:44:05', 0, '0000-00-00 00:00:00', '1', 6),
(7, 'AFGH98', 14.00, 27, '2016-08-11 11:49:28', 0, '0000-00-00 00:00:00', '0', 34);

-- --------------------------------------------------------

--
-- Table structure for table `d_coupons_used`
--

CREATE TABLE IF NOT EXISTS `d_coupons_used` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `user_id` int(3) NOT NULL,
  `order_id` int(3) NOT NULL,
  `created_date` date NOT NULL,
  `coupon_id` int(3) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_id` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `d_product`
--

CREATE TABLE IF NOT EXISTS `d_product` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sku` varchar(45) NOT NULL,
  `short_description` varchar(100) NOT NULL,
  `long_description` text NOT NULL,
  `price` float(14,2) NOT NULL,
  `special_price` float(14,2) NOT NULL,
  `speciat_price _from` date NOT NULL,
  `special_price_to` date NOT NULL,
  `status` enum('1','0') NOT NULL,
  `quantity` int(10) NOT NULL,
  `meta_title` varchar(10) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_by` int(3) NOT NULL,
  `modify_date` date NOT NULL,
  `is_featured` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `d_product`
--

INSERT INTO `d_product` (`id`, `name`, `sku`, `short_description`, `long_description`, `price`, `special_price`, `speciat_price _from`, `special_price_to`, `status`, `quantity`, `meta_title`, `meta_description`, `meta_keywords`, `created_by`, `created_date`, `modify_by`, `modify_date`, `is_featured`) VALUES
(25, 'Notes', '', '', '', 126.00, 0.00, '0000-00-00', '0000-00-00', '1', 0, '', '', '', 0, '2016-08-11 08:18:22', 0, '0000-00-00', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `d_product_attributes`
--

CREATE TABLE IF NOT EXISTS `d_product_attributes` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_by` int(3) NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `d_product_attributes_assoc`
--

CREATE TABLE IF NOT EXISTS `d_product_attributes_assoc` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `product_id` int(3) NOT NULL,
  `product_atrribute_id` int(3) NOT NULL,
  `product_atrribute_value_id` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `d_product_attribute_values`
--

CREATE TABLE IF NOT EXISTS `d_product_attribute_values` (
  `id` int(3) NOT NULL,
  `product_attribute_id` int(3) NOT NULL,
  `attribute_value` varchar(45) NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_by` int(3) NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `d_product_categories`
--

CREATE TABLE IF NOT EXISTS `d_product_categories` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `product_id` int(3) NOT NULL,
  `category_id` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id_3` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `d_product_images`
--

CREATE TABLE IF NOT EXISTS `d_product_images` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_by` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_by` int(3) NOT NULL,
  `modify_date` date NOT NULL,
  `product_id` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `d_product_images`
--

INSERT INTO `d_product_images` (`id`, `name`, `status`, `created_by`, `created_date`, `modify_by`, `modify_date`, `product_id`) VALUES
(24, 'blog_share_gplus3.jpg', '1', 27, '2016-08-11 08:18:23', 0, '0000-00-00', 25);

-- --------------------------------------------------------

--
-- Table structure for table `d_roles`
--

CREATE TABLE IF NOT EXISTS `d_roles` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `d_roles`
--

INSERT INTO `d_roles` (`id`, `name`) VALUES
(2, 'admin'),
(5, 'customer'),
(3, 'inventory manager'),
(4, 'order manager'),
(1, 'superadmin');

-- --------------------------------------------------------

--
-- Table structure for table `d_users`
--

CREATE TABLE IF NOT EXISTS `d_users` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` char(32) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `role_id` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `role_id_2` (`role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `d_users`
--

INSERT INTO `d_users` (`id`, `firstname`, `lastname`, `email`, `password`, `status`, `role_id`) VALUES
(27, 'kamal', 'siddiqui', 'kamal.siddiqui@wwindia.com', '25d55ad283aa400af464c76d713c07ad', '1', 2),
(30, 'Manish', 'singh', 'manish@wwindia.com', '25d55ad283aa400af464c76d713c07ad', '1', 3),
(31, 'Kamal', 'Siddiqui', 'mohammad@gmail.com', '25d55ad283aa400af464c76d713c07ad', '1', 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `d_coupons_used`
--
ALTER TABLE `d_coupons_used`
  ADD CONSTRAINT `d_coupons_used_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `d_coupon` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `d_product_attributes_assoc`
--
ALTER TABLE `d_product_attributes_assoc`
  ADD CONSTRAINT `d_product_attributes_assoc_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `d_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `d_product_categories`
--
ALTER TABLE `d_product_categories`
  ADD CONSTRAINT `d_product_categories_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `d_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `d_product_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `d_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `d_product_images`
--
ALTER TABLE `d_product_images`
  ADD CONSTRAINT `d_product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `d_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
