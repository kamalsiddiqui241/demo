<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->Model('Show');
        $this->load->library('session');
        $this->load->library('form_validation');
    }

    function index() {

        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric');

        if ($this->form_validation->run()) {
            $data = array();
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $result = $this->Show->login($data);
            if($result){
                redirect('welcome/index');
            }else{
                 $this->session->set_flashdata('message','Invalid email or password');
                redirect('login/index');
     
            }
        }
        $this->load->view('home_page');
    }

}
