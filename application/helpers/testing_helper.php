<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function pr($value) {
    echo "<pre>";
    print_r($value);
    exit;
}
function lastquery($value) {
    echo "<pre>";
    echo $this->db->last_query();
    exit;
}
function is_added($id){
    $is_added = FALSE; 
    $ci =& get_instance();
    $bag = $ci->cart->contents();
    foreach ($bag as $item) {
        if($item['id'] == $id){
            $is_added = TRUE;         
        } 
    }
    return $is_added;
}
function is_added_wishlist($id,$user_id){
       $is_added = FALSE; 
       $ci =& get_instance(); 
       if($id){
           $ci->db->select('product_id');
           $ci->db->where(array('product_id'=>$id,'user_id'=>$user_id));
           $query = $ci->db->get('d_users_wish_list');
           
           if($query->row()){
               $is_added = TRUE;
           }
       }
       return $is_added;
    }
  function change_date_format($format,$date){
//    echo mdate($datestring, $time);    
     echo  date($format,strtotime($date));
  }
