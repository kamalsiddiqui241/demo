<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['admin'] = 'admin/login';
$route['site'] = 'site/home';

$route['admin/(:any)'] = 'admin/$1';  // admin/login
$route['admin/(:any)/(:any)'] = 'admin/$1/$2';  // admin/login
//$route['(:any)'] = 'site/$1';                   //home
//$route['(:any)/(:any)'] = 'site/$1/$2';         //home/index

//$route['site/account']='site/product/my_account_listing';
$route['default_controller'] = 'site/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['site/address/add'] = 'site/user/add';
$route['site/invoice/(:any)'] = 'site/product/invoice/$1';
$route['site/category/(:any)'] = 'site/product/index/$1';
$route['site/user/addressbook']= 'site/user/index';
$route['site/address/edit/(:any)']='site/user/edit/$1';
$route['site/track_order'] = 'site/product/track_order';
$route['site/my_account'] = 'site/product/my_account_listing';
$route['site/my_orders'] = 'site/product/my_orders';
$route['site/change_password'] = 'site/product/change_password';
$route['site/wishlist'] = 'site/product/wishlist_listing';
$route['site/checkout'] = 'site/product/display_checkout';
$route['site/cart'] = 'site/product/display_cart';
$route['site/home'] = 'site/home/index';
$route['site/login'] ='site/login/index';
$route['site/contact_us'] = 'site/product/contact_us';
$route['site/cms/(:any)']  = 'site/cms/index/$1';