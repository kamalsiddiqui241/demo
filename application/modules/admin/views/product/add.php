<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Add Product
            <small>Adding Products</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                            echo validation_errors();
                        }
                        ?></div>
                        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data">    
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Name <span class="require">*</span></label>
                                    <input id="exampleInputRole" name="name" value="<?php echo set_value('name'); ?>" class="form-control" type="text" placeholder="Enter Name">
                                </div> 
                                <div class="form-group">
                                    <label>Product Price <span class="require">*</span></label>
                                    <input maxlength="10" id="exampleInputPrice" name="price" value="<?php echo set_value('price'); ?>" class="form-control" type="text" placeholder="Enter Price">
                                </div>
                                <div class="radio">
                                    <div><label class="control-label"><strong>Select Status*</strong></label></div>
                                    <div><label><input id="optionsRadios1" type="radio" checked="" value="1" name="optionsRadio">Active</label>
                                    <label><input id="optionsRadios1" type="radio"  value="0" name="optionsRadio">Inactive</label></div>
                                </div>
                                <div class="upload_fileds">
                                    <label>Upload Image <span class="require">*</span></label>
                                    <input id="uploadFileAdd" name="myimage" type="file" placeholder="Choose File" >
                                </div>
                                <div class="form-group">
                                    <label>Select Category <span class="require">*</span></label>
                                    <select class="form-control" id="residence"  name="parent" >
                                        <option value="">---Select Category---</option>
                                        <?php foreach ($categories as $cat) { ?>
                                            <option value="<?php echo $cat->id; ?>"<?php
                                            ?>><?php echo $cat->name ?></option>
                                                <?php } ?>
                                    </select>
                                </div>  
                                <div class="form-group">
                                    <label>Quantity <span class="require">*</span></label>
                                    <input id="exampleInputQuantity" maxlength="4" name="quantity" value="<?php echo set_value('quantity'); ?>" class="form-control" type="text" placeholder="Enter Quantity" max_length="4">
                                </div>
                                    <div class="form-group">
                                        <label>Description <span class="require">*</span></label>
                                        <textarea id="exampleInputDescription" name="description" class="form-control" type="text" placeholder="Enter Description"><?php echo set_value('description');?></textarea>
                                    </div> 
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label><input id="" type="checkbox" name="optionCheck" value="1">Is_featured</label>
                                        </div>
                                    </div>        
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/product'" type="button">Back</button>
                                </div>
                         </div>
                  </form>
                </div>
            </div>
        </div> 
    </section>     
</div>