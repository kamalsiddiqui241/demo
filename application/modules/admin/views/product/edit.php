<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Product
            <small>Edit Products</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Update Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                            echo validation_errors();
                        }
                        ?></div>
                        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data" >
                            <div class="box-body">  
                                <div class="form-group">
                                    <label>Name <span class="require">*</span></label>
                                    <input id="exampleInputName" name="name" class="form-control" value="<?php echo $productedit->name; ?>"  type="text" placeholder="Enter Product Name">
                                </div>
                                <div class="form-group">
                                    <label>Product Price <span class="require">*</span></label>
                                    <input maxlength="10" id="exampleInputPrice" name="price" value="<?php echo $productedit->price; ?>" class="form-control" type="text" placeholder="Enter Price">
                                </div>
                                <div class="radio">
                                    <div><label class="control-label"><strong>Select Status <span class="require">*</span></strong></label></div>
                                    <div><label><input id="optionsRadios1" type="radio" <?php if($productedit->status==1){ echo 'checked';}?> value="1" name="optionsRadio">Active</label>
                                    <label><input id="optionsRadios1" type="radio" <?php if($productedit->status==0){ echo 'checked';}?> value="0" name="optionsRadio">Inactive</label></div>
                                </div>
                                <div class="upload_fileds">
                                    <label>Upload Image <span class="require">*</span></label>
                                    <input id="uploadFile" name="myimage" type="file" placeholder="Choose File" >
                                    <div><img alt="Image Not Found" src="<?php echo base_url(); ?>public/Image/<?php echo $productedit->image;?>"  width="150" height="80"></div>
                                     <input type="hidden" name="old_image" value="<?php echo $productedit->image;?>">
                                </div>					    
                                <div class="form-group">
                                    <label>Select Category</label>
                                    <select class="form-control" id="residence" name="parent" >
                                        <option value="">---Select Category---</option>
                                        <?php foreach ($categories as $cat) { ?>
                                            <option value="<?php echo $cat->id; ?>"<?php
                                            if ($cat->id == $productedit->category_id) {
                                                echo 'selected="selected"';
                                            }
                                            ?>><?php echo $cat->name ?></option>
                                                <?php } ?>
                                    </select>      
                                </div>  
                                <div class="form-group">
                                    <label>Quantity <span class="require">*</span></label>
                                    <input maxlength="4" id="exampleInputQuantity" name="quantity" value="<?php echo $productedit->quantity; ?>" class="form-control" type="text" placeholder="Enter Quantity">
                                </div>
                                <div class="form-group">
                                        <label>Description <span class="require">*</span></label>      
                                        <textarea id="exampleInputDescription" name="description"  class="form-control" type="text" placeholder="Enter Description"><?php echo $productedit->long_description; ?></textarea>
                                    </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label><input id="" type="checkbox" name="optionCheck" <?php if($productedit->is_featured==1){echo 'checked';} ?>>Is_featured</label>     
                                    </div>
                                </div>                 
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/product'" type="button">Back</button>
                            </div>
                    </div>
                 </form>
                </div>
            </div>
        </div>
</div> 
</section>     
</div>
