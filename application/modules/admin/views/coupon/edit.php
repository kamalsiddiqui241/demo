<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Coupons 
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                            echo validation_errors();
                        }
                        ?></div>
                        <div>
                            <form id="myForm" role="form" method="post" action="">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Code <span class="require">*</span></label>
                                        <input id="exampleInputCode" name="code" value="<?php echo $coupon->code;?>" class="form-control" type="text" placeholder="Enter Code">
                                    </div>
                                    <div class="form-group">
                                        <label>Percentage Off <span class="require">*</span></label>
                                        <input id="exampleInputPerc" name="percent_off" value="<?php echo $coupon->percent_off;?>" class="form-control" type="text" placeholder="Enter Percentage">
                                    </div>
                                    <div class="form-group">
                                        <label>No Of Uses <span class="require">*</span></label>
                                        <input id="exampleInputUses" name="uses" value="<?php echo $coupon->no_of_uses;?>" class="form-control" type="text" placeholder="Enter No of Uses">
                                    </div>
                                    <div class="radio">
                                        <div><label class="control-label"><strong>Select Status <span class="require">*</span></strong></label></div>
                                        <div><label><input id="optionsRadios1" <?php if($coupon->status==1){ echo 'checked';}?> type="radio" value="1" name="optionsRadio">Active</label>
                                        <label><input id="optionsRadios1" type="radio" <?php if($coupon->status==0){ echo 'checked';}?> value="0" name="optionsRadio">Inactive</label></div>
                                    </div>
                                    <div class="box-footer">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                        <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/coupon'" type="button">Back</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 
                </section>     
            </div>

