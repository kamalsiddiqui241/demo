<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Coupon Table
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><button class="btn btn-block btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/coupon/add'" type="button">Add Coupon</button></li>
            <li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="listing" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending">Sr No.</th>
                                            <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending">Code</th>
                                            <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending">Percentage</th>
                                            <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending">No of Uses</th>
                                            <th class="sorting_desc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending">Status</th>
                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>  
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </section>     
</div>
<script>
    var datatable_url = '<?php echo base_url(); ?>admin/coupon/index';
    var datatable_edit_url = '<?php echo base_url(); ?>admin/coupon/edit';
    var datatable_delete_url = '<?php echo base_url(); ?>admin/coupon/delete'; 
     <?php $value = $this->session->flashdata('message');if($value=='Added'){?>
        $.growl({
	          title: "",
	          message: "Coupon Added Successfully!!" });  
     <?php } ?>
     <?php $value = $this->session->flashdata('message');if($value=='Update'){?>
        $.growl({
	          title: "",
	          message: "Coupon Updated Successfully!!" });  
     <?php } ?>
</script>   