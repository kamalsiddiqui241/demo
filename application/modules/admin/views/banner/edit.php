<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Banners
            <small>Edit Banner</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Update Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                            echo validation_errors();
                        }
                        ?></div>
                        <form id="myForm" role="form" method="post" action="" enctype="multipart/form-data">
                            <div class="box-body">

                                <div class="form-group">
                                    <label>Name <span class="require">*</span></label>
                                    <input id="exampleInputName" name="name" class="form-control" value="<?php echo $banner_edit->name; ?>"  type="text" placeholder="Enter Name">
                                </div>
                                <div class="radio">
                                    <div><label><strong>Select Status <span class="require">*</span></strong></label></div>
                                    <div><label><input id="optionsRadios1" <?php if($banner_edit->status==1){ echo 'checked';}?>  type="radio" value="1" name="optionsRadio">Active</label>
                                     <label><input id="optionsRadios1" type="radio" <?php if($banner_edit->status==0){ echo 'checked';}?>  value="0" name="optionsRadio">Inactive</label></div>
                                </div>
                                <li class="fileds">
                                    <div class="upload_fileds">
                                        <label>Upload Image <span class="require">*</span></label>
                                        <input id="uploadFile" name="myimage" type="file" placeholder="Choose File"  >
                                        <div><img alt="Image Not Found" src="<?php echo base_url(); ?>public/banner/<?php echo $banner_edit->image; ?>"  width="200" height="100"></div>
                                        <input type="hidden" name="old_image" value="<?php echo $banner_edit->image;?>">
                                    </div>						
                                </li>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/banner'" type="button">Back</button>
                                </div>
                            </div>
                        </form>
                    </div>   
                </div>
            </div>
        </div>
    </section>     
</div>
