<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Add Role
            <small>Adding Roles</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    } ?></div>
                        <form id="myForm" role="form" method="post" action="">
                            
                                <div class="form-group">
                                    <label>Role <span class="require">*</span></label>
                                    <input id="exampleInputRole" name="role" value="<?php echo set_value('role'); ?>" class="form-control" type="text" placeholder="Enter Role">
                                </div>           
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>admin/role'" type="button">Back</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 

    </section>     
</div>
