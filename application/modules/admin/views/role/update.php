<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Role
            <small>Edit Role</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Update Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    }
                        ?></div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Role <span class="require">*</span></label>
                                    <input id="exampleInputFname" name="role" class="form-control" value="<?php echo $updated_role->name; ?>"  type="text" placeholder="Enter Role">
                                <?php // echo form_error('fname');  ?>
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/role'" type="button">Back</button>
                                </div>
                            </div>
                        </form> 
                    </div>        
                </div>
            </div>
        </div> 
    </section>     
</div>
