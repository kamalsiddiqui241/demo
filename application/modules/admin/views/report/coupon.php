<div class="content-wrapper" style="min-height: 916px;">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Coupon Report</h3>
                            <div class="box-body">
                                <div id="chart_div"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
<?php
//pr($coupon);
$coupon = json_encode($coupon,JSON_NUMERIC_CHECK);
//pr($coupon);
$name = array('Jan'=>14,'Feb'=>67);
//pr($name);
//$name = json_encode($name);
//pr($name);
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBarColors);
 var total_coupon = <?php echo $coupon;?>;
function drawBarColors() {
      var data = google.visualization.arrayToDataTable([
        ['Coupon Used', '2016'],
        ['Jan',total_coupon.Jan],
        ['Feb', total_coupon.Feb],
        ['March', total_coupon.March],
        ['April', total_coupon.April],
        ['May', total_coupon.May],
        ['June', total_coupon.June],
        ['July', total_coupon.July],
        ['Aug', total_coupon.Aug],
        ['Sept', total_coupon.Sept],
        ['Oct', total_coupon.Oct],
        ['Nov', total_coupon.Nov],
        ['Dec',total_coupon.Dec],
      ]);
      var options = {
        title: 'Coupon Used in a year',
        chartArea: {width: '50%'},
        colors: ['#b0120a', '#ffab91'],
        hAxis: {
          title: 'Total Coupon Used',
          minValue: 0
        },
        vAxis: {
          title: 'Month'
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
</script>  