<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Add Config
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    }
                        ?></div>
                        <div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Name <span class="require">*</span></label>
                                    <input id="exampleInputName" name="name" value="<?php echo set_value('name'); ?>" class="form-control" type="text" placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label>Value <span class="require">*</span></label>
                                    <input id="exampleInputValue" name="fValue" value="<?php echo set_value('fValue'); ?>" class="form-control" type="text" placeholder="Enter Value">
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/configuration'" type="button">Back</button>
                                </div>
                                </div>
                        </form>
                    
                </div>
            </div>
        </div> 

    </section>     
</div>



