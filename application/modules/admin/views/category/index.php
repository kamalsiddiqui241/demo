<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Category Table
            <small>Category List</small>
        </h1>
        <ol class="breadcrumb">
            <li><button class="btn btn-block btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/category/add'" type="button">Add Category</button></li>
            <li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="listing" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th  name="sr_no" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Sr. No</th>
                                            <th class="sorting" name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Name</th>
                                            <th  name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Parent Name</th>
                                            <th  name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Status</th>
                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>  
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </section>     
</div>

<script>
    var datatable_url = '<?php echo base_url(); ?>admin/category/index';
    var datatable_edit_url = '<?php echo base_url(); ?>admin/category/edit';
    var datatable_delete_url = '<?php echo base_url(); ?>admin/category/delete';
    <?php $value = $this->session->flashdata('message');if($value=='Added'){?>
        $.growl({
	          title: "",
	          message: "Category Added Successfully!!" });  
     <?php } ?>
     <?php $value = $this->session->flashdata('message');if($value=='Update'){?>
        $.growl({
	          title: "",
	          message: "Category Updated Successfully!!" });  
     <?php } ?>
</script>


