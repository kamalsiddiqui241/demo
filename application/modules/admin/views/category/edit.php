<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Role
            <small>Edit Role</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Update Information</h3>
                        </div>
                    <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                        echo validation_errors();
                    }
                    ?></div>
                    <form id="myForm" role="form" method="post" action="">
                        <div class="box-body">  
                            <div class="form-group">
                                <label>Name <span class="require">*</span></label>
                                <input id="exampleInputName" name="name" class="form-control" value="<?php echo $categoryedit->name; ?>"  type="text" placeholder="Enter Name">
                            </div>
                            <div class="radio">
                                <div><label class="control-label"><strong>Select Status <span class="require">*</span></strong></label></div>                        
                                <div><label><input id="optionsRadios1" <?php if($categoryedit->status==1){ echo 'checked';}?>  type="radio"  value="1" name="optionsRadio">Active</label>
                                <label><input id="optionsRadios1" <?php if($categoryedit->status==0){ echo 'checked';}?> type="radio" value="0" name="optionsRadio">Inactive</label></div>
                            </div>
                            <div class="form-group">
                                <label>Select Parent Name</label>                                
                                <select class="form-control" id="residence" name="parent" >
                                    <option value="0">---Select Parent Name---</option>
                                    <?php foreach ($categories as $cat) {  ?>
                                        <option value="<?php echo $cat->id; ?>"<?php
                                        if ($cat->id == $categoryedit->parent_id) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo $cat->name ?></option>
                                            <?php } ?>
                                </select>      
                            </div>                   
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/category'" type="button">Back</button>
                            </div>
                        </div>   
                    </form>
                </div>
            </div>      
        </div>
</div>
</section>
</div>   
