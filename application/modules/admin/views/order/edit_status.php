<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Status
        </h1>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form role="form" method="post" action="<?php echo base_url();?>admin/order/update_status/<?php echo $update_status->id;?>">
                            <div class="box-body">
                                <div class="form-group required">
                                     <div class="col-sm-3">
                                      <div><label>Order Status <span class="require">*</span></label></div>
                                      <?php // pr($update_status);?>
                                        <select name="status" id="input-status"  class="form-control" >
                                            <option value="P" <?php if($update_status->status=='P'){echo 'selected';}?> >Pending</option>  
                                            <option value="O" <?php if($update_status->status=='O'){echo 'selected';}?> >Processing</opvaluetion>
                                            <option value="S" <?php if($update_status->status=='S'){echo 'selected';}?> >Shipped</option>
                                            <option value="D" <?php if($update_status->status=='D'){echo 'selected';}?> >Delivered</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>admin/order'" type="button">Back</button>
                           </div>
                        </form>
                    </div>        
                </div>
            </div>
        </div> 
    </section>     
</div>