<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Order
            <small>Order List</small>
        </h1>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="listing" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th  name="sr_no" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Order_id</th>
                                            <th class="sorting" name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Date</th>
                                            <th name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Status</th>
                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Payment_Gateway</th>
                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Shipping_charges(IN $)</th>
                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>  
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </section>     
</div>

<script>
    var datatable_url = '<?php echo base_url(); ?>admin/order/index';
    var datatable_update_url = '<?php echo base_url(); ?>admin/order/update_status';
    var datatable_view_url = '<?php echo base_url(); ?>admin/order/view_order';
     <?php $value = $this->session->flashdata('message');if($value=='Added'){?>
        $.growl({
	          title: "",
	          message: "Order Status updated Successfully!!" });  
     <?php } ?>
</script>