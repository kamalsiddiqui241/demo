<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Add Email
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    }
                        ?></div>
                        <div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Title <span class="require">*</span></label>
                                    <input id="exampleInputContent" name="title" value="<?php echo set_value('title'); ?>" class="form-control" type="text" placeholder="Enter Title">
                                </div>
                                <div class="form-group ">
                                        <label>Content <span class="require">*</span></label>
                                        <textarea id="exampleInputContent" name="content" value="<?php echo set_value('content'); ?>" class="form-control" type="text" placeholder="Enter Content"></textarea>
                                </div> 
                                <div class="form-group">
                                    <label>Subject <span class="require">*</span></label>
                                    <input id="exampleInputPage" name="subject" value="<?php echo set_value('subject'); ?>" class="form-control" type="text" placeholder="Enter Subject Name">
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>admin/email'" type="button">Back</button>
                                </div>
                                </div>
                        </form>
                </div>
            </div>
        </div> 

    </section>     
</div>
