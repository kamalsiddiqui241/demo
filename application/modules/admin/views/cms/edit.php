<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit CMS
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    }
                        ?></div>
                        <div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Title <span class="require">*</span></label>
                                    <input id="exampleInputContent" name="title" value="<?php echo $edit_value->title;?>" class="form-control" type="text" placeholder="Enter Title">
                                </div>
                                <div class="form-group">
                                        <label>Content <span class="require">*</span></label>
                                        <textarea id="exampleInputContent" name="content" value="" class="form-control" type="text" placeholder="Enter Content"><?php echo $edit_value->content;?></textarea>
                                </div> 
                                <div class="form-group">
                                    <label>Page Name <span class="require">*</span></label>
                                    <input id="exampleInputPage" name="pname" value="<?php echo $edit_value->page_name;?>" class="form-control" type="text" placeholder="Enter Page Name" disabled>
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                    <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url();?>admin/cms'" type="button">Back</button>
                                </div>
                                </div>
                        </form>
                </div>
            </div>
        </div> 

    </section>     
</div>
