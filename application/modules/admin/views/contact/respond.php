<script>
$(document).ready(function(){
    $('#myModal').modal('show');
});
</script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Respond</h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <form role="form" method="post" action="respond">    
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Respond <span class="require">*</span></label>
                                    <textarea id="exampleInputDescription" name="respond" required class="form-control" type="text" placeholder="Enter Description"></textarea>
                                </div> 
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                           <input type="hidden" id="id"  value="<?php echo $id;?>" name="id_name">
                        </form>
                    </div>
                </div>
            </div> 
            </div>
      </div>
    </div>
  </div>
</div>