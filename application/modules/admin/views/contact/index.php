<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Contact Information
            <small>Customer List</small>
        </h1>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="listing" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th  name="sr_no" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Sr.No</th>
                                            <th class="sorting" name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Name</th>
                                            <th  name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Email</th>
                                            <th  name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Contact No</th>
                                            <th   name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Message</th>
                                            <th   name="" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Response</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>  
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </section>     
</div>
<script>
    var datatable_url = '<?php echo base_url(); ?>admin/contact/index';
    var datatable_url_response = '<?php echo base_url(); ?>admin/contact/respond';
</script>
