<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Edit Admin Users
            <small>Edit users</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active">Data tables</li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Update Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    } ?></div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>First Name <span class="require">*</span></label>
                                    <input id="exampleInputFname" name="fname" class="form-control" value="<?php echo $useredit->firstname; ?>" required type="text" placeholder="Enter Firstname">
<?php // echo form_error('fname'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Last Name <span class="require">*</span></label>
                                    <input required="required" id="exampleInputLname" name="lname" class="form-control" value="<?php echo $useredit->lastname; ?>" type="text" placeholder="Enter Lastname">
<?php // echo form_error('lname'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email <span class="require">*</span></label>
                                    <input id="exampleInputEmail1" name="email" value="<?php echo $useredit->email; ?>" class="form-control" required type="email" placeholder="Enter email">
<?php // echo form_error('email'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Password <span class="require">*</span></label>
                                    <input id="inputPassword" name="password" class="form-control" type="password" placeholder=" Enter Password">
<?php // echo form_error('password'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password <span class="require">*</span></label>
                                    <input id="inputCoPassword" name="co_password" class="form-control" type="password" placeholder="Enter  same Password as above">
<?php // echo form_error('co_password'); ?>
                                </div>
                                <div class="radio">
                                    <div><label><strong>Select Status <span class="require">*</span></strong></label></div>
                                    <div><label><input id="optionsRadios1" <?php if($useredit->status==1){ echo 'checked';}?> type="radio"  value="1" name="optionsRadio">Active</label>
                                    <label><input id="optionsRadios1" <?php if($useredit->status==0){ echo 'checked';}?> type="radio"  value="0" name="optionsRadio">Inactive</label></div>
                                </div>
                                <div class="form-group">
                                    <label>Select Role <span class="require">*</span></label>
                                    <select class="form-control" id="residence" name='role' >
                                        <option <?php if($useredit->role_id==1){ echo 'selected';}?> value="1">SuperAdmin</option>
                                        <option <?php if($useredit->role_id==2){ echo 'selected';}?> value="2">Admin</option>
                                        <option <?php if($useredit->role_id==3){ echo 'selected';}?> value="3">InventoryManager</option>
                                        <option <?php if($useredit->role_id==4){ echo 'selected';}?> value="4">OrderManager</option>
                                        <option <?php if($useredit->role_id==5){ echo 'selected';}?> value="5">Customer</option></select>
                                </div>                     
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                 <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>admin/user'" type="button">Back</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>     
</div>
