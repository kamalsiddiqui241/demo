<style></style>
<div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
        <h1>
            Add Admin Users
            <small>Adding users</small>
        </h1>
        <ol class="breadcrumb">
            <li>
            <li>
            <li class="active"></li>
        </ol>
    </section>  
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border"> 
                            <h3 class="box-title">Fill Following Information</h3>
                        </div>
                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php echo validation_errors();
                    } ?></div>
                        <form id="myForm" role="form" method="post" action="">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>First Name <span class="require">*</span></label>
                                    <input  id="exampleInputFname" name="fname"  value="<?php echo set_value('fname'); ?>"  class="form-control" type="text" placeholder="Enter Firstname">
                                </div>
                                <div class="form-group">
                                    <label>Last Name <span claa="require">*</span></label>
                                    <input id="exampleInputLname" name="lname" value="<?php echo set_value('lname'); ?>" class="form-control" type="text" placeholder="Enter Lastname">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email <span class="require">*</span></label>
                                    <input id="exampleInputEmail1" name="email" class="form-control" value="<?php echo set_value('email'); ?>" type="email" placeholder="Enter email">

                                </div>
                                <div class="form-group">
                                    <label>Password <span claa="require">*</span></label>
                                    <input id="inputPassword" name="password" class="form-control" type="password" placeholder="Password">

                                </div>
                                <div class="form-group">
                                    <label>Confirm Password <span class="require">*</span></label>
                                    <input id="inputCoPassword"  name="co_password" class="form-control" type="password" placeholder="Enter  same Password as above">

                                </div>
                                <div class="radio">
                                    <div><label><strong>Select Status <span class="require">*</span></strong></label></div>
                                    <div><label><input id="optionsRadios1" type="radio" checked="checked" value="1" name="optionsRadio">Active</label>
                                    <label><input id="optionsRadios1" type="radio"  value="0" name="optionsRadio">Inactive</label></div>
                                </div>
                                <div class="form-group">
                                    <label>Select role <span class="require">*</span></label>
                                    <select class="form-control"  id="residence" name='role' >
                                        <option value="1">Superadmin</option>
                                        <option value="2">Admin</option>
                                        <option value="3">InventoryManager</option>
                                        <option value="4">OrderManager</option>
                                        <option selected="selected" value="5">Customer</option></select>
                                </div>                     
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>admin/user'" type="button">Back</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </section>     
</div>



