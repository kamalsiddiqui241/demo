<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category EmailModel
 * @link localhost/admin/email
 */
class Email extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('EmailModel');
    }
   /**
    * @param array $data
    */
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->EmailModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/email-listing.js');
        $this->render('index', $data);
    }
   function add() {
        $this->form_validation->set_rules('title', 'Title', 'required|is_unique[d_email_template.title]');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('content', 'content', 'required'); 
        if ($this->form_validation->run()) {
            $data = array();
            $data['title'] = $this->input->post('title');
            $data['subject'] = $this->input->post('subject');
            $data['content'] = $this->input->post('content');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $this->EmailModel->insert_data($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/email/index');
        }
        $data['js'] = array('admin/email-listing.js');
        $this->render('add',$data);
    }

    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('content', 'content', 'required'); 
        if ($this->form_validation->run()) {
            $data = array();
            $data['subject'] = $this->input->post('subject');
            $data['content'] = $this->input->post('content');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $this->EmailModel->edit($data,$id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/email/index');
        }
        if ($id) {
            $data['email_info'] = $this->EmailModel->get($id);
        }
        $data['js'] = array('admin/email-listing.js');
        $this->render('edit', $data);
    }
//    /**
//     * used for delete operation
//     * @param type $id
//     */
    function delete($id){
        $this->EmailModel->delete($id);
        redirect('admin/email');
    }
}