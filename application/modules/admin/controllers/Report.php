<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('ReportModel');
    }
    /**
     * @param array sale
     */
    function sale() {
       $sales = array();
        $sales = $this->ReportModel->get_sale_information();
        $page = array();
        $final_sale = array();
        foreach ($sales as $reg) {
            $final_sale[$reg->month-1] = $reg->total;
        }
        for ($month =0; $month <12; $month++) {
            if (isset($final_sale[$month])) {
                $page[$month] = $final_sale[$month];
            } else {
                $page[$month] = "";
            }
        }
        $data['sale'] = $page;
        $this->render('sales', $data);
    }
    /**
     * @param array registration
     */
    function user_registration() {
        $registration = array();
        $registration = $this->ReportModel->get_registration_information();
        $page = array();
        $final_register = array();
        foreach ($registration as $reg) {
            $final_register[$reg->month-1] = $reg->count;
        }
        for ($month = 0; $month <12; $month++) {
            if (isset($final_register[$month])) {
                $page[$month] = $final_register[$month];
            } else {
                $page[$month] = "";
            }
        }
        $data['registration'] = $page;
        $this->render('customerRegister', $data);
    }

    function coupon() {
        $months = array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'Aug',
            9 => 'Sept',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec',
        );
        $data = array();
        $page = array();
        $data = $this->ReportModel->get_coupon_information();
        $coupon = array();
        foreach ($data as $data_array) {
            $coupon[$months[$data_array->month]] = $data_array->count;
        }
        $page['coupon'] = $coupon;
//        pr($page);
        $this->render('coupon', $page);
    }

}
