<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category CmsModel
 * @link localhost/admin/cms
 */
class Cms extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('CmsModel');
    }
    function index() {        
        $data = array();
        if(!empty($this->input->post())) {
            $data = $this->CmsModel->listing();
            echo json_encode($data);
            exit();
        } 
        $data['js'] = array('admin/cms-listing.js');       
        $this->render('index', $data);
    }  
    function add(){
        $data = array();
        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('pname','Page Name','required|is_unique[d_cms.page_name]');
        $this->form_validation->set_rules('content','Content','required');
        if($this->form_validation->run()){
            $data['title']= $this->input->post('title');
            $data['page_name']=$this->input->post('pname');
            $data['content']=$this->input->post('content');
            $this->CmsModel->insert($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/cms/index');
        }
        $data['js'] = array('admin/cms-listing.js');
        $this->render('add',$data);
    }
    function edit($id){
        $data = array();
        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('content','Content','required');
         if($this->form_validation->run()){
            $data['title']= $this->input->post('title');
            $data['page_name']=$this->input->post('pname');
            $data['content']=$this->input->post('content');
            $this->CmsModel->edit($data);
            $this->session->set_flashdata('message','Update');
            redirect('admin/cms/index');
        }
         if($id){
            $data['edit_value'] = $this->CmsModel->get($id);
        }
          $data['js'] = array('admin/cms-listing.js');
          $this->render('edit',$data);
    }
    function delete($id){
        $this->CmsModel->delete($id);
        redirect('admin/cms/index');
    }
}