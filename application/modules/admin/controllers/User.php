<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category UserModel
 * @link localhost/admin/user
 */
class User extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('UserModel');
        if (!$this->session->userdata('user')) {
            redirect('admin');
        }
    }
    /**
     * used for listing user info
     * @param array $userlist
     * @param array $page
     */
    function index() {
        $data = array();
        if(!empty($this->input->post())) {
            $data = $this->UserModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/user-listing.js');
        $this->render('index', $data);
    }
    
    /**
     * inser  user info into database
     * @param array $data
     * @param array $page
     */
    function add() {

        $this->form_validation->set_rules('fname', 'First Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|is_unique[d_users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric|max_length[12]|min_length[8]');
        $this->form_validation->set_rules('co_password', 'Password Confirmation', 'trim|required|matches[password]');

        if ($this->form_validation->run()) {

            $data = array();
            $data['firstname'] = $this->input->post('fname');
            $data['lastname'] = $this->input->post('lname');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['password'] = $this->input->post('co_password');
            $data['status'] = $this->input->post('optionsRadio');
            $data['role_id'] = $this->input->post('role');
            $this->UserModel->insert_data($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/user/index');
        }
         $this->render('add');
    }

    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {

        $this->form_validation->set_rules('fname', 'First Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric|max_length[12]|min_length[8]');
        $this->form_validation->set_rules('co_password', 'Password Confirmation', 'trim|required|matches[password]');

        if ($this->form_validation->run()) {
            $data = array();
            $data['firstname'] = $this->input->post('fname');
            $data['lastname'] = $this->input->post('lname');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['password'] = $this->input->post('co_password');
            $data['status'] = $this->input->post('optionsRadio');
            $data['role_id'] = $this->input->post('role');
            $this->UserModel->edit($data, $id);
             $this->session->set_flashdata('message','Update');
            redirect('admin/user/index');
        }
        if ($id) {
            $data['useredit'] = $this->UserModel->get($id);
        }
          $this->render('edit', $data);
    }

    /**
     * used for delete the variable from database
     * @param int $id
     */
    function delete($id) {
        
        $this->UserModel->delete($id);
        redirect('admin/user');
    }
   /**
    * used to logout the user
    */
    function logout() {
        $this->session->unset_userdata('user');
        session_destroy();
        redirect('admin/login');
    }

}
