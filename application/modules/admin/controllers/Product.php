<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category CategoryModel
 * @link localhost/admin/user
 */
class Product extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('ProductModel');
//        $this->load->library('upload');
    }

    /**
     * used for listing user info
     * @param array $userlist
     * @param array $page
     */
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->ProductModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/product-listing.js');
        $this->render('index', $data);
    }
    /**
     * used to add the product to database
     */
    function add() {
        $this->form_validation->set_rules('name', ' Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required|decimal');
        $this->form_validation->set_rules('quantity', 'Quantity', 'required|integer|max_length[4]');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['price']=$this->input->post('price');
            $data['quantity']=$this->input->post('quantity');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['category_id'] = $this->input->post('parent');
            $data['long_description'] = $this->input->post('description');
            $data['status'] = $this->input->post('optionsRadio');
            $data['is_featured'] = $this->input->post('optionCheck'); 
            if($this->input->post('optionCheck')== null){
                $data['is_featured'] = 0;
            }
            $_config['upload_path'] = FCPATH.IMAGE_DIR;//global variables
//            pr(FCPATH.IMAGE_DIR);
            $_config['allowed_types'] = 'gif|jpg|png|jpeg';   
//            pr($_config);
            $this->load->library('upload', $_config);
            if (!$this->upload->do_upload('myimage')) {
                $error = array('error' => $this->upload->display_errors());
//                pr($error);
//                 echo ('image must be in gif|jpg|png|jpeg');exit;
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
//                chmod('FCPATH.IMAGE_DIR/'.$data['image'], 0777);
            }          
            $this->ProductModel->insert_data($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/product/index');
        }
         $data['categories'] = $this->ProductModel->category_list(); 
         $data['js'] = array('admin/cms-listing.js');
         $this->render('add', $data);
    }
    /**
     * 
     * @param integer $id
     */
     function edit($id) {
         $data = array();
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run()) { 
            $data['name'] = $this->input->post('name');
            $data['price'] = $this->input->post('price');
            $data['quantity']=$this->input->post('quantity');
            $data['category_id'] = $this->input->post('parent');
            $data['long_description'] = $this->input->post('description');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['status'] = $this->input->post('optionsRadio');
            $data['is_featured'] = $this->input->post('optionCheck'); 
            $_config['upload_path'] = FCPATH.IMAGE_DIR;
            $_config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $_config);
            if (!$this->upload->do_upload('myimage')) {
                $data['image'] = $this->input->post('old_image'); 
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
            }
            $this->ProductModel->edit($data, $id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/product/index');
        }
        if ($id) { 
            $data['productedit'] = $this->ProductModel->get($id);
        }
        $data['categories'] = $this->ProductModel->category_list();
        $data['js'] = array('admin/cms-listing.js');
        $this->render('edit', $data);
    }
      /**
     * used for delete the variable from database
     * @param int $id
     */
    function delete($id) {
        $data = $this->ProductModel->get($id);
        $name = $data->image;
//        echo $name;exit;
        $this->ProductModel->delete($id,$name);
        redirect('admin/product');
    }

}
