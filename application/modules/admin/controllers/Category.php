<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category CategoryModel
 * @link localhost/admin/user
 */
class Category extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('CategoryModel');
          if (!$this->session->userdata('user')) {
            redirect('admin');
        }
    }

    /**
     * used for listing user info
     * @param array $userlist
     * @param array $page
     */
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->CategoryModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/category-listing.js');
        $this->render('index', $data);
    }
    /**
     * inser  user info into database
     * @param array $data
     * @param array $page
     */
    function add() {
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['parent_id'] = $this->input->post('parent');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['status'] = $this->input->post('optionsRadio');
            $this->CategoryModel->insert($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/category/index');
        }
        $data['categories'] = $this->CategoryModel->category_list();
        $this->render('add', $data);
    }

    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['parent_id'] = $this->input->post('parent');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['status'] = $this->input->post('optionsRadio');
            $this->CategoryModel->edit($data, $id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/category/index');
        }
        if ($id) {
            $data['categoryedit'] = $this->CategoryModel->get($id);
        }
        $data['categories'] = $this->CategoryModel->category_list();
        $this->render('edit', $data);
    }

    /**
     * used for delete the variable from database
     * @param int $id
     */
    function delete($id) {
        $this->CategoryModel->delete($id);
        redirect('admin/category');
    }

}
