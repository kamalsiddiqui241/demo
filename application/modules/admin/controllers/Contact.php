<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('ContactModel');
        $this->load->Model('SendEmailModel');
    }
    /**
     * used to listing the contact details of all users
     * @param $data array
     */
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->ContactModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/contact-listing.js');
        $this->render('index', $data);
    }
//    function respond($id){
//        $data = array();
//         $this->form_validation->set_rules('respond', 'Response', 'required');
//         if ($this->form_validation->run()) {
//          $data['respond']= $this->input->post('respond');
//          $this->ContactModel->respond($data,$id);
//           $title ='admin_response_contact_us';
//                $email_template = $this->SendEmailModel->template($title);
//                $data = $this->ContactModel->get_info($id);
//                $string[0] = $data->name;
//                $string[1] = $data->contact_no;
//                $string[2] = $data->email;
//                $string[3] = $data->message;
//                $string[4] = $data->respond;
//                $replace[0] = '{name}';
//                $replace[1] = '{c.no}';
//                $replace[2] = '{email}';
//                $replace[3] = '{message}';
//                $replace[4] = '{respond}';
//                $body = str_replace($replace, $string, $email_template->content);
//                $send_data = array(
//                    'body' => $body,
//                    'to_email' => $data->email,
//                );
//          $result = $this->SendEmailModel->send_mail($send_data);
//          if($result){
//          redirect('admin/contact/index');
//          }
//        }
//        $data['js'] = array('admin/contact-listing.js');
//        $this->render('respond',$data);
//    }
                              /**
     * used to give response to user
     * @param int $id
     */
    function respond($id=0){ 
        $this->form_validation->set_rules('respond', 'Response', 'required');
        if($this->form_validation->run()){
        $id = $this->input->post('id_name');
         $data['respond']= $this->input->post('respond');
         $this->ContactModel->respond($data,$id);
         $title ='admin_response_contact_us';
                $email_template = $this->SendEmailModel->template($title);
                $data = $this->ContactModel->get_info($id);
                $string[0] = $data->name;
                $string[1] = $data->contact_no;
                $string[2] = $data->email;
                $string[3] = $data->message;
                $string[4] = $data->respond;
                $replace[0] = '{name}';
                $replace[1] = '{c.no}';
                $replace[2] = '{email}';
                $replace[3] = '{message}';
                $replace[4] = '{respond}';
                $body = str_replace($replace, $string, $email_template->content);
                $send_data = array(
                    'body' => $body,
                    'to_email' => $data->email,
                );
          $result = $this->SendEmailModel->send_mail($send_data);
          redirect('admin/contact/index');
        }
        $page['id'] = $id; 
        $this->load->view('contact/respond',$page);
    }
}
