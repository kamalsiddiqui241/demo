<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class Admin_controller
 * @category UserModel
 * @link localhost/admin/user
 */
class Order extends Admin_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('OrderModel');
        $this->load->Model('SendEmailModel');
        $this->load->Model('ConfigurationModel');
    }
    function index() {      
        $data = array();
        if(!empty($this->input->post())) {
            $data = $this->OrderModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/order-listing.js');       
        $this->render('index', $data);
    }
    /**
     * 
     * @param integer $id
     */
    function view_order($id){
        $data = array();
        $data['order_details']= $this->OrderModel->get_order_details($id);
        $this->render('view',$data);
    }
    function update_status($id){
        $data = array();       
        $this->form_validation->set_rules('status','Status','required');
        if ($this->form_validation->run()) {
            $status = $this->input->post('status');
            $this->OrderModel->update_status($status, $id);
                $title ='order_details_status';
                $email_template = $this->SendEmailModel->template($title);
                $data=$this->OrderModel->get_order_details($id);
                $string[0] = 'kamal.siddiqui@wwindia.com'; 
                $replace[0] = '{email}';
                $body = str_replace($replace, $string, $email_template->content);
                $send_data = array(
                    'body' => $body,
                    'to_email' => $data[0]->email,
                );
          $result = $this->SendEmailModel->send_mail($send_data);
          if($result){
            $this->session->set_flashdata('message','Added');
            redirect('admin/order/index');
        }
      }
      if($id){
          $data['update_status']= $this->OrderModel->get_status($id);
      }
     $this->render('edit_status',$data);
    }
}