<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('RoleModel');
    }
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->RoleModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/role-listing.js');
        $this->render('index', $data);
    }
    /**
     * insert  role info into database
     * @param array $data
     * @param array $page
     */
    function add() {
        $this->form_validation->set_rules('role', 'Role', 'required|alpha_numeric_spaces');

        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('role');
            $this->RoleModel->insert($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/role/index');
        }
        $this->render('add');
    }
    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {
        $this->form_validation->set_rules('role', 'Role', 'required|alpha_numeric_spaces');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('role');
            $this->RoleModel->edit($data, $id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/role/index');
        }
        if ($id) {
            $data['updated_role'] = $this->RoleModel->get_user($id);
        }
       $data['js'] = array('admin/role-listing.js');
        $this->render('update', $data);
    }

    /**
     * used for delete the variable from database
     * @param int $id
     */
    function delete($id) {
            $this->RoleModel->delete($id);
            redirect('admin/role');
    }

}
