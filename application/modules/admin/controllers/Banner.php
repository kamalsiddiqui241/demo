<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category UserModel
 * @link localhost/admin/user
 */
class Banner extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('BannerModel');
    }

    /**
     * used for listing user info
     * @param array $userlist
     * @param array $page
     */
    function index() {
            
        $data = array();
        if(!empty($this->input->post())) {
            $data = $this->BannerModel->listing();
            echo json_encode($data);
            exit();
        }
        
        $data['js'] = array('admin/banner-listing.js');       
        $this->render('index', $data);
    }    
    /**
     * inser  user info into database
     * @param array $data
     * @param array $page
     */
    function add() {
        $this->form_validation->set_rules('name', ' Name', 'required');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['status'] = $this->input->post('optionsRadio');
            $_config['upload_path'] = FCPATH.BANNER_DIR;//global variables
            $_config['allowed_types'] = 'gif|jpg|png|jpeg';    
            $this->load->library('upload', $_config);
            if (!$this->upload->do_upload('myimage')) {
                $error = array('error' => $this->upload->display_errors());              
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
            }
            $this->BannerModel->insert_data($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/banner/index');
        }     
         $this->render('add');
    }
    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {          
        $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha');
        if ($this->form_validation->run()) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['status'] = $this->input->post('optionsRadio');
            $_config['upload_path'] = FCPATH.BANNER_DIR;
            $_config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $_config);
            if (!$this->upload->do_upload('myimage')) {
               $data['image']=$this->input->post('old_image');
            } else {
                $upload_data = $this->upload->data();
                $data['image'] = $upload_data['file_name'];
            }
            $this->BannerModel->edit($data, $id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/banner/index');
        }
        if ($id) {
            $data['banner_edit'] = $this->BannerModel->get($id);
        }   
        $this->render('edit', $data);;
    }

    /**
     * used for delete the variable from database
     * @param int $id
     */
    function delete($id) { 
        $data = $this->BannerModel->get($id);
        $name = $data->image;
//        echo $name;exit;
        $this->BannerModel->delete($id,$name);
        redirect('admin/banner');
    }
}