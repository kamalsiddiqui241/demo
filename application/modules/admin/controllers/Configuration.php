<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Configuration extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('ConfigurationModel');  
    }
   /**
    * used to listing the configuration table
    * @param array $data
    */
    function index(){
       $data = array();
         if (!empty($this->input->post())) {
            $data = $this->ConfigurationModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/configuration-listing.js');
        $this->render('index',$data);
    }
    /**
     * used to add configuration
     */
    function add(){
        $data = array();
        $this->form_validation->set_rules('name','Name','trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('fValue','Value','trim|required|is_unique[d_configuration.name]');
        if($this->form_validation->run()){
           
            $data['name'] = $this->input->post('name');
            $data['value'] = $this->input->post('fValue');
            $this->ConfigurationModel->insert($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/configuration');
        }
        $this->render('add');
    }
    /**
     * used to edit
     * @param int $id
     * @param array $data
     */
    function edit($id){
        $data = array();
        $this->form_validation->set_rules('name','Name','trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('fValue','Value','trim|required|is_unique[d_configuration.name]');
        if($this->form_validation->run()){
            $data['name'] = $this->input->post('name');
            $data['value'] = $this->input->post('fValue');
             $this->ConfigurationModel->edit($data ,$id);
             $this->session->set_flashdata('message','Update');
             redirect('admin/configuration/index');
        }
        if($id){
            $data['updated_value'] = $this->ConfigurationModel->get($id);
        }
          $this->render('update',$data);
}
/**
 * used to delete 
 * @param integer $id
 */
    function delete($id){
           $this->ConfigurationModel->delete($id);
           redirect('admin/configuration'); 
    }
    
}