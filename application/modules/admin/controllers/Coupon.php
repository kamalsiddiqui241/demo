<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category UserModel
 * @link localhost/admin/user
 */
class Coupon extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('CouponModel');
    }

    /**
     * used for listing user info
     * @param array $userlist
     * @param array $page
     */
    function index() {
        $data = array();
        if (!empty($this->input->post())) {
            $data = $this->CouponModel->listing();
            echo json_encode($data);
            exit();
        }
        $data['js'] = array('admin/coupon-listing.js');
        $this->render('index', $data);
    }

    /**
     * inser  user info into database
     * @param array $data
     * @param array $page
     */
    function add() {
        $this->form_validation->set_rules('code', ' Name', 'trim|required|alpha_numeric|is_unique[d_coupon.code]');
        $this->form_validation->set_rules('percent_off', 'Percentage Off', 'trim|decimal|required');
        $this->form_validation->set_rules('uses', 'No Of Uses', 'required|max_length[4]|integer');
        if ($this->form_validation->run()) {
            $data = array();
            $data['code'] = $this->input->post('code');
            $data['percent_off'] = $this->input->post('percent_off');
            $data['no_of_uses'] = $this->input->post('uses');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['status'] = $this->input->post('optionsRadio');
            $this->CouponModel->insert_data($data);
            $this->session->set_flashdata('message','Added');
            redirect('admin/coupon/index');
        }
        $this->render('add');
    }

    /**
     * to update the value of users 
     * @param int $id
     * @param array $data
     */
    function edit($id) {
        $data = array();
        $this->form_validation->set_rules('code', ' Name', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('percent_off', 'Percentage Off', 'trim|decimal|required');
        $this->form_validation->set_rules('uses', 'No Of Uses', 'required|max_length[4]|integer');
        if ($this->form_validation->run()) {
            $data['code'] = $this->input->post('code');
            $data['percent_off'] = $this->input->post('percent_off');
            $data['no_of_uses'] = $this->input->post('uses');
            $data['created_by'] = $this->session->userdata('user')['id'];
            $data['status'] = $this->input->post('optionsRadio');
            $this->CouponModel->edit($data,$id);
            $this->session->set_flashdata('message','Update');
            redirect('admin/coupon/index');
        }
        if ($id) {
            $data['coupon'] = $this->CouponModel->get($id);
        }
        $this->render('edit', $data);
    }
    /**
     * used for delete operation
     * @param type $id
     */
    function delete($id){
        $this->CouponModel->delete($id);
        redirect('admin/coupon');
    }
}
