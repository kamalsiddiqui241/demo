<?php

/**
 * Super class Admin_Controller
 * @category Loginmodel
 * @link        localhost/demo/admin/Login
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->Model('LoginModel');
    }

    /**
     * used for listing users info
     * @param $data
     */
    function index() {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric');
        if ($this->form_validation->run()) {
            $data = array();

            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $result = $this->LoginModel->login($data);
//            pr($result->id);
            if ($result) {
                $datasession = array(
                    'user' => array(
                        'firstname'=>$result->firstname,
                        'lastname'=>$result->lastname,
                        'id' => $result->id,
                        'email'=> $result->email,
                        'is_logged_in' => true
                ));
                $this->session->set_userdata($datasession);
//                pr($datasession);
                redirect('admin/user/index');
            } else {
                $this->session->set_flashdata('message', 'Invalid email or password  ');
            }
        }

        $this->load->view('login/login_page');
    }

}
