<?php

class ConfigurationModel extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    function listing(){
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_configuration.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();
           if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }
        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
                $sort_by = 'd_configuration.name';
            $order = $post['order'][0]['dir'];
        }
//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];
        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
         $sql_count = '';
         $this->db->select('id,name,value');
         if($search_text){
           $this->db->like('name',$search_text);
         }
         $this->db->order_by($sort_by,$order);
         if($limit){
             $this->db->limit($limit,$offset);
         }   
        $query = $this->db->get('d_configuration');
        $total_value = array();
        $total_value = $query->result(); 
         $this->db->select('id');
        $this->db->from('d_configuration');
        if($search_text){
          $this->db->like('name',$search_text);  
        }
        $total_display_records = $this->db->count_all_results();
        
         $final = array();
         if ($total_value) {
            foreach ($total_value as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['name'] = $row->name;
                $singleListArray['value'] = $row->value;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
    
    function insert($data){
        $this->db->insert('d_configuration',$data);    
    }
    function get($id) {
        $this->db->select('id,name,value');
        $this->db->where('id', $id);
        $query = $this->db->get('d_configuration');
        return $query->row();
    }
    function edit($data,$id){
        $this->db->where('id',$id);
        $this->db->update('d_configuration',$data);    
    }
     function delete($id) {
        if(!empty($id)){
        $this->db->where('id', $id);
        $this->db->delete('d_configuration');
        }
    }   
}