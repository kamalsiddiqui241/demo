<?php
/**
 * Super class CI_Model
 * @category UserModel 
 */
class UserModel extends CI_Model {

    public function construct() {
        parent::construct();
    }

    /**
     * used for listing info of users from database 
     * @return array
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_users.firstname';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];

            if ($sort_by === 0)
                $sort_by = 'd_users.firstname';
            else if ($sort_by === 1)
                $sort_by = 'd_users.lastname';
            $order = $post['order'][0]['dir'];
        }

//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }

        $sql_count = '';

        //now get actual data with all the filters and limit
//            $sql = "SELECT * FROM d_users d";
        $this->db->select('id,firstname,lastname,email,status,role_id');
        if ($search_text) {
            $this->db->like('firstname', $search_text);
//               $sql .= $sql_count = " WHERE c.firstname LIKE '%$search_text%'";
        }
//            $sql .= " ORDER BY $sort_by $order";
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            //  $sql .= " LIMIT $limit OFFSET $offset";
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get('d_users');
        /* echo'<pre>';
          echo $this->db->last_query();
          exit; */
        $categories = array();
        $categories = $query->result();
//          echo '<pre>';
//          print_r($categories);exit;
        //Count
        $this->db->select('id');
        $this->db->from('d_users');
        if ($search_text) {
            $this->db->like('firstname', "%" . $search_text . "%");
        }
        $total_display_records = $this->db->count_all_results();
        /* echo'<pre>';
          print_r($total_count);
          exit; */
         
        $sr_number = 1;
        $final = array();
        if ($categories) {
            foreach ($categories as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['firstname'] = $row->firstname;
                $singleListArray['lastname'] = $row->lastname;
                $singleListArray['email'] = $row->email;
                $singleListArray['role'] = $row->role_id;
                if($row->role_id==1){
                  $singleListArray['role']='SuperAdmin';  
                }else if($row->role_id==2){
                    $singleListArray['role']='Admin';
                }else if($row->role_id==3){
                    $singleListArray['role']='InventoryManager';
                }else if($row->role_id==4){
                    $singleListArray['role']='OrderManager';
                }else{
                    $singleListArray['role']='Customer';
                }
                $singleListArray['status'] = $row->status ? 'Active' : 'Inactive';
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }

    /**
     * used for inserting information of users into database
     * @param array $data
     */
    function insert_data($data) {
        $temp = $data['password'];
        $data['password'] = md5($temp);
        $this->db->insert('d_users', $data);
    }

    /**
     * used for update the data of users
     * @param array $data
     * @param int $id
     */
    function edit($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('d_users', $data);
    }

    /**
     * used for retrieved the specific data from database
     * @param int $id
     * @return array
     */
    function get($id) {
        $this->db->select('firstname,lastname,email,status,role_id');
        $this->db->where('id', $id);
        $query = $this->db->get('d_users');
        return $query->row();
    }
 /**
  * used to delete users
  * @param integer $id
  */
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('d_users');
    }

}
