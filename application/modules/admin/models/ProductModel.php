<?php
/**
 * Super class CI_Model
 * @category ProductModel 
 */
class ProductModel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /**
     * used to listing product info from db
     * @return json $finalJsonArray
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'p.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();
        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }
        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            $sort_by = 'p.name';
            $order = $post['order'][0]['dir'];
        }
        if (array_key_exists('start', $post))
            $offset = $post['start'];
        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
        $sql_count = '';
        $this->db->select('p.id,p.is_featured,p.name AS name,p.status,p.price,i.name AS image,d.name AS category');
        $this->db->from('d_product as p');
        $this->db->join('d_product_attributes_assoc AS a', 'p.id = a.product_id', 'left');
        $this->db->join('d_product_images AS i', 'p.id = i.product_id', 'left');
        $this->db->join('d_product_categories AS c', 'p.id=c.product_id', 'left');
        $this->db->join('d_category AS d', 'c.category_id=d.id', 'left');
        if ($search_text) {
            $this->db->like('p.name', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $total_value = $query->result();
        $this->db->select('p.id');
        $this->db->from('d_product as p');
        $this->db->join('d_product_attributes_assoc AS a', 'p.id = a.product_id', 'left');
        $this->db->join('d_product_images AS i', 'p.id = i.product_id', 'left');
        if ($search_text) {
            $this->db->like('p.name', $search_text);
        }
        $total_display_records = $this->db->count_all_results();
        $final = array();
        if ($total_value) {
            foreach ($total_value as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['name'] = $row->name;
                $money='$'.round($row->price,2);
                $singleListArray['price'] = $money;
                $singleListArray['status'] = $row->status ? 'Active' : 'Inactive';
                $singleListArray['image'] = $row->image;
                $singleListArray['category'] = $row->category;
                $singleListArray['is_featured'] = $row->is_featured;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
    /**
     * 
     * @param array $data
     */
    function insert_data($data) {
        $page = array();
        $product = array();
        $category = array();
        $product['name'] = $data['name'];
        $product['price'] = $data['price'];
        $product['quantity'] = $data['quantity'];
        $product['long_description'] = $data['long_description'];
        $product['status'] = $data['status'];
        $product['is_featured'] = $data['is_featured'];
        $this->db->insert('d_product', $product);
        $data['id'] = $this->db->insert_id();
        $page['name'] = $data['image'];
        $page['created_by'] = $data['created_by'];
        $page['status'] = $data['status'];
        $page['product_id'] = $data['id'];
        $this->db->insert('d_product_images', $page);
        $category['category_id'] = $data['category_id'];
        $category['product_id'] = $data['id'];
        $this->db->insert('d_product_categories', $category);
    }
   /**
    * 
    * @param integer $id
    * @return array
    */
    function get($id) {
        $this->db->select('p.id,p.quantity,p.is_featured,p.long_description,p.name AS name,p.status,i.name AS image,p.price,c.category_id');
        $this->db->from('d_product as p');
        $this->db->join('d_product_attributes_assoc AS a', 'p.id = a.product_id', 'left');
        $this->db->join('d_product_images AS i', 'p.id = i.product_id', 'left');
        $this->db->join('d_product_categories AS c', 'p.id=c.product_id', 'left');  
        $this->db->where('p.id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    /**
     * 
     * @param array $data
     * @param integer $id
     */
    function edit($data, $id) {
        $page = array();
        $product = array();
        $category = array();
        $page['name'] = $data['image'];
        $page['status'] = $data['status'];
        $page['created_by'] = $data['created_by'];
        $this->db->where('product_id', $id);
        $this->db->update('d_product_images', $page);
        $product['created_by'] = $data['created_by'];
        $product['name'] = $data['name'];
        $product['status'] = $data['status'];
        $product['price'] = $data['price'];
        $product['quantity'] = $data['quantity'];
        $product['long_description'] = $data['long_description'];
        $product['is_featured'] = $data['is_featured'];
        $this->db->where('id', $id);
        $this->db->update('d_product', $product);
        $category['category_id'] = $data['category_id'];
        $this->db->where('product_id', $id);
        $this->db->update('d_product_categories', $category);
    }
   /**
    * 
    * @return array
    */
    function category_list() {
        $this->db->select('name,id');
        $query = $this->db->get("d_category");
        return $query->result();
    }
    /**
     * 
     * @param integer $id
     * @param text $name
     */
    function delete($id,$name) {
        if (!empty($id)) {
            $this->db->where('id', $id);
            $this->db->delete('d_product');
            $this->db->where('product_id', $id);
            $this->db->delete('d_product_images');
            $this->db->where('product_id', $id);
            $this->db->delete('d_product_categories');
            $this->db->where('product_id', $id);
            $this->db->delete('d_product_attributes_assoc'); 
//            echo ('public/Image/'.$name);exit;
            unlink('public/Image/'.$name);
        }
    }
}
