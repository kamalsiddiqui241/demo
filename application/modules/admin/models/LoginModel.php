<?php

class LoginModel extends CI_Model {

    public function construct() {
        parent::construct();
    }

    function login($data) {

        $password = $data['password'];
        $email = $data['email'];
        $condition = array('email' => $email, 'password' => MD5($password),'role_id!=' => '5');
        $this->db->select('u.firstname,u.lastname,u.id,u.email,u.password,r.name');
        $this->db->from('d_users AS u');
        $this->db->where($condition);
        $this->db->join('d_roles AS r','r.id = u.role_id');
        $query = $this->db->get();      
        if ($query->num_rows() == 1) {
            return $query->row();            
        } else {
            return false;
        }
    }

}
