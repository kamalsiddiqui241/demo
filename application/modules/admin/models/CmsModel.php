<?php
/**
 * Super class CI_Model
 * @category CMsModel 
 */
class CmsModel extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    /**
     * used to listing the cms info from db
     * @return json $finalJsonArray
     */
    function listing(){
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_cms.tilte';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();
           if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }
        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
                $sort_by = 'd_cms.title';
            $order = $post['order'][0]['dir'];
        }
//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];
        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
         $sql_count = '';
         $this->db->select('*');
         if($search_text){
           $this->db->like('title',$search_text);
         }
         $this->db->order_by($sort_by,$order);
         if($limit){
             $this->db->limit($limit,$offset);
         }   
        $query = $this->db->get('d_cms');
        $total_value = array();
        $total_value = $query->result(); 
         $this->db->select('id');
        $this->db->from('d_cms');
        if($search_text){
          $this->db->like('title',$search_text);  
        }
        $total_display_records = $this->db->count_all_results(); 
         $final = array();
         if ($total_value) {
            foreach ($total_value as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['title'] = $row->title;
                $singleListArray['content'] = $row->content;
                $singleListArray['page_name'] = $row->page_name;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
    /**
     * 
     * @param array $data
     */
    function insert($data){
        $this->db->insert('d_cms',$data);
    }
    /**
     * 
     * @param int $id
     * @return array
     */
    function get($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        $query = $this->db->get('d_cms');
        return $query->row();
    }
    /**
     * 
     * @param array $data
     * @param integer $id
     */
   function edit($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('d_cms', $data);
    }
    /**
     * 
     * @param integer $id
     */
    function delete($id){
       $this->db->where('id',$id);
       $this->db->delete('d_cms'); 
    }
}