<?php

/**
 * Super class CI_Model
 * @category UserModel 
 */
class CouponModel extends CI_Model {

    public function construct() {
        parent::construct();
    }
    /**
     * used for listing info of users from database 
     * @return array
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_coupon.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            if($sort_by==0){
            $sort_by = 'd_coupon.code';
            }
            if($sort_by==1){
                $sort_by = 'd_coupon.percent_off';
            }
            $order = $post['order'][0]['dir'];
        }
//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
        $sql_count = '';
        $this->db->select('id,code,percent_off,status,no_of_uses');
        if ($search_text) {
            $this->db->like('code', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get('d_coupon');
       $total_coupon = array();
       $total_coupon = $query->result();
        $this->db->select('id');
        $this->db->from('d_coupon');
        if ($search_text) {
            $this->db->like('code', "%" . $search_text . "%");
        }
        $total_display_records = $this->db->count_all_results();  
        $final = array();
        if ($total_coupon) {
            foreach ($total_coupon as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['code'] = $row->code;
                $singleListArray['percent_off'] = $row->percent_off;
                $singleListArray['no_of_uses'] = $row->no_of_uses;
                $singleListArray['status'] = $row->status ? 'Active' : 'Inactive';               
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
    function insert_data($data){
        $this->db->insert('d_coupon',$data);      
    }
    function get($id){
     $this->db->select('*');
     $this->db->where('id',$id);
     $query = $this->db->get('d_coupon');
     return $query->row();
    }
    function edit($data,$id){
        $this->db->where('id',$id);
        $this->db->update('d_coupon',$data);
    }
    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('d_coupon');
    }
}    