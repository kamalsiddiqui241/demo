<?php
/**
 * Super class CI_Model
 * @category ContactModel 
 */
class ContactModel extends CI_model {
    public function construct() {
        parent::construct();
    }
    /**
     * used to listing the contact info
     * @return json $finalJsonArray
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_contact_us.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
                $sort_by = 'd_contact_us.name';
            $order = $post['order'][0]['dir'];
        }

//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }

        $sql_count = '';
        $this->db->select('*');
        if($search_text){
            $this->db->like('name',$search_text);
        }
        $this->db->order_by($sort_by,$order);
        if($limit){
         $this->db->limit($limit,$offset);
        }
        $query = $this->db->get('d_contact_us');
        $total_roles = array();
        $total_roles = $query->result();
        
        $this->db->select('id');
        $this->db->from('d_contact_us');
        if($search_text){
          $this->db->like('name',$search_text);  
        }
//        echo "<pre>";
//    echo $this->db->last_query();
//    exit;
        $total_display_records = $this->db->count_all_results();
        $final = array();
         if ($total_roles) {
            foreach ($total_roles as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['name'] = $row->name;
                $singleListArray['email'] = $row->email;
                $singleListArray['contact'] = $row->contact_no;
                $singleListArray['message'] = $row->message;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
    /**
     * used to add the respond
     * @param array $data
     * @param integer $id
     */
    function respond($data,$id){
        $this->db->set('respond', $data['respond']);
        $this->db->where('id', $id);
        $this->db->update('d_contact_us'); 
    }
    /**
     * used to get the information of particulart id
     * @param int $id
     * @return array
     */
    function get_info($id){
       $this->db->select('*');
       $this->db->where('id',$id);
       $query = $this->db->get('d_contact_us');
       return $query->row();
    }
}