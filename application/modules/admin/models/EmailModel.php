<?php
/**
 * Super class CI_Model
 * @category UserModel 
 */
class EmailModel extends CI_Model {

    public function construct() {
        parent::construct();
    }
    /**
     * used for listing info of users from database 
     * @return array
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_email_template.title';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();
        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            $sort_by = 'd_email_template.title';
            $order = $post['order'][0]['dir'];
        }
//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
        $sql_count = '';
        $this->db->select('*');
        if ($search_text) {
            $this->db->like('title', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get('d_email_template');
       $total_orders = array();
       $total_orders = $query->result();
        $this->db->select('id');
        $this->db->from('d_email_template');
        if ($search_text) {
            $this->db->like('title', "%" . $search_text . "%");
        }
        $total_display_records = $this->db->count_all_results();  
        $final = array();
        if ($total_orders) {
            foreach ($total_orders as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['title'] = $row->title; 
                $singleListArray['subject'] = $row->subject;
                $singleListArray['content'] = $row->content;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
     /**
     * used for inserting information of email into database
     * @param array $data
     */
    function insert_data($data) {
        $this->db->insert('d_email_template', $data);
    }

    /**
     * used for update the data of users
     * @param array $data
     * @param int $id
     */
    function edit($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('d_email_template', $data);
    }

    /**
     * used for retrieved the specific data from database
     * @param int $id
     * @return array
     */
    function get($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('d_email_template');
        return $query->row();
    }
  /**
   * 
   * @param integer $id
   */
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('d_email_template');
    }
}