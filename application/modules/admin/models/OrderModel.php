<?php

/**
 * Super class CI_Model
 * @category UserModel 
 */
class OrderModel extends CI_Model {

    public function construct() {
        parent::construct();
    }

    /**
     * used for listing info of users from database 
     * @return array
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_user_order.created_date';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }
        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            $sort_by = 'd_user_order.created_date';
            $order = $post['order'][0]['dir'];
        }

//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }

        $sql_count = '';
        $this->db->select('*');
        if ($search_text) {
            $this->db->like('id', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get('d_user_order');
        $total_orders = array();
        $total_orders = $query->result();
        $this->db->select('id');
        $this->db->from('d_user_order');
        if ($search_text) {
            $this->db->like('id', "%" . $search_text . "%");
        }
        $total_display_records = $this->db->count_all_results();
        $final = array();
        if ($total_orders) {
            foreach ($total_orders as $row) {
                $singleListArray = array();
                $singleListArray['order_id'] = $row->id;
                $date = date('d M , o',strtotime($row->created_date));
                $singleListArray['date'] = $date;
                if ($row->status == 'P') {
                    $row->status = 'Pending';
                } else if ($row->status == 'O') {
                    $row->status = 'Processing';
                } else if ($row->status == 'S') {
                    $row->status = 'Shipped';
                } else {
                    $row->status = 'Delivered';
                }
                $singleListArray['status'] = $row->status;
                if ($row->payment_gateway_id == '1') {
                    $row->payment_gateway_id = 'Cash on delivery';
                } else {
                    $row->payment_gateway_id = 'Paypal';
                }
                $singleListArray['payment_gateway'] = $row->payment_gateway_id;
                $money = '$' . round($row->shipping_charges, 2);
                $singleListArray['shipping_charges'] = $money;
                $singleListArray['id'] = $row->id;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }

    /**
     * 
     * @param integer $id
     * @return array
     */
    function get_order_details($id){
        $id = $id;
        $this->db->select('a.address,u.firstname,u.lastname,u.email,a.address1,a.city,a.pin_code,o.grand_total,o.created_date,o.status,o.payment_gateway_id,o.shipping_charges,s.name AS state,c.name AS country,a.mobile_number,d.amount AS price,d.quantity,p.name,i.name AS image,cu.percent_off');
        $this->db->from('d_user_order AS o');
        $this->db->join('d_users_address AS a', 'o.billing_address_id=a.id', 'left');
        $this->db->join('states AS s', 'a.state_id=s.id', 'left');
        $this->db->join('countries AS c', 'a.country_id=c.id', 'left');
        $this->db->join('d_order_datails AS d', 'o.id=d.order_id', 'left');
        $this->db->join('d_product AS p', 'd.product_id=p.id', 'left');
        $this->db->join('d_product_images AS i', 'd.product_id=i.product_id', 'left');
        $this->db->join('d_coupon AS cu', 'o.coupon_id=cu.id', 'left');
        $this->db->join('d_users AS u', 'o.user_id = u.id', 'left');
        $this->db->where('o.id', $id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    function update_status($status, $id) {
        $this->db->set('status', $status);
        $this->db->where('id', $id);
        $this->db->update('d_user_order');
    }

    function get_status($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('d_user_order');
        return $query->row();
    }

}
