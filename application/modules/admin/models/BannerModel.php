<?php

/**
 * Super class CI_Model
 * @category UserModel 
 */
class BannerModel extends CI_Model {

    public function construct() {
        parent::construct();
    }

    /**
     * used for listing info of users from database 
     * @return array
     */
    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_banners.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            $sort_by = 'd_banners.name';
            $order = $post['order'][0]['dir'];
        }

//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }

        $sql_count = '';
        $this->db->select('id,name,status,image');
        if ($search_text) {
            $this->db->like('name', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get('d_banners');
       $total_banners = array();
       $total_banners = $query->result();
        $this->db->select('id');
        $this->db->from('d_banners');
        if ($search_text) {
            $this->db->like('name', "%" . $search_text . "%");
        }
        $total_display_records = $this->db->count_all_results();  
        $final = array();
        if ($total_banners) {
            foreach ($total_banners as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['name'] = $row->name;
                $singleListArray['status'] = $row->status ? 'Active' : 'Inactive';
                $singleListArray['image'] = $row->image;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }

    /**
     * used for inserting information of users into database
     * @param array $data
     */
    function insert_data($data) {
        $this->db->insert('d_banners', $data);
    }
    /**
     * used for update the data of users
     * @param array $data
     * @param int $id
     */
    function edit($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('d_banners', $data);
    }
    /**
     * used for retrieved the specific data from database
     * @param int $id
     * @return array
     */
    function get($id) {

        $this->db->select('id,name,image,status');
        $this->db->where('id', $id);
        $query = $this->db->get('d_banners');
        return $query->row();
    }
   /**
    * 
    * @param int $id
    */
    function delete($id,$name) {
        $this->db->where('id', $id);
        $this->db->delete('d_banners');
        unlink('public/banner/'.$name);

    }

}
