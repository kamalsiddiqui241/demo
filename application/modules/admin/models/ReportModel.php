<?php

/**
 * Super class CI_Model
 * @category UserModel 
 */
class ReportModel extends CI_Model {
    public function construct() {
        parent::construct();
    }
   function get_sale_information() {
   $this->db->select('SUM(grand_total) as total,month(created_date) as month');
   $this->db->group_by('month(created_date)');
   $this->db->where('year(created_date)', date('Y'));
   $query = $this->db->get('d_user_order');
//    echo $this->db->last_query();exit;
   return $query->result();
   }
  function get_registration_information(){
   $this->db->select('COUNT(id) as count,month(created_date) as month');
   $this->db->group_by('month(created_date)');
   $this->db->where('year(created_date)', date('Y'));
   $query = $this->db->get('d_users');
//    echo $this->db->last_query();exit;
   return $query->result(); 
  }
  function get_coupon_information(){
    $this->db->select('COUNT(id) as count,month(created_date) as month');
   $this->db->group_by('month(created_date)');
   $this->db->where('year(created_date)', date('Y'));
   $query = $this->db->get('d_coupons_used');
//    echo $this->db->last_query();exit;
   return $query->result();    
  }
}
