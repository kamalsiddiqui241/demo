<?php

class CategoryModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function listing() {
        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'c.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();
        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }
        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
            $sort_by = 'c.name';
            $order = $post['order'][0]['dir'];
        }
//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];
        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }
        $sql_count = '';
        //        SELECT c.id,c.name,c1.id , c1.name as parent FROM d_category AS c 
//        left join d_category as c1
//        on c.parent_id = c1.id
        $this->db->select('c.id,c.name,c.status,c1.id AS parent_id,c1.name AS parent');
        $this->db->from('d_category AS c');
        $this->db->join('d_category AS c1','c.parent_id=c1.id','left');
        if ($search_text) {
            $this->db->like('c.name', $search_text);
        }
        $this->db->order_by($sort_by, $order);
        if ($limit) {
            $this->db->limit($limit, $offset);
        }      
        $query = $this->db->get();
        $total_value = array();
        $total_value = $query->result();
        $this->db->select('id');
        $this->db->from('d_category');
        if ($search_text) {
            $this->db->like('name', $search_text);
        }
        $total_display_records = $this->db->count_all_results();
        $final = array();
        if ($total_value) {
            foreach ($total_value as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['name'] = $row->name;
                $singleListArray['parent'] = $row->parent;
                if($row->parent_id==0){
                    $singleListArray['parent'] ='No Parent'; 
                }
                $singleListArray['status'] = $row->status ? 'Active' : 'Inactive';
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }

    function insert($data) {

        $this->db->query('call addCategory("' . $data['name'] . '",' . $data['status'] . ',' . $data['parent_id'] . ',' . $data['created_by'] . ')');
    }
    function get($id) {
        $this->db->select('id,name,status,parent_id');
        $this->db->where('id', $id);
        $query = $this->db->get('d_category');
        return $query->row();
    }
    function edit($data, $id) {
        $this->db->query('call editCategory("' . $data['name'] . '",' . $data['status'] . ',' . $data['parent_id'] . ',' . $data['created_by'] . ',' . $id . ')');
    }
    function delete($id) {
        if (!empty($id)) {
            $this->db->query('call deleteCategory(' . $id . ')');
        }
    }
   function category_list() {
        $this->db->select('name,id');
        $query = $this->db->get("d_category");
        return $query->result();
    }

}
