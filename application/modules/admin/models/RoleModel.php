<?php

class RoleModel extends CI_model {

    public function construct() {
        parent::construct();
    }

    function listing() {

        $post = $this->input->post();
        $search_text = '';
        $sort_by = 'd_roles.name';
        $order = 'asc';
        $offset = 0;
        $limit = 0;
        $total_display_records = 0;
        $finalJsonArray = array();

        if (array_key_exists('search', $post)) {
            $search_text = trim($post['search']['value']);
        }

        if (array_key_exists('order', $post)) {
            $sort_by = $post['order'][0]['column'];
                $sort_by = 'd_roles.name';
            $order = $post['order'][0]['dir'];
        }

//        //Set Limit / Offset Params
        if (array_key_exists('start', $post))
            $offset = $post['start'];

        if (array_key_exists('length', $post)) {
            if ($post['length'] != -1)
                $limit = $post['length'];
        }

        $sql_count = '';
        
//        set @i=0;
//select * from (SELECT @i:=@i+1 as sno, `id`, `name`
//FROM `d_roles`
//ORDER BY `d_roles`.`name` ASC
// LIMIT 5) as data       
        $this->db->select('id,name');
        if($search_text){
            $this->db->like('name',$search_text);
        }
        $this->db->order_by($sort_by,$order);
        if($limit){
         $this->db->limit($limit,$offset);
        }
        $query = $this->db->get('d_roles');
        $total_roles = array();
        $total_roles = $query->result();
        
        $this->db->select('id');
        $this->db->from('d_roles');
        if($search_text){
          $this->db->like('name',$search_text);  
        }
//        echo "<pre>";
//    echo $this->db->last_query();
//    exit;
        $total_display_records = $this->db->count_all_results();
        
       
        $final = array();
         if ($total_roles) {
            foreach ($total_roles as $row) {
                $singleListArray = array();
                $singleListArray['id'] = $row->id;
                $singleListArray['role'] = $row->name;
//                $singleListArray['r'] = $row->id;
                $final[] = $singleListArray;
            }
        }
        $finalJsonArray['draw'] = $post['draw'];
        $finalJsonArray['recordsTotal'] = $total_display_records;
        $finalJsonArray['recordsFiltered'] = $total_display_records;
        $finalJsonArray['data'] = $final;
        return $finalJsonArray;
    }
     function insert($data) {
        $this->db->insert('d_roles', $data);
    }
      /**
     * used for update the data of users
     * @param array $data
     * @param int $id
     */
    function edit($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('d_roles', $data);
    }
      /**
     * used for retrieved the specific data from database
     * @param int $id
     * @return array
     */
    function get_user($id) {
        $this->db->select('id,name');
        $this->db->where('id', $id);
        $query = $this->db->get('d_roles');
        return $query->row();
    }   
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('d_roles');
    }



}
