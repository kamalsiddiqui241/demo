<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 * @category UserModel 
 */
class CmsModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
/**
     * 
     * @return array
     */
    function listing_cms($page_name){
        $this->db->select('*');
        $this->db->where('page_name',$page_name);
        $query=$this->db->get('d_cms');
        return $query->row();
    }
}