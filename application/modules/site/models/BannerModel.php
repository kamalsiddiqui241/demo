<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 */
class BannerModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    /**
     * 
     * @return array
     */
    function get_banner(){
        $this->db->select('*');
        $this->db->where('status','1');
        $query = $this->db->get('d_banners');
        return $query->result();
    }
    /**
     * 
     * @return array
     */
    function get_header_contact(){
        $this->db->select('*');
        $this->db->where('name','kamal.siddiqui@wwindia.com');
        $query = $this->db->get('d_configuration');
        return $query->row();
    }
}