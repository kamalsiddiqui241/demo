<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 */
class LoginModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    /**
     * 
     * @param array $data
     * @return boolean
     */
    function login($data){
        $password = $data['password'];
        $email = $data['email'];
        $condition = array('email' => $email, 'password' => MD5($password),'role_id' => '5');
        $this->db->select('*');
        $this->db->where($condition);
        $query = $this->db->get('d_users');   
        if ($query->num_rows() == 1) {
            return $query->row();       
        } else {
            return false;
        }
    }
     /**
     * used for inserting information of users into database
     * @param array $data
     */
    function insert_data($data) {
        $temp = $data['password'];
        $data['password'] = md5($temp);
        $data['role_id'] = 5;
        $this->db->insert('d_users', $data);
        return true;
    }
    function checkUser($email){
     $this->db->select('email');
     $this->db->where('email',$email);
     $query = $this->db->get('d_users');
     $result = $query->row();
     if($result){
         return 'true';
     }else{
         return 'false';
     }
    }
    /**
     * 
     * @param text $email
     * @param integer $fb_token
     */
    function insert_fb_token($email,$fb_token){
       $this->db->set('fb_token',$fb_token);
       $this->db->where('email',$email);
       $this->db->update('d_users');
//        echo $this->db->last_query();exit;
    }
    /**
     * 
     * @param array $userData
     * @param text $new_password
     */
    function insert_data_fb_google($userData,$new_password){
//        echo $new_password;exit;
    $userData['password'] = md5($new_password);   
    $userData['role_id'] =  5;
    $this->db->insert('d_users',$userData);
    }
    /**
     * 
     * @param text $email
     * @return array
     */
    function get_user_data($email){
     $this->db->select('*');
     $this->db->where('email',$email);
     $query = $this->db->get('d_users');
     return $query->row();
    }
   /**
    * 
    * @param text $email
    * @param text $google_token
    */
     function insert_google_token($email,$google_token){
       $this->db->set('google_token',$google_token);
       $this->db->where('email',$email);
       $this->db->update('d_users');
//        echo $this->db->last_query();exit;
    }
}    