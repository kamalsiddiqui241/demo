<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 */
class CategoryModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
/**
 * 
 * @return array
 */
    function get_category() {
        $this->db->select('c.id,c.name,c.status,c1.id AS parent_id,c1.name AS parent,');
        $this->db->from('d_category AS c');
        $this->db->join('d_category AS c1','c.parent_id=c1.id','left');
        $this->db->order_by('parent');
        return $this->db->get()->result_array();
    }
    /**
     * 
     * @return array
     */
    function get_parent_category(){
        $this->db->select('*');
        $this->db->where('parent_id',0);
        $query = $this->db->get('d_category');
        return $query->result();
    }
}
