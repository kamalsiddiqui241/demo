<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 */
class UserModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    /**
     * 
     * @return array
     */
    function listing() {
        $user_id = $this->session->userdata('customer')['id'];
        $this->db->select('d.id,d.firstname,d.lastname,d.address,d.address1,d.pin_code,d.city,d.primary_address,c.name AS country,s.name AS state,d.state_id,d.country_id');
        $this->db->from('d_users_address AS d');
        $this->db->join('countries AS c', 'd.country_id=c.id');
        $this->db->join('states AS s', 'd.state_id=s.id');
        $this->db->where('d.user_id',$user_id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }
    /**
     * 
     * @param array $data
     */
    function insert($data) {
        if ($data['primary_address'] == 1) {
            $this->db->set('primary_address', '0');
            $this->db->where('primary_address', '1');
            $this->db->update('d_users_address');
        }
        $this->db->insert('d_users_address', $data);
    }
    /**
     * 
     * @param array $data
     * @param integer $id
     */
    function edit($data, $id) {
        if ($data['primary_address'] == 1) {
            $this->db->set('primary_address', '0');
            $this->db->where('primary_address', '1');
            $this->db->update('d_users_address');
        }
        $this->db->where('id', $id);
        $this->db->update('d_users_address', $data);
    }

    /**
     * used for retrieved the specific data from database
     * @param int $id
     * @return array
     */
    function get_address($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('d_users_address');
        return $query->row();
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('d_users_address');
    }
    /**
     * 
     * @return array
     */
    function get_countries() {
        $this->db->select('*');
        $query = $this->db->get('countries');
        return $query->result();
    }
    /**
     * 
     * @return array
     */
    function get_states() {
        $this->db->select('*');
        $query = $this->db->get('states');
        return $query->result();
    }
    /**
     * 
     * @param int $country_id
     * @return array
     */
    function get_specific_state($country_id){
      $this->db->select('name,id'); 
      $this->db->where('country_id',$country_id);
      $query = $this->db->get('states');
//       echo $this->db->last_query();exit;
      return $query->result();
    }
}
