<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 */
class PasswordModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    /**
     * 
     * @param text $email
     * @return array
     */
    function get($email){
        $this->db->select('*');
        $this->db->where('email',$email);
        $query = $this->db->get('d_users');
        return $query->row();
    }
    /**
     * 
     * @param int $rs
     * @param text $email
     */
    function set($rs,$email){
         $condition = array('password'=>MD5($rs));
            $this->db->where('email', $email);
            $this->db->update('d_users', $condition);
    }
}