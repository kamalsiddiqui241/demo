<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class CI_Model
 * @category UserModel 
 */
class EmailModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    /**
     * 
     * @param text $new_password
     * @param array $result
     * @param text $title
     */
  function send_mail($send_data){
        $this->load->library('email'); 
        $data = array();
        $this->db->select('*');
        $this->db->where('name','Kamal Siddiqui');
        $query = $this->db->get('d_configuration');
        $data = $query->row();
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'smtp.wwindia.com';
        $config['smtp_port']    = '25';
        $config['smtp_user']    = 'kamal.siddiqui@wwindia.com';
        $config['smtp_pass']    = 'wa8LHVtHi-k)';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not  
//        pr($send_data);
        $this->email->initialize($config);
        $this->email->from($data->value, $data->name);
        $this->email->to($send_data['to_email']); 
        $this->email->subject($send_data['subject']);
        $this->email->message($send_data['body']); 
        if($this->email->send()){
            echo $this->email->print_debugger();
            return 'true';
        }else{
            return 'false';
        }  
    }
    /**
     *@param text $title
     * @return array
     */
    function template($title){
        $this->db->select('content,subject');
        $this->db->where('title',$title);
        $query=$this->db->get('d_email_template');
        return $query->row();
    }  
}