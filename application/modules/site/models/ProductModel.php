<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class CI_Model
 * @category UserModel 
 */
class ProductModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * used to listing the product from database
     * @return array 
     */
    function get_product($limit = false) {
        $condition = array('p.status' => '1', 'p.is_featured' => '1');
        $this->db->select('p.id,p.name AS name,p.quantity,p.price,i.name AS image,p.long_description,p.status,p.is_featured');
        $this->db->from('d_product AS p');
        $this->db->join('d_product_images AS i', 'p.id=i.product_id', 'left');
        $this->db->where($condition);
        if($limit){
            $this->db->limit($limit);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param int $id
     * @param int $limit
     * @return array
     */
    function get_child_category($id, $limit = FALSE) {
//       echo $id;
//      $value = array();
        $condition = " parent_id='$id' OR id='$id'";
        $this->db->select('GROUP_CONCAT(id) AS id');
        $this->db->where($condition);
        $query = $this->db->get('d_category');
//      echo $this->db->last_query();exit;
        $data = $query->row();
        $category_ids = explode(",", $data->id);
        $this->db->select('GROUP_CONCAT(product_id) AS product_id');
        $this->db->where_in('category_id', $category_ids);
        $query = $this->db->get('d_product_categories');
        $value = $query->row();
//      echo $this->db->last_query();exit;
//      pr($value);
//      echo $this->db->last_query();exit;
        $page = explode(",", $value->product_id);
        $condition = array('p.status' => '1');
        $this->db->select('p.name,p.status,p.quantity,p.price,p.id,i.name AS image');
        $this->db->from('d_product AS p');
        $this->db->join('d_product_images AS i', 'p.id=i.product_id');
        $this->db->where_in('p.id', $page);
        $this->db->where($condition);
        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    function get_detail($id){
        $this->db->select('p.id,p.quantity,p.name AS name,p.price,i.name AS image,p.long_description,p.status,p.is_featured');
        $this->db->from('d_product AS p');
        $this->db->join('d_product_images AS i', 'p.id=i.product_id', 'left');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * 
     * @param array $data
     * @return array
     */
    function insert($data) {
        return $this->db->insert('d_users_wish_list', $data);
    }

    /**
     * used to display list
     * @return boolean
     */
    function wishlist_listing() {
        $user_id = $this->session->userdata('customer')['id'];
        $this->db->select('u.id,p.id AS product_id,p.name,p.price,i.name AS image');
        $this->db->from('d_users_wish_list AS u');
        $this->db->where('u.user_id', $user_id);
        $this->db->join('d_product AS p', 'u.product_id=p.id');
        $this->db->join('d_product_images AS i', 'u.product_id=i.product_id');
        $query = $this->db->get();
//       echo $this->db->last_query();exit;
        $result = $query->result();
        if(empty($result)){
            return 'false';
        }else{
            return $result;
        }
    }

    /**
     * 
     * @param int $id
     * @return boolean
     */
    function deleteWishlist($id) {
        $this->db->where('id', $id);
        $status = $this->db->delete('d_users_wish_list');
        return $status;
    }
    /**
     * 
     * @param int $id
     * @return boolean
     */
     function delete_bulk_Wishlist($id) {
        $this->db->where('product_id', $id);
        $status = $this->db->delete('d_users_wish_list');
        return $status;
    }
    /**
     * 
     * @param int $user_id
     * @return array
     */
    function checkoutListing($user_id) {
        $condition = array('u.id' => $user_id);
        $this->db->select('a.firstname,a.pin_code,a.mobile_number,a.company_name,a.middle_name,a.lastname,a.city,a.address1,a.address,u.email,u.password,s.name AS state,s.id AS state_id,c.id AS country_id,c.name AS country');
        $this->db->from('d_users AS u');
        $this->db->join('d_users_address AS a', 'u.id=a.user_id AND a.primary_address=1', 'left');
        $this->db->join('states AS s', 'a.state_id=s.id', 'left');
        $this->db->join('countries AS c', 'a.country_id=c.id', 'left');
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * 
     * @return array
     */
    function paymentInfo_listing() {
        $this->db->select('*');
        $query = $this->db->get('d_payment_gateway');
//         echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param array $data
     * @return integer
     */
    function edit($data) {
        $this->db->set('primary_address', '0');
        $this->db->where('primary_address', '1');
        $this->db->update('d_users_address');
        $this->db->insert('d_users_address', $data);
        return $this->db->insert_id();
    }

    /**
     * 
     * @param array $page
     * @return integer
     */
    function insert_to_checkout($page) {
        $this->db->insert('d_user_order', $page);
//            echo $this->db->last_query();exit;
        return $this->db->insert_id();
    }

    /**
     * 
     * @param int $code
     * @return array
     */
    function get_coupon_info($code) {
        $this->db->select('no_of_uses');
        $this->db->where('code',$code);
        $query = $this->db->get('d_coupon');
        $result = $query->row();
        $no_of_uses = $result->no_of_uses;
        if($no_of_uses>0){
        $this->db->select('d.code,d.id,d.status,d.percent_off,u.id AS order_id');
        $this->db->from('d_coupon AS d');
        $this->db->join('d_user_order AS u','d.id=u.coupon_id','left');
        $this->db->where('code', $code);
        $query = $this->db->get();
//         echo $this->db->last_query();exit;
        return $query->row();
        }
    }
    /**
     * 
     * @param array $data
     */
   function decreased_coupon_used($data){
//       pr($data);
       $no_of_uses=$data->no_of_uses-1;
       $this->db->set('no_of_uses',$no_of_uses);
       $this->db->where('code',$data->code);
       $this->db->update('d_coupon');
//      echo $this->db->last_query();exit;
   }
   /**
    * 
    * @param int $id
    * @return array
    */
   function get_product_quantity($id){
       $this->db->select('*');
       $this->db->where('order_id',$id);
       $query = $this->db->get('d_order_datails');
       return $query->result();
   }
   /**
    * 
    * @param array $data
    */
   function decreased_item_quantity($data){
//       pr($data);
       foreach($data as $page){          
         $this->db->select('quantity');
         $this->db->where('id',$page->product_id);
         $query = $this->db->get('d_product');
        $result = $query->row();
//        pr($query->row());
         $quantity =  $result->quantity;
//        pr($quantity);
       $item_qty = $page->quantity;
       if($item_qty>0){
       $quantity = $quantity-$item_qty;
       $this->db->set('quantity',$quantity);
       $this->db->where('id',$page->product_id);
       $this->db->update('d_product');  
       }
   }
   }
    /**
     * 
     * @param array $data
     */
    function insert_coupon_used($data){
       $page = array();
//       pr($data);
       $page['coupon_id'] = $data->id;
       $page['order_id']=$data->order_id;
       $page['user_id'] = $this->session->userdata('customer')['id'];
       $page['status']=$data->status;
       $this->db->insert('d_coupons_used', $page);
    }
    /**
     * 
     * @param array $order
     */
    function insert_into_order($order) {
        $this->db->insert('d_order_datails', $order);
    }

    /**
     * 
     * @return array
     */
    function get_information() {
        $this->db->select('*');
        $query = $this->db->get('d_configuration');
//         echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param int $txn_id
     * @param int $last_id
     */
    function insert_transaction_id($txn_id, $last_id,$status) {
        $this->db->set(array('transaction_id'=>$txn_id,'status'=>$status));
        $this->db->where('id', $last_id);
        $this->db->update('d_user_order');
//        echo $this->db->last_query();exit;
    }

    /**
     * 
     * @param int $last_id
     * @return array
     */
    function get_info($last_id) {
        $this->db->select('u.grand_total,u.status,u.created_date,u.payment_gateway_id,co.no_of_uses,co.code,co.percent_off,u.shipping_charges,a.city,a.address,a.address1,a.pin_code,a.mobile_number,c.name AS country,s.name AS state');
        $this->db->from('d_user_order AS u');
        $this->db->where('u.id', $last_id);      
        $this->db->join('d_users_address AS a', 'a.id=u.billing_address_id', 'left');
        $this->db->join('d_coupon AS co', 'u.coupon_id=co.id', 'left');
        $this->db->join('countries AS c', 'a.country_id = c.id', 'left');
        $this->db->join('states AS s', 'a.state_id = s.id', 'left');
        $query = $this->db->get();
//          echo $this->db->last_query();exit;
        return $query->row();
    }
    /**
     * 
     * @param integer $last_id
     * @return array
     */
    function get_product_info($last_id){
//        echo $last_id;exit;
        $this->db->select('d.amount,d.quantity,i.name AS image,d.id');
        $this->db->from('d_order_datails AS d');
        $this->db->where('d.order_id', $last_id);      
        $this->db->join('d_product_images AS i', 'd.product_id=i.product_id', 'left');
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @return array
     */
    function my_account() {
        $id = $this->session->userdata('customer')['id'];
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('d_users');
//         echo $this->db->last_query();exit;
        return $query->row();
    }

    /**
     * 
     * @param array $data
     */
    function edit_my_account($data) {
        $id = $this->session->userdata('customer')['id'];
        $this->db->where('id', $id);
        $this->db->update('d_users', $data);
    }

    /**
     * 
     * @param array $data
     */
    function change_password($data) {
        $id = $this->session->userdata('customer')['id'];
        $password = md5($data['password']);
        $condition = array('id' => $id, 'password' => $password);
        $this->db->select('password');
        $this->db->where($condition);
        $query = $this->db->get('d_users');
//        echo $this->db->last_query();exit;
        $result = $query->row();
        if ($result) {
            $new_password = md5($data['new_password']);
            $this->db->set('password', $new_password);
            $this->db->where('password', $password);
            $this->db->update('d_users');
        }
    }

    /**
     * 
     * @return array
     */
    function get_order_info() {
        $user_id = $this->session->userdata('customer')['id'];
        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('d_user_order');
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param array $data
     */
    function contact_us($data) {
         $this->db->insert('d_contact_us', $data);
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    function get_order_details($id) {
        $id = $id;
        $user_id = $this->session->userdata('customer')['id'];
        $this->db->select('a.address,a.address1,a.city,a.pin_code,o.grand_total,o.status,o.payment_gateway_id,o.created_date,o.shipping_charges,,s.name AS state,c.name AS country,a.mobile_number,d.amount AS price,d.quantity,p.name,i.name AS image,cu.percent_off');
        $this->db->from('d_users AS u');
        $this->db->where('u.id', $user_id);
        $this->db->join('d_user_order AS o', 'u.id=o.user_id', 'left');    
        $this->db->join('d_users_address AS a', 'o.billing_address_id=a.id', 'left');
        $this->db->join('states AS s', 'a.state_id=s.id', 'left');
        $this->db->join('countries AS c', 'a.country_id=c.id', 'left');
        $this->db->join('d_order_datails AS d', 'o.id=d.order_id', 'left');
        $this->db->join('d_product AS p', 'd.product_id=p.id', 'left');
        $this->db->join('d_product_images AS i', 'd.product_id=i.product_id', 'left');
        $this->db->join('d_coupon AS cu', 'o.coupon_id=cu.id', 'left');
        $this->db->where('o.id', $id);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        return $query->result();
    }

    /**
     * 
     * @param array $data
     * @return array
     */
    function track_order($data) {
        $this->db->select('o.status');
        $this->db->from('d_users AS u');
        $this->db->where('u.email', $data['email']);
        $this->db->join('d_user_order AS o', 'u.id=o.user_id', 'left');
        $this->db->where('o.id', $data['order_id']);
        $query = $this->db->get();
        $result = $query->row();
        $page = new stdClass();
        if (empty($result)) {
            $page->status = 'B';
            return $page;
        } else {
            return $result;
        }
    }
/**
 * 
 * @return array
 */
    function get_admin_email() {
        $this->db->select('value');
        $this->db->where('name', 'Kamal Siddiqui');
        $query = $this->db->get('d_configuration');
         return $query->row();
    }
    /**
     * 
     * @return array
     */
   function product_per_day(){ 
   $created_date = date('Y-m-d 00:00:00');
   $this->db->select('o.id,o.grand_total,u.firstname,u.lastname');
   $this->db->from('d_user_order AS o');
   $this->db->where('o.created_date >=',$created_date );
   $this->db->join('d_users AS u','o.user_id=u.id','left');
   $query = $this->db->get();
//     echo $this->db->last_query();exit;
   return $query->result();
    }
    /**
     * 
     * @return array
     */
    function complete_wish_list(){
        $this->db->select('u.id,p.name,u.user_id,p.price,i.name AS image,s.firstname,s.lastname');
        $this->db->from('d_users_wish_list AS u');
        $this->db->join('d_product AS p', 'u.product_id=p.id','left');
        $this->db->join('d_users AS s', 'u.user_id=s.id','left');
        $this->db->join('d_product_images AS i', 'u.product_id=i.product_id','left');
        $query = $this->db->get();
//       echo $this->db->last_query();exit;
        return $query->result();
    }
    /**
     * 
     * @param int $id
     * @return array
     */
    function get_quantity($id){
        $this->db->select('quantity');
        $this->db->where('id',$id);
        $query=$this->db->get('d_product');
        return $query->row();
    }
    /**
     * 
     * @return integer
     */
    function count_wishlist_item(){
        $user_id = $this->session->userdata('customer')['id'];
        $this->db->select('count(id) AS count');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get('d_users_wish_list');
//         echo $this->db->last_query();exit;
        return $query->row();
    }
}
