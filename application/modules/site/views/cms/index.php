<section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                    <li class="active"><?php echo $page_name->title;?></li>
                </ol>
            </div>  
        </div>
      <div class="container">
        <div class="contact-info ">
            <p class="text_color"><?php echo $page_name->content;?></p>
        </div>
    </div>
</section>