<div class="container">
    <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Change Password</li>
            </ol>
           </div>
    <div class="col-sm-4">    	
        <div class="contact-info">
            <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                echo validation_errors();
            }
            ?></div>
             <div class=" alert-success text-center "><?php echo $this->session->flashdata('success');?></div>
            <form id="myForm" class="form-horizontal" method="post" action="">
                <div class="form-group required">
                    <div class="form-group required">
                        <div class="form-group">
                            <div class="form-group required">  
                                <div class="col-sm-10 shopper-informations">
                                        <label><span class="require">*</span> Enter Email</label>
                                       <input type="email" name="email" id="ValueEmail" placeholder="Email Address" />
                                       <div class="error"><?php echo $this->session->flashdata('message'); ?></div>
                                </div>
                            </div>
                        </div>
                             <div class="pull-left">
                                         <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/login/index'" type="button">Back</button>
                                         <button class="btn btn-primary success_forgot" type="submit">Submit</button>
                            </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>