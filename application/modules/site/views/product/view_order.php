<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Order Details</li>
            </ol>
        </div>
    </div>
    <form role="form" method="post" action="">
        <div class="form-group">
            <div class="bg">
                <div class="row">    		
                    <div class="col-sm-6">    			   		
                        <div class="contact-info">
                            <div class="shiftRight"><address>
                                    <p><?php echo $order_details[0]->address; ?></p>
                                    <p><?php echo $order_details[0]->address1; ?></p>
                                    <p><?php echo $order_details[0]->city.','.$order_details[0]->pin_code; ?></p>
                                    <p><?php echo $order_details[0]->state.','.$order_details[0]->country; ?></p>                  
                                    <p><strong>Phone:</strong><?php echo $order_details[0]->mobile_number; ?></p>
                                </address></div>
                        </div>
                    </div>
                    <div>
                        <div class="col-sm-6">    			   		
                            <div class="contact-info">
                                <div class="pull-center"><address>
                                        <p><strong>Date: </strong><?php $date = $order_details[0]->created_date; change_date_format("d M , o",$date);?></p>
                                        <p><strong>Status: </strong><?php
                                            if ($order_details[0]->status == 'P') {
                                                echo 'Pending';
                                            } else if ($order_details[0]->status == 'O') {
                                                echo 'Processing';
                                            } else if ($order_details[0]->status == 'S') {
                                                echo 'Shipped';
                                            } else {
                                                echo 'delivered';
                                            }
                                            ?></p>
                                        <p><strong>Payment Mode: </strong><?php
                                            if ($order_details[0]->payment_gateway_id == '1') {
                                                echo 'Cash on Delivery';
                                            } else {
                                                echo 'Paypal';
                                            }
                                            ?></p>
                                    </address></div>
                            </div>
                        </div>    	                                            
                    </div>                    
                </div>  
            </div> 

            <div class="container">
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item</td>
                                <td class="description"></td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                            </thead><?php $sub_total = 0 ?>
                          <?php foreach ($order_details as $order) { ?>
                            <tbody>
                                <tr>
                                    <td class="cart_product ">
                                        <h4><?php echo $order->name; ?></h4>
                                    </td>
                                    <td class="price">
                                        <a href=""><img src="<?php echo base_url(); ?>public/Image/<?php echo $order->image; ?>" class="img2" alt="No Image Found"></a>
                                    </td>
                                    <td class="price">
                                        <p>$<?php echo $order->price; ?></p>
                                    </td>
                                    <td class="total">
                                        <p><?php echo $order->quantity; ?></p>
                                    </td>
                                    <td class="total">
                                        <p>$<?php $total = ($order->quantity * $order->price);
                                       echo $total;
                                     $sub_total+=$total; ?></p>
                                    </td>
                                </tr>
                            </tbody>    
                        <?php } ?>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                            <td colspan="2">
                                <table class="table table-condensed total-result">
                                    <tr>
                                        <td>Cart Sub Total</td>
                                        <td id="sub_total" >$<?php echo $sub_total; ?></td>
                                    </tr>
                                    <tr class="shipping-cost">
                                        <td>Shipping Cost</td>
                                        <td><?php echo $order->shipping_charges; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Discount</td>
                                        <td><?php if ($order->percent_off == null) {
                                    echo '0';
                                    } else {
                                    echo $discount = (($sub_total + $order->shipping_charges) * $order->percent_off * .01);
                                    } ?></td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            <span>$<?php echo $order->grand_total; ?></span>            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                               
                    </table>
                     <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/my_orders'" type="button">Back</button>
                </div>
            </div> 
        </div> 
    </form>
</section>