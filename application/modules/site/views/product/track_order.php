<div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Track Order</li>
            </ol>
        </div>
    <div class="col-sm-4">
        <div class="well">          
            <div class="error_order_id  alert-danger"></div>
                <div class="form-group shopper-informations">
                    <label class="col-sm-4 control-label" for="input-email"><span class="require">*</span> Order Id</label>
                    <input id="order_id" class="form-control" type="text" placeholder="Order Id" value="" name="order_id">
                </div>
                <div class="form-group shopper-informations">
                    <label class="col-sm-4 control-label" for="input-password"><span class="require">*</span> Email</label>
                    <input id="email" class="form-control" type="text" placeholder="Email" value="" name="email">
                </div>
                <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home'" type="button">Back</button>
                <button class="btn btn-primary track" value= "" type="button">Track order</button>
        </div>
    </div>
    <div class="row shop-tracking-status track_display">
        <div class="col-md-12">
            <div class="well">
                <div class="  order-status">
                    <div class="order-status-timeline">
                          <!--class names: c0 c1 c2 c3 and c4--> 
                        <div class=" order-status-timeline-completion status_value"></div>
                    </div>
                    <div class="image-order-status image-order-status-new active img-circle">
                        <span class="status">Pending</span>
                        <div class="icon"></div>
                    </div>
                    <div class="image-order-status image-order-status-active active img-circle">
                        <span class="status">Processing</span>
                        <div class="icon"></div>
                    </div>
                    <div class="image-order-status image-order-status-intransit active img-circle">
                        <span class="status">Shipping</span>
                        <div class="icon"></div>
                    </div>
                    <div class="image-order-status image-order-status-delivered active img-circle">
                        <span class="status">Delivered</span>
                        <div class="icon"></div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
    var track_url = '<?php echo base_url(); ?>site/product/track_order_display';
</script>    