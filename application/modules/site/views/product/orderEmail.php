<!DOCTYPE html>
<html lang="en">
    <head>
    </head><!--/head-->
    <body>
        <div class="form-group">         
            <div class="container">
                <div class="table-responsive cart_info">
                    <table style="styleborder-collapse: collapse;border:1px solid black;">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="bg" >
                                        <div class="row">                      
                                            <div class="col-sm-6">    			   		
                                                <div class="contact-info">
                                                    <div class="shiftRight"><address>
                                                            <b><p><?php echo $order_information->address; ?></p></b>
                                                            <b><p><?php echo $order_information->address1; ?></p></b>
                                                            <b><p><?php echo $order_information->city . ',' . $order_information->pin_code; ?></p></b>
                                                            <b><p><?php echo $order_information->state . '-' . $order_information->country; ?></p></b>                       
                                                            <b><p><strong>Phone:</strong> <?php echo $order_information->mobile_number; ?></p></b>                               
                                                        </address></div>
                                                </div>
                                            </div>  
                                            <div class="col-sm-6" style="float:right;display: block;">    			   		
                                                    <div class="contact-info">
                                                        <div style="margin-top:-150px"><address>
                                                                <p><strong>Date:</strong> <?php $date = $order_information->created_date;change_date_format('d M , o',$date); ?></p>
                                                                <p><strong>Status:</strong> <?php if ($order_information->status == 'P') {
                                                                  echo '<b>Pending</b>';
                                                                 } else if ($order_information->status == 'O') {
                                                                      echo '<b>Processing</b>';
                                                                } else if ($order_information->status == 'S') {
                                                                echo '<b>Shipped</b>';
                                                                } else {
                                                                   echo '<b>delivered</b>';
                                                                 } ?></p>
                                                                <p><strong>Payment Mode:</strong> <?php if ($order_information->payment_gateway_id == '1') {
                                                                echo '<b>Cash on Delivery</b>';
                                                                } else {
                                                                echo '<b>Paypal</b>';
                                                                } ?></p>
                                                            </address></div>
                                                    </div>
                                                </div>    	                                            
                                            </div>                                              
                                        </div>  
                                    </div> 
                                </td>
                            </tr>
                        </tbody>
                        <td>
                            <table width="900px" border="0" cellspacing="0" cellpadding="0">                                  
                                <thead style="border: 1px solid black ;">
                                    <tr>
                                        <td style="border: 1px solid black ;text-align: center;" border="1" class="name"><strong>Image</strong></td>
                                        <td style="border: 1px solid black ;text-align: center;" border="1" class="price"><strong>Price</strong></td>
                                        <td style="border: 1px solid black ;text-align: center;" border="1" class="quantity"><strong>Quantity</strong></td>
                                        <td style="border: 1px solid black ;text-align: center;" border="1" class="total"><strong>Total</strong></td>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid black ;">
                                    <?php $sub_total = 0; ?>
                                    <?php
                                    foreach ($product_information as $items) {
                                        $sub_total = $sub_total + ($items->quantity * $items->amount);
                                        ?>
                                        <tr>
                                            <td style="border: 1px solid black ;" border="1">
                                                 <a href=""><img src="<?php echo base_url(); ?>public/Image/<?php echo $items->image; ?>" style="height:80px;" alt="No Image Found"></a>
                                            </td>
                                            <td style="border: 1px solid black ;text-align:center" border="1" class="cart_price">
                                                <p>$<?php echo $items->amount; ?></p>
                                            </td>
                                            <td style="border: 1px solid black ;text-align:center" border="1">
                                                <div class="cart_quantity_button">
                                                    <?php echo $items->quantity; ?>
                                                </div>
                                            </td>
                                            <td border="1" style="border: 1px solid black ;text-align:center" class="cart_total_price" id="totalprice<?php echo $items->id; ?>">
                                                <p class="cart_total_price" ><?php echo ($items->quantity * $items->amount); ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?> 
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td><strong>Cart Sub Total: </strong></td>
                                                    <td id="sub_total" >$<?php echo $sub_total; ?></td>
                                                </tr>
                                                <tr class="shipping-cost">
                                                    <td><strong>Shipping Cost: </strong></td>
                                                    <td>$<?php echo $order_information->shipping_charges; ?></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Discount: </strong></td>
                                                    <td>$<?php echo (($order_information->shipping_charges) + $sub_total - ($order_information->grand_total)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Total: </strong></td>
                                                    <td>
                                                        <span>$<?php echo $order_information->grand_total; ?></span>            
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </table>
                </div>
            </div> 
        </div>
    </body>
</html>
