<script>
    var delete_wish_url = '<?php echo base_url(); ?>site/product/deleteWishlist';
    var add_cart_url ='<?php echo base_url();?>site/product/insert_to_cart';
</script>    
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Wish List</li>
            </ol>
            <div class="empty_list"></div>
        </div>
        <?php if($wish_list== 'false'){?>
        <div id="content" class="col-sm-12 ">            
            <p class="text_color">Your WishList is empty!</p>
        </div>
       <?php }
       else { ?>
        <div class="table-responsive cart_info display_list">
            <div class="div_space pull-right"><button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url();?>site/product/bulk_add_to_cart'"  type="button">All Add to cart</button></div>
            <table class="table table-condensed">               
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description">Description</td>
                        <td></td>
                        <td>Delete</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($wish_list as $list) { ?>
                        <tr id="display_column<?php echo $list->product_id;?>">
                            <td class="cart_product img1">
                                <a href=""><img src="<?php echo base_url(); ?>public/Image/<?php echo $list->image; ?>" class="img2" alt="No Image Found"></a>
                            </td>
                            <td class="cart_description">
                                <h4> <a class="text_color" href="<?php echo base_url(); ?>site/product/get_product/<?php echo $list->product_id; ?>"><?php echo $list->name;?></a></h4>
                                <h4><p>$<?php echo $list->price;?></p></h4>
                            </td>
                            <td><?php  $is_added = is_added($list->product_id);?>
                                <input name="quantity" id="cartQuantity<?php echo $list->product_id; ?>" type="hidden" value="1" />
                                 <button type="button" id="cartbutton_wishlist<?php echo $list->product_id; ?>" class="btn btn-fefault cart              
                                            " data-id="<?php echo $list->product_id; ?>" data-name="<?php echo $list->name; ?>" data-image="<?php echo $list->image; ?>" data-price="<?php echo $list->price; ?>">      
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Add to cart
                                            </button> 
                            </td>
                            <td class="cart_delete" id="delete<?php echo $list->id; ?>">
                                <a class="wish_quantity_delete" data-id="<?php echo $list->id; ?>" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    <?php } ?> 
                </tbody>
            </table>
            <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home'" type="button">Back</button>
        </div>
       <?php } ?>
     </div>
</section>

