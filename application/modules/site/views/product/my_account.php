<div class="container">
    <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Edit Account</li>
            </ol>
        </div>
    <div class="col-sm-4">    	
        <div class="contact-info">
            <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                echo validation_errors();
            }
            ?></div>
            <form id="myForm" class="form-horizontal" method="post" action="<?php echo base_url(); ?>site/product/my_account_edit">
                <div class="form-group required">
                    <div class="form-group required">
                        <div class="form-group">
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-firstname"><span class="require">*</span> First Name</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="input-firstname" class="form-control" type="text" placeholder="First Name" value="<?php echo $my_profile->firstname; ?>" name="firstname">
                                </div>
                            </div> 

                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-lastname"><span class="require">*</span> Last Name</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="input-lastname" class="form-control" type="text" placeholder="Last Name" value="<?php echo $my_profile->lastname; ?>" name="lastname">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-email"><span class="require">*</span> Email</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="input-email" class="form-control" type="text" placeholder="Email" value="<?php echo $my_profile->email; ?>" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="buttons clearfix">
                             <div class="pull-left">
                                <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home'" type="button">Back</button>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>