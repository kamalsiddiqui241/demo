<section id="cart_items">
    <div class="form-group">
   <form role="form" method="post" action="<?php echo base_url(); ?>site/product/update_checkout_address">
       <?php if (validation_errors() != false) { ?><div class="alert alert-danger  container"><?php
                            echo validation_errors();
                        }
                        ?></div>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Checkout</li>
            </ol>
        </div>        
        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-5 clearfix">
                    <div class="bill-to">
                        <p>Bill To</p>
                        <div class="form-one">
                                <input type="text"  name="company_name" value="<?php echo $user_data->company_name; ?>" placeholder="Company Name">
                                <input type="text" name="email" value="<?php echo $user_data->email; ?>" placeholder="Email*">
                                <input type="text" value="<?php echo $user_data->firstname; ?>" name="fname" placeholder="First Name *">
                                <input type="text" name="mname" value="<?php echo $user_data->middle_name; ?>" placeholder="Middle Name">
                                <input type="text" value="<?php echo $user_data->lastname; ?>" name="lname" placeholder="Last Name *">
                                <input type="text" value="<?php echo $user_data->address; ?>" name="address1" placeholder="Address 1 *">
                                <input type="text" value="<?php echo $user_data->address1; ?>" name="address2" placeholder="Address 2">
                           
                        </div>
                        <div class="form-two">
                                <input type="text" maxlength="10" name="pin" value="<?php echo $user_data->pin_code; ?>" placeholder="Zip / Postal Code *">
                                <select id="input-country" class="form-control" name="country">
                                    <option value="">-- Country --</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country->id; ?>"<?php
                                        if ($country->id == $user_data->country_id) {
                                            echo 'selected="selected"';
                                        }
                                        ?>><?php echo $country->name; ?></option> 
                                   <?php } ?>
                                </select>
                                <select id="input-state" class="form-control" name="state">
                                    <option value="">-- State / Province / Region --</option>
                                       <?php foreach($states as $state) { ?>
                                            <option value="<?php echo $state->id;?>"<?php
                                            if ($state->id == $user_data->state_id) {
                                                echo 'selected="selected"';
                                            }?>><?php echo $state->name;?></option> 
                                            <?php } ?>                         
                                </select>
                                <input type="text" value="<?php echo $user_data->city; ?>" name="city"  placeholder="City">
                                <input type="text" value="<?php echo $user_data->mobile_number; ?>" name="phone" maxlength="12" placeholder="Phone *">
                           
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="order-message">
                        <p>Shipping Order</p>
                        <textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
                        <label><input id="s_address" value="1" name="shipping_address" checked="checked" type="checkbox"> Shipping to bill address</label>
                    </div>	
                </div>					
            </div>
        </div>
        <div class="review-payment">
            <h2>Review & Payment</h2>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php $sub_total=0;?>
                    <?php foreach ($this->cart->contents() as $items) { $sub_total= $sub_total+($items['qty'] * $items['price']); ?>
                     <tr>
                                <td class="cart_product ">
                                    <a><img src="<?php echo base_url(); ?>public/Image/<?php echo $items['image']; ?>" class="img2" alt="No Image Found"></a>
                                </td>
                                <td class="cart_description text_color">
                                    <p><?php echo $items['name']; ?></p>
                                </td>
                                <td class="cart_price text_color">
                                    <p>$<?php echo $items['price']; ?></p>
                                </td>
                                <td class="cart_quantity text_color">
                                    <div class="cart_quantity_button">
                                         <?php echo $items['qty'];?>
                                    </div>
                                </td>
                                <td class=" text_color cart_total_price" id="totalprice<?php echo $items['id']; ?>">
                                    <p class="cart_total_price" ><?php echo ($items['qty'] * $items['price']);?></p>
                                </td>
                            </tr>
                <?php } ?> 
                    <tr>
                        <td>
                            <table class="table table-condensed total-result">
                                <tr>
                                    <div class="alert alert-success coupon_success"><p>Coupon Successfully Added</p></div>
                                    <td><label>Coupon Code:</label>     
                                <div class="shopper-informations"><input id="coupon_code" class="coupon_type " type="text" value="" placeholder="Enter Coupon Code" name="coupon_code"></div>
                                <div id="error" class="error"></div>
                                <div><button type="button" id="couponBtn" class="btn btn-primary coupon">Apply</button><button type="button" id="couponBtnCancel" class="btn btn-primary coupon_cancel">Cancel</button></div>                                 
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="4">&nbsp;</td>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                     <tr>
                                        <td>Cart Sub Total</td>
                                        <td id="sub_total" data-sub_total="<?php echo $sub_total;?>">$<?php echo $sub_total;?></td>
                                    </tr>
                                <tr class="shipping-cost"><?php $shopping_cost=50;?>
                                    <td>Shipping Cost</td>
                                    <td>$<?php if($sub_total>500){ echo 'Free';}else{ $sub_total = $sub_total+50;echo $shopping_cost;}?></td>
                                </tr>
                                <tr><?php $percent_off=0?>
                                    <td>Discount</td>
                                    <td id="coupon_discount" data-percent_off="<?php echo $percent_off;?>"><?php echo $percent_off;?></td>
                                </tr>
                              <tr>
                                  <td>Total</td>
                                  <td id="sub_total1" data-sub_total="<?php echo $sub_total;?>">
                                      <span><?php echo '$'.$sub_total;?></span>            
                                  </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="payment-options">
            <?php foreach($payment_mode as $mode){?>
            <span>
                <label><input name="payment_mode" checked="checked" value="<?php echo $mode->id;?>" type="radio"><?php echo $mode->name;?></label>
            </span>           
            <?php } ?>
            <div><button type="" class="btn btn-primary">Processing</button>
               <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/product/display_cart'" type="button">Back</button></div>
        </div>
    </div><?php $id=0;?>
       <input type="hidden" id="coupon_id"  value="<?php if($id=='')echo 'null';?>" name="coupon_id_name">
       <input id="sub_total2" type="hidden" name="grand_total" value="<?php echo $sub_total;?>">
       <input type="hidden" id="shopping_id"  value="<?php if($sub_total>500){ echo "0";}else{echo "50";}?>" name="shopping_charges">
       <input type="hidden" id="discount"  name="discount" value="">
</form>
    </div>  
</section>    
<script>
    var cart_coupon_url = '<?php echo base_url(); ?>site/product/get_coupon_info';
    var cart_coupon_cancel_url = '<?php echo base_url(); ?>site/product/cancel_coupon';
</script>

