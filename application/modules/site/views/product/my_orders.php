<section id="cart_items">   
    <form  role="form" method="post" action="">
        <div class="form-group">          
            <div class="container">
                 <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">My Order</li>
            </ol>
           </div>
                       <?php if(count($order_info)=='0' ){?>
            <div id="content" class="col-sm-12 ">
            <p>Your OrderList is empty!</p>
            </div>
            <?php } else { ?>
                      <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="cart_product">Date</td>
                                <td class="price"> status</td>
                                <td class="quantity">Total</td>
                                <td class="total">View</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                           <?php foreach($order_info as $order) { ?>
                               <tr class="text_color">
                                    <td class="cart_product">
                                       <h4><?php $date = $order->created_date; change_date_format('d M , o',$date);?></h4>
                                    </td>
                                    <td class="price">
                                        <p><?php if($order->status=='O'){echo 'Processing';}else if($order->status=='P'){echo 'Pending';}else if($order->status=='S'){echo 'Shipping';}else{echo 'Delivered';} ?></p>
                                    </td>
                                     <td class="price">
                                        <p>$<?php echo $order->grand_total; ?></p>
                                    </td>
                                      <td class="total">
                                          <a href="<?php echo base_url();?>site/product/view_order/<?php echo $order->id;?>"><i class="fa fa-fw fa-user"></i></a>
                                    </td>
                                </tr>
                           <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div> 
            <?php } ?>
        </div>  
    </form>
</section>