 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		            <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Contact Us</li>
            </ol>
        </div>
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
                                        <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                        echo validation_errors();
                    }
                  ?></div>
				    	<form id="myForm" class="contact-form row" name="contact-form" method="post" action="contact_us">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control"  placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control"  placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" minlength="10" maxlength="10" name="contact" value="<?php echo set_value('contact'); ?>" class="form-control"  placeholder="Contact No">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" class="form-control" rows="8" placeholder="Your Message Here"><?php echo set_value('message'); ?></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <button class="btn btn-primary" type="submit" name="submit" class="btn btn-primary pull-right" value="">Submit</button>
                                                <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home'" type="button">Back</button>
                                                
				            </div>
				        </form>
	    			</div>
	    		</div>		
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
	