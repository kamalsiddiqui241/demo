<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php for ($count = 0; $count < $banners_count; $count++) { ?>
                            <li data-target="#slider-carousel" data-slide-to="<?php echo $count; ?>" class="<?php
                            if ($count == '0') {
                                echo'active';
                            }
                            ?>"></li>
<?php } ?> 
                    </ol>

                    <div class="carousel-inner">
                        <?php
                        $i = 0;
                        foreach ($banners as $ban) {
                            ?>
                            <div class="item <?php
                        if ($i == '0') {
                            echo 'active';
                        }
                        ?>">
                                <div class="col-sm-6">                 
                                    <img src="<?php echo base_url(); ?>public/banner/<?php echo $ban->image; ?>"  class="girl img-responsive" alt="No Image Found"/>   
                                </div>
                            </div>
    <?php $i++;
}
?>
                    </div>
                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>	
            </div>
        </div>
    </div>
</section><!--/slider-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian">
    <?php foreach ($categories as $cat) { ?>
                            <div class="panel panel-default">
    <?php if ($cat['parent_id'] == "") { ?>
                                    <div class="panel-heading">
                                        <h4 class="panel-title">                               
                                            <?php if (isset($cat['sub_categories'])) { ?>
                                                <a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $cat['name']; ?>">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                                <?php echo $cat['name']; ?>
                                                </a>    
        <?php } else { ?>
                                                <a href="<?php echo base_url(); ?>site/category/<?php echo $cat['id']; ?>">
                                                <?php echo $cat['name']; ?>
                                                </a>      
        <?php } ?>
                                        </h4>
                                    </div>
                                                <?php if (isset($cat['sub_categories'])) { ?>
                                        <div id="<?php echo $cat['name']; ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>    
                                        <?php foreach ($cat['sub_categories'] as $sub) { ?>                                      
                                                        <li><a href="<?php echo base_url(); ?>site/category/<?php echo $sub['id']; ?>"><?php echo $sub['name'] ?></a></li>
                                        <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
        <?php } ?>
    <?php } ?>
                            </div>						
                            <!--/category-products-->
<?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    <h2 class="title text-center"></h2>
                    <?php if (count($childCategory) == '0') { ?>
                        <div id="content" class="col-sm-12 ">
                            <h1 class="text_color">No Items Found</h1>
                        </div>
<?php } else {
    foreach ($childCategory AS $pro) { ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center img1">
                                            <img class="img3" src="<?php echo base_url(); ?>public/Image/<?php echo $pro->image; ?>" alt="No image Found" />
                                            <h2>$<?php echo $pro->price; ?></h2>
                                            <p><?php echo $pro->name; ?></p>
                                            <?php if ($pro->quantity == 0) { ?>    
                                                <button type="button" class="btn btn-fefault textcolor label-danger">Out Of Stock</button>                   
                                                        <?php
                                                        } else {
                                                            $is_added = is_added($pro->id);
                                                            ?>                                     
                                                <button type="button" id="cartbutton<?php echo $pro->id; ?>" class="btn btn-fefault cart <?php
                                        if ($is_added) {
                                            echo 'disabled';
                                        }
                                        ?>" data-id="<?php echo $pro->id; ?>" data-name="<?php echo $pro->name; ?>" data-image="<?php echo $pro->image; ?>" data-price="<?php echo $pro->price; ?>">
            <?php if ($is_added) { ?>
                                                        <i class="fa fa-shopping-cart"></i>
                                                        Added to cart
            <?php } else { ?> 
                                                        <i class="fa fa-shopping-cart"></i>
                                                        Add to cart
                                                    <?php } ?>
                                                </button> 
        <?php } ?>
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content ">
                                                <h2>$<?php echo $pro->price; ?></h2>
                                                <a href="<?php echo base_url(); ?>site/product/get_product/<?php echo $pro->id; ?>"><p><?php echo $pro->name; ?></p></a>
                                                    <?php if ($pro->quantity == 0) { ?>    
                                                    <button type="button" class="btn btn-fefault textcolor label-danger">Out Of Stock</button>                  
        <?php } else { ?> 
                                                    <input name="quantity" id="cartQuantity<?php echo $pro->id; ?>" type="hidden" value="1" />
                                                    <button type="button" id="cartbutton1<?php echo $pro->id; ?>" class="btn btn-fefault add-to-cart cart <?php
                                            if ($is_added) {
                                                echo 'disabled';
                                            }
                                            ?>" data-quantity="<?php echo $pro->quantity; ?>" data-id="<?php echo $pro->id; ?>" data-image="<?php echo $pro->image; ?>" data-name="<?php echo $pro->name; ?>" data-price="<?php echo $pro->price; ?>">
            <?php if ($is_added) { ?>
                                                            <i class="fa fa-shopping-cart"></i>
                                                            Added to cart
                                <?php } else { ?> 
                                                            <i class="fa fa-shopping-cart"></i>
                                                            Add to cart
            <?php } ?>
                                                    </button> 
                                    <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>	
    <?php }
} ?>      
                </div>
                
                 <?php
                 $data['parents_categories'] = $parents_categories;
                 $data['product'] = $limited_product;
//                 pr($data['product']);
                 $data['pro'] = $product; 
                 $this->load->view('/home/bottom_category.php',$data);
                 ?>
            </div>
        </div>
</section>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var add_cart_url = base_url + 'site/product/insert_to_cart';
    var category_url = base_url + 'site/product/get_sub_category';
</script>    