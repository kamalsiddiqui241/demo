<div class="container">
     <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Change Password</li>
            </ol>
           </div>
    <div class="col-sm-4">    	
        <div class="contact-info">          
            <?php if (validation_errors() != false) { ?><div class="alert alert-danger"><?php
                echo validation_errors();
            }
            ?></div>
            <form id="myForm" class="form-horizontal" method="post" action="change_password">
                <div class="form-group required">
                    <div class="form-group required">
                        <div class="form-group">
                            <div class="form-group required">
                                <label class="col-sm-4 control-label"><span class="require">*</span>Current Password</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="input-password" class="form-control" type="password" placeholder="Password" value="<?php echo set_value('password'); ?>" name="password">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" ><span class="require">*</span>New Password</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="password" class="form-control" type="password" placeholder="Enter New Password" value="<?php echo set_value('change_password'); ?>" name="change_password">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label"><span class="require">*</span>Confirm Password</label>
                                <div class="col-sm-8 shopper-informations">
                                    <input id="cpassword" class="form-control" type="password" placeholder="Enter New Password" value="<?php echo set_value('change_password'); ?>" name="c_password">
                                </div>
                            </div>
                        </div>
                        <div class="buttons clearfix">
                             <div class="pull-left">
                                         <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home/index'" type="button">Back</button>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>