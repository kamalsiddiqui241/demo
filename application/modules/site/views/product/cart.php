<section id="cart_items">
    <form>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="display_shopping_cart"></div>
        <?php if ($this->cart->total_items() > 0) { ?>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description"></td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  $sub_total=0;?>
                        <?php foreach ($this->cart->contents() as $items) {
                            $sub_total = $sub_total +($items['qty'] * $items['price']);
                            ?>
                            <tr>
                                <td class="cart_product ">
                                    <a  href=""><img src="<?php echo base_url();?>public/Image/<?php echo $items['image']; ?>" class="img2" alt="No Image Found"></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a class="text_color" href="<?php echo base_url(); ?>site/product/get_product/<?php echo $items['id']; ?>"><?php echo $items['name'];?></a></h4>
                                </td>
                                <td class="cart_price">
                                    <p>$<?php echo $items['price'];?></p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up"  data-price="<?php echo $items['price'];?>" data-id="<?php echo $items['id']; ?>" data-rowid="<?php echo $items['rowid']; ?>"  href="javascript:void(0)"> + </a>
                                        <input id="quant<?php echo $items['id']; ?>" class="cart_quantity_input" type="text" name="quantity" value="<?php echo $items['qty']; ?>" autocomplete="off" size="1">
                                        <a  class="cart_quantity_down" data-price="<?php echo $items['price']; ?>" data-id="<?php echo $items['id']; ?>" data-rowid="<?php echo $items['rowid']; ?>"  href="javascript:void(0)"> - </a>
                                    </div>
                                </td>
                                <td class="cart_total_price" id="totalprice<?php echo $items['id']; ?>">
                                    <p class="cart_total_price" ><?php echo ($items['qty'] * $items['price']);?></p>
                                </td>
                                <td class="cart_delete" id="delete<?php echo $items['rowid']; ?>">
                                    <a class="cart_quantity_delete" data-rowid="<?php echo $items['rowid'];?>" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                            <td colspan="2">
                                <table class="table table-condensed total-result">
                                    <tr>
                                        <td>Cart Sub Total</td>
                                        <td id="sub_total" data-sub_total="<?php echo $sub_total;?>">$<?php echo $sub_total;?></td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td id="sub_total1"><span>$<?php echo $sub_total;?></span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="pull-right">
                     <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/home'" type="button">Continue Shopping</button>
                    <a class="btn btn-primary" href="<?php echo base_url();?>site/product/display_checkout">Checkout</a>
                </div>
            </div>
        <?php } else { ?>
            <div id="content" class="col-sm-12 display_shopping_cart">
                <p class="text_color">Your shopping cart is empty!</p>
            <?php } ?>
        </div>
    </div>
</form>
</section> <!--/#cart_items-->
<script>
    var cart_update_url = '<?php echo base_url(); ?>site/product/updateCart';
    var cart_delete_url = '<?php echo base_url(); ?>site/product/deleteCart';
</script>
