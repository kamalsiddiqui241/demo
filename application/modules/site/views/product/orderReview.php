<?php // pr($product_information);?>
<section id="cart_items">
    <form role="form" method="post" action="">
        <div class="form-group">
            <div class="bg">
                <div class="row">                      
                    <div class="col-sm-12">    			   		
                        <div class="contact-info">
                            <div class="container">
                            <div class="breadcrumbs">
                                <ol class="breadcrumb">
                                    <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                                    <li class="active">Order View</li>
                                </ol>
                            </div>
                            </div>  
                            <div class="shiftRight"><address>
                                    <p><?php echo $order_information->address; ?></p>
                                    <p><?php echo $order_information->address1; ?></p>
                                    <p><?php echo $order_information->city.','.$order_information->pin_code; ?></p>
                                    <p><?php echo $order_information->state.','.$order_information->country ?></p>                  
                                    <p><strong>Phone:</strong> <?php echo $order_information->mobile_number; ?></p>                               
                                </address></div>
                        </div>
                    </div>    			
                </div>  
            </div> 

            <div class="container">
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item</td>
                                <td class="name">Name</td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sub_total = 0; ?>
                            <?php foreach ($product_information as $items) {
                                $sub_total = $sub_total + ($items->quantity * $items->amount);
                                ?>
                                <tr>
                                    <td class="cart_product ">
                                        <a  href=""><img src="<?php echo base_url(); ?>public/Image/<?php echo $items->image; ?>" class="img2" alt="No Image Found"></a>
                                    </td>
                                     <td class="name">
                                        <p><?php echo $items->image; ?></p>
                                    </td>
                                    <td class="cart_price">
                                        <p>$<?php echo $items->amount; ?></p>
                                    </td>                        
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
    <?php echo $items->quantity; ?>
                                        </div>
                                    </td>
                                    <td class="cart_total_price" id="totalprice<?php echo $items->id; ?>">
                                        <p class="cart_total_price" ><?php echo ($items->quantity * $items->amount); ?></p>
                                    </td>
                                </tr>
<?php } ?> 
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td colspan="2">
                                    <table class="table table-condensed total-result">
                                        <tr>
                                            <td>Cart Sub Total</td>
                                            <td id="sub_total" >$<?php echo $sub_total; ?></td>
                                        </tr>
                                        <tr class="shipping-cost">
                                            <td>Shipping Cost</td>
                                            <td><?php echo $order_information->shipping_charges; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td><?php echo (($order_information->shipping_charges) + $sub_total - ($order_information->grand_total)); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td>
                                                <span>$<?php echo $order_information->grand_total; ?></span>            
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div class="pull-right">
                    <a class="btn btn-primary" href="<?php echo base_url();?>site/home/index">Continue Shopping</a>
                </div>
                </div>
            </div> 
        </div>  
    </form>
</section>