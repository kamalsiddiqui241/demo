<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian">
                        <?php foreach ($categories as $cat) { ?>
                            <div class="panel panel-default">
                                <?php if ($cat['parent_id'] == "") { ?>
                                    <div class="panel-heading">
                                        <h4 class="panel-title">                               
                                            <?php if (isset($cat['sub_categories'])) {
                                           ; ?>
                                                <a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $cat['name']; ?>">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                                <?php echo $cat['name']; ?>
                                                </a>    
                                                <?php } else { ?>
                                                <a href="<?php echo base_url(); ?>site/category/<?php echo $cat['id']; ?>">
                                                <?php echo $cat['name']; ?>
                                                </a>      
        <?php } ?>
                                        </h4>
                                    </div>
        <?php if (isset($cat['sub_categories'])) { ?>
                                        <div id="<?php echo $cat['name']; ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>    
                                                    <?php foreach ($cat['sub_categories'] as $sub) { ?>                                      
                                                        <li><a href="<?php echo base_url(); ?>site/category/<?php echo $cat['id']; ?>"><?php echo $sub['name'] ?></a></li>
            <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } ?>
                            <?php } ?>
                            </div>	
<?php } ?>                        
                    </div><!--/category-products-->
                </div>
            </div>
            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->                    
                    <div class="col-sm-5">
                        <div class="view-product zoom" id="zoom_07">
                            <img  src="<?php echo base_url(); ?>public/Image/<?php echo $product_details->image; ?>" alt="" />
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <h2 ><?php echo $product_details->name; ?></h2>                                                               
                            <span>
                                <span>US $<?php echo $product_details->price; ?></span>
                                <label>Quantity:</label>
                                <input name="quantity" id="cartQuantity<?php echo $product_details->id; ?>" type="text" value="1" />
                                <div style="margin-left:-15px; ">
                                    <?php if($product_details->quantity==0) { ?>    
                                    <button type="button" class="btn btn-fefault cart"><del class="error"><i class="fa fa-shopping-cart"></i>Add to cart</del></button>
                                                            
                                    <?php } else {
                                           $is_aaded = is_added($product_details->id);
                                    ?>                                     
                                      <button type="button" id="cartbutton<?php echo $product_details->id;?>" class="btn btn-fefault cart <?php if($is_aaded){ echo 'disabled'; } ?>"  data-id="<?php echo $product_details->id;?>" data-image="<?php echo $product_details->image;?>" data-name="<?php echo $product_details->name; ?>" data-price="<?php echo $product_details->price;?>">
                                        <?php if($is_aaded){ ?>
                                           <i class="fa fa-shopping-cart"></i>
                                          Added to cart
                                    <?php } else {?> 
                                        <i class="fa fa-shopping-cart"></i>
                                        Add to cart
                                    <?php } ?>
                                        
                                       </button>    
                                    <?php } ?>
                                    <?php $is_added = is_added_wishlist($product_details->id,$this->session->userdata('customer')['id']);?>
                                    <button style="height:35px;margin-bottom: 10px; " data-id="<?php echo $product_details->id; ?>" id="wish<?php echo $product_details->id;?>" class="btn btn-default wish <?php if($is_added){ echo 'disabled'; } ?>" title="" data-toggle="tooltip" type="button" data-original-title="Add to Wish List">
                                        <i class="fa fa-heart"></i>
                                    </button>     
                                </div>
                            </span>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->
        
                <div class="category-tab shop-details-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li><a href="" data-toggle="tab">Description</a></li>
                        </ul>
                    </div>
                    <div class="tab-pae fade active in" id="reviews" >
                        <div class="col-sm-12 ">
                            <p><?php echo $product_details->long_description; ?></p>
                        </div>
                    </div>n
                </div>
            </div><!--/category-tab-->
        </div>
    </div>
</div>
</section>
<script>
    var add_cart_url = '<?php echo base_url();?>site/product/insert_to_cart';
    var add_wish_url = '<?php echo base_url();?>site/product/insert_to_wishlist';
</script>    