<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | E-Shopper</title>
        <?php $this->load->library('cart');?>
         <link href="<?php echo base_url();?>public/themes/eshoppers/css/status.1.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/prettyPhoto.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/price-range.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/main.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/responsive.css" rel="stylesheet">  
        <link href="<?php echo base_url();?>public/themes/eshoppers/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url();?>public/themes/adminlte/dist/css/AdminLTE.min.css" rel="stylesheet">      
        <link rel="shortcut icon" href="<?php echo base_url();?>public/themes/eshoppers/images/home/favicon.ico">
    </head><!--/head-->
    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i><?php echo $header_info->value;?></a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i><?php echo $header_info->name;?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->
            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="<?php echo base_url();?>site/home"><img src="<?php echo base_url();?>public/themes/eshoppers/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="mainmenu pull-right">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <?php if ($this->session->userdata('customer') == TRUE) {?>
                                    <li class="dropdown"><a href="#"><i class="fa fa-user"></i><?php echo ($this->session->userdata('customer')['firstname']."    ".$this->session->userdata('customer')['lastname']); ?></a>
                                        <ul role="menu" class="sub-menu">
                                            <li><a href="<?php echo base_url();?>site/user/addressbook">Address Book</a></li>
                                            <li><a href="<?php echo base_url();?>site/my_account">My Account</a></li>
                                            <li><a href="<?php echo base_url();?>site/track_order">Track Order</a></li>
                                            <li><a href="<?php echo base_url();?>site/my_orders">My Orders</a></li>
                                            <li><a href="<?php echo base_url();?>site/change_password">Change Password</a></li>
                                        </ul>
                                    </li>  
                                    <?php } ?>
                                    <?php if ($this->session->userdata('customer') == TRUE) {?><li><a class="wishlist_listing" href="<?php echo base_url();?>site/wishlist"><i class="fa fa-star"></i> Wishlist(<?php echo $quantity->count;?>)</a></li><?php } ?> 
                                    <li><a class="display_checkout" href="<?php echo base_url();?>site/checkout"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                                    <li><a class="display_cart" href="<?php echo base_url();?>site/cart"><i class="fa fa-shopping-cart"></i>Cart(<?php echo $this->cart->total_items();?>)</a></li>
                                    <?php if ($this->session->userdata('customer') == TRUE) {?>
                                    <li><a  href="<?php echo base_url();?>site/login/logout"><i class="fa fa-lock"></i>Logout</a></li>
                                   <?php } else { ?>
                                    <li><a class="login" href="<?php echo base_url();?>site/login"><i class="fa fa-lock"></i> Login</a></li>
                                 <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->
            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="<?php echo base_url();?>site/home" class="index">Home</a></li>
                                    <li><a class="contact_us" href="<?php echo base_url();?>site/contact_us">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header--> 
        
        <?php echo $content;?>   
        
        <footer id="footer"><!--Footer-->
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="companyinfo">
                                <h2><span>e</span>-shopper</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="#">
                                        <div class="iframe-img">
                                            <img src="<?php echo base_url();?>public/themes/eshoppers/images/home/iframe1.png" alt="" />
                                        </div>
                                        <div class="overlay-icon">
                                            <i class="fa fa-play-circle-o"></i>
                                        </div>
                                    </a>
                                    <p>Circle of Hands</p>
                                    <h2>24 DEC 2014</h2>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="#">
                                        <div class="iframe-img">
                                            <img src="<?php echo base_url();?>public/themes/eshoppers/images/home/iframe2.png" alt="" />
                                        </div>
                                        <div class="overlay-icon">
                                            <i class="fa fa-play-circle-o"></i>
                                        </div>
                                    </a>
                                    <p>Circle of Hands</p>
                                    <h2>24 DEC 2014</h2>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="#">
                                        <div class="iframe-img">
                                            <img src="<?php echo base_url();?>public/themes/eshoppers/images/home/iframe3.png" alt="" />
                                        </div>
                                        <div class="overlay-icon">
                                            <i class="fa fa-play-circle-o"></i>
                                        </div>
                                    </a>
                                    <p>Circle of Hands</p>
                                    <h2>24 DEC 2014</h2>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="#">
                                        <div class="iframe-img">
                                            <img src="<?php echo base_url();?>public/themes/eshoppers/images/home/iframe4.png" alt="" />
                                        </div>
                                        <div class="overlay-icon">
                                            <i class="fa fa-play-circle-o"></i>
                                        </div>
                                    </a>
                                    <p>Circle of Hands</p>
                                    <h2>24 DEC 2014</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="address">
                                <img src="<?php echo base_url();?>public/themes/eshoppers/images/home/map.png" alt="" />
                                <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-widget">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="single-widget">
                                <h2>Service</h2>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="#">Online Help</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Order Status</a></li>
                                    <li><a href="#">Change Location</a></li>
                                    <li><a href="#">FAQ’s</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="single-widget">
                                <h2>Quock Shop</h2>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="#">T-Shirt</a></li>
                                    <li><a href="#">Mens</a></li>
                                    <li><a href="#">Womens</a></li>
                                    <li><a href="#">Gift Cards</a></li>
                                    <li><a href="#">Shoes</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="single-widget">
                                <h2>Policies</h2>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="<?php echo base_url();?>site/cms/<?php echo 'terms_of_uses';?>">Terms of Use</a></li>
                                    <li><a href="<?php echo base_url();?>site/cms/<?php echo 'privecy_policy';?>">Privecy Policy</a></li>
                                    <li><a href="<?php echo base_url();?>site/cms/<?php echo 'refund_policy';?>">Refund Policy</a></li>
                                    <li><a href="#">Billing System</a></li>
                                    <li><a href="#">Ticket System</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="single-widget">
                                <h2>About Shopper</h2>
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="#">Company Information</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Store Location</a></li>
                                    <li><a href="#">Affillate Program</a></li>
                                    <li><a href="#">Copyright</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 col-sm-offset-1">
                            <div class="single-widget">
                                <h2>About Shopper</h2>
                                <form method="post" action="<?php echo base_url();?>site/mailChimp/news_letter" class="searchform">
                                    <input type="text" name="email" placeholder="Your email address" />
                                    <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                                    <p>Get the most recent updates from <br />our site and be updated your self...</p>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <p class="pull-left">2016 E-SHOPPER Inc. All rights reserved.</p>
                        <p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
                    </div>
                </div>
            </div>
        </footer><!--/Footer-->     
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/jquery.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/jquery.scrollUp.min.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/price-range.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/main.js"></script>
        <script src='<?php echo base_url()?>public/themes/eshoppers/js/jquery.growl.js' type='text/javascript'></script>
        <link href='<?php echo base_url()?>public/themes/eshoppers/css/jquery.growl.css' rel='stylesheet' type='text/css'>
        <script src="<?php echo base_url();?>public/themes/adminlte/dist/js/jquery.validate.js"></script>
        <script src="<?php echo base_url();?>public/themes/adminlte/dist/js/validate.js"></script>
        <script src="<?php echo base_url();?>public/themes/eshoppers/js/jquery.zoom.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var method_name = '<?php echo $this->router->fetch_method();?>';
                $('.'+method_name).addClass('active');
            });
        </script>
         <script type="text/javascript">
        $(document).ready(function(){
                var controller_name = '<?php echo $this->router->fetch_class();?>';
                $('.'+controller_name).addClass('active');
                $(".zoom").zoom();
            });
        </script>  
        <?php
            if(!empty($js)) {
                $theme_folder_path = base_url().'public/themes/eshoppers';
                foreach($js as $single_js) {
                    echo '<script src="'.($theme_folder_path.'/'.$single_js).'"></script>';
                }
            }
        ?>
    </body>
</html>