<div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <?php foreach ($parents_categories as $parent) { ?>
                                <li><a href="javascript:void(0)" class="sub_category" data-id="<?php echo $parent->id; ?>" id="parent<?php echo $parent->id; ?>"><?php echo $parent->name; ?></a></li>
                            <?php } ?>                     
                        </ul>
                    </div>
                    <div id="content" class="col-sm-12 error_cat">            
                        <h1 class="text_color">No Item Found</h1>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in " >                           
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center ">
                                            <div class="sub_cat">
                                                <?php foreach($product as $pro) { ?>
                                                    <span class="col-sm-3"><img class="img3" src="<?php echo base_url(); ?>public/Image/<?php echo $pro->image; ?>" alt="" />
                                                    <h2>$<?php echo $pro->price; ?></h2>
                                                    <a href="<?php echo base_url(); ?>site/product/get_product/<?php echo $pro->id; ?>"><p><?php echo $pro->name; ?></p></a>
                                                    <?php if ($pro->quantity == 0) { ?>    
                                            <button type="button" class="btn btn-fefault textcolor label-danger">Out Of Stock</button>                   
                                            <?php
                                        } else {
                                            $is_added = is_added($pro->id);
//                                            echo  $is_added;exit;
                                            ?>                                     
                                            <button type="button" id="cartbutton2<?php echo $pro->id; ?>" class="btn btn-fefault add-to-cart cart  <?php if ($is_added){echo 'disabled';}?>" 
                                                    data-id="<?php echo $pro->id; ?>" data-name="<?php echo $pro->name; ?>" data-image="<?php echo $pro->image; ?>" data-price="<?php echo $pro->price;?>">
                                                        <?php if ($is_added) { ?>
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Added to cart
                                                <?php } else { ?> 
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Add to cart
                                                <?php } ?>
                                            </button> 
                                        <?php } ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div> 
    </div>
            <script>
                var base_url = '<?php echo base_url(); ?>';
                var add_cart_url = base_url+'site/product/insert_to_cart';
                var category_url = base_url+'site/product/get_sub_category';
            </script>    