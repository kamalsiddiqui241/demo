<section id="cart_items">
    <div class="row">       
        <div class="container">
            <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Address Book</li>
            </ol>
           </div>
            <div id="content" class="col-sm-9">
                <form>
                <?php if (count($address) == '0') { ?>
                    <div id="content" class="col-sm-12 ">
                        <h1 class="text_color">No Address</h1>
                    </div>
                <?php } else { ?>
                    <?php foreach ($address AS $add) { ?>
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <td class="text-left">
                                        <?php
                                        echo ($add->firstname . '   ' . $add->lastname);
                                        echo "<br>";
                                        echo $add->address;
                                        echo "<br>";
                                        echo $add->address1;
                                        echo "<br>";
                                        echo ($add->city . '   ' . $add->pin_code);
                                        echo "<br>";
                                        echo $add->state;
                                        echo "<br>";
                                        echo $add->country;
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <a class="btn btn-info" href="<?php echo base_url() ?>site/address/edit/<?php echo $add->id; ?>">Edit</a>
                                        <a class="btn btn-danger" href="<?php echo base_url() ?>site/user/delete/<?php echo $add->id; ?>">Delete</a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    <?php } ?>         
               <?php } ?>
                <div class="buttons clearfix">
                    <div class="pull-left">
                        <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site'" type="button">Back</button>
                        <a class="btn btn-primary" href="<?php echo base_url(); ?>site/address/add">New Address</a>
                    </div>
                </div>
                </form> 
            </div>
        </div>
    </div>
</section>    
