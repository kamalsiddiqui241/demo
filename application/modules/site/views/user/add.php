<?php if (validation_errors() != false) { ?><div class="alert alert-danger container"><?php
    echo validation_errors();
}
?></div>
<div class="container">
    <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>site/home">Home</a></li>
                <li class="active">Address Book</li>
            </ol>
           </div>
    <div class="row">
        <div id="content" class="col-sm-9">
            <form id="myForm" class="form-horizontal" role="form" method="post" action="">
                <fieldset>
                    <div class="form-group required">
                        <div class="form-group required">
                            <div class="form-group">
                                <div class="form-group required">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label"><span class="require">*</span>Company Name</label>
                                    <div class="col-sm-10">
                                        <input id="input-companyname" class="form-control" type="text" placeholder="Company Name" value="<?php echo set_value('cname'); ?>" name="cname">
                                    </div>
                                </div>
                                    <label class="col-sm-2 control-label"><span class="require">*</span> First Name</label>
                                    <div class="col-sm-10">
                                        <input id="input-firstname" value="<?php echo set_value('firstname'); ?>" class="form-control" type="text" placeholder="First Name" value="" name="firstname">
                                    </div>
                                </div> 
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label"><span class="require">*</span> Last Name</label>
                                    <div class="col-sm-10">
                                        <input id="input-lastname" class="form-control" type="text" placeholder="Last Name" value="<?php echo set_value('lastname'); ?>" name="lastname">
                                    </div>
                                </div>
                               <div class="form-group required">
                                    <label class="col-sm-2 control-label">Middle Name</label>
                                    <div class="col-sm-10">
                                        <input id="input-middlename" class="form-control" type="text" placeholder="Middle Name" value="<?php echo set_value('middlename'); ?>" name="middlename">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" ><span class="require">*</span> Address</label>
                                    <div class="col-sm-10">
                                        <input id="input-address" class="form-control" type="text" placeholder="Address" value="<?php echo set_value('address'); ?>" name="address">
                                    </div>
                                </div>   
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-address-2">Address 1</label>
                                    <div class="col-sm-10">
                                        <input id="input-address-2" class="form-control" type="text" placeholder="Address 1" value="<?php echo set_value('address1'); ?>" name="address1">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label"><span class="require">*</span> City</label>
                                    <div class="col-sm-10">
                                        <input id="input-city" class="form-control" type="text" placeholder="City" value="<?php echo set_value('city'); ?>" name="city">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-country"><span class="require">*</span> Country</label>
                                    <div class="col-sm-10">
                                        <select id="input-country" class="form-control" name="country">
                                            <option value=""> --- Please Select --- </option>
                                            <?php foreach($countries as $country) {?>
                                             <option value="<?php echo $country->id;?>"><?php echo $country->name;?></option> 
                                             <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" ><span class="require">*</span> Region / State</label>
                                    <div class="col-sm-10 ">
                                        <select id="input-zone" class="form-control state_name" name="state">
                                            <option value=""> --- Please Select --- </option>
                                            <?php foreach($states as $state) { ?>
                                            <option class='first_time_state' value="<?php echo $state->id;?>"><?php echo $state->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" ><span class="require">*</span> Phone Number</label>
                                    <div class="col-sm-10">
                                        <input id="input-address" maxlength="12" class="form-control" type="text" placeholder="Phone Number" value="<?php echo set_value('mobile_number'); ?>" name="mobile_number">
                                    </div>
                                </div>   
                                 <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-address"><span class="require">*</span> Pin Code</label>
                                    <div class="col-sm-10">
                                        <input id="input-address"  maxlength="10" class="form-control" type="text" placeholder="Pin code" value="<?php echo set_value('pin'); ?>" name="pin">
                                    </div>
                                </div>   
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><span class="require">*</span> Default Address</label>
                                    <div class="col-sm-10">
                                        <label class="radio-inline">
                                            <input type="radio" name="optionsAdd" value="1" name="default">
                                            Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="optionsAdd" checked="checked" value="0" name="default">
                                            No
                                        </label>
                                    </div>
                                </div>
                                </fieldset>
                                <div class="buttons clearfix">
                                    <div class="pull-left">  
                                        <button class="btn btn-primary" onclick="window.location.href = '<?php echo base_url(); ?>site/user/index'" type="button">Back</button>
                                       <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>   
                                </form>
                            </div>
                        </div>
                    </div>  
                   <script>
                       var state_url = '<?php echo base_url();?>site/user/get_specific_state';
                   </script>