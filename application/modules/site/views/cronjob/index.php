<?php // pr($order_information);?>
<div class="container">
                <div class="table-responsive cart_info">
                    <table width="900px" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr class="cart_menu">
                                <td style="border: 1px solid black ;text-align:center" border="1" class="name"><strong>User Name</strong></td>
                                <td style="border: 1px solid black ;text-align:center" border="1" class="price"><strong>Total Amount</strong></td>
                                <td style="border: 1px solid black ;text-align:center" border="1" class="quantity"><strong>Order Id</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($order_information as $items) {
                                ?>
                                <tr>
                                    <td style="border: 1px solid black ;text-align:center" border="1" class="name">
                                        <p><?php echo $items->firstname.' '.$items->lastname; ?></p>
                                    </td>
                                    <td style="border: 1px solid black ;text-align:center" border="1" class="cart_price">
                                        <p>$<?php echo $items->grand_total; ?></p>
                                    </td>
                                    <td style="border: 1px solid black ;text-align:center" border="1" class="cart_quantity">
                                        <p>#ORD<?php echo $items->id;?></p>
                                    </td>
                                </tr>
                        <?php } ?> 
                        </tbody>
                    </table>
                </div>
            </div> 