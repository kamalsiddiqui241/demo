<section id="form">
    <div class="container">
        <div class="alert-success text-center"><strong><?php echo $this->session->flashdata('success'); ?></strong></div>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    <div class="error"><?php echo $this->session->flashdata('message'); ?></div>
                    <form id="myForm1" action="<?php echo base_url() ?>site/login/index" method="post" role="form">
                        <label><span class="error">*</span> Email</label>
                        <input type="email" name="email" id="ValueEmail" placeholder="Email Address" />
                        <div class="error"><?php echo form_error('email'); ?></div>
                        <label><span class="error">*</span> Password</label>
                        <input  type="password" name="password" id="ValuePassword" placeholder="Password"/>
                        <div class="error"><?php echo form_error('password'); ?></div>
                        <a href="<?php echo base_url(); ?>site/password/index" >I forgot my password</a>
                        <button type="submit" class="btn btn-default">Login</button>
                    </form>
                </div><!--/login form-->
                <div class="social-auth-links ">
                    <a href="<?php echo base_url();?>site/login/fb_login" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                        Facebook</a>
                    <a href="<?php echo base_url();?>site/login/google_login" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                        Google+</a>
                </div>
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->                                            
                    <h2>New User Signup!</h2>
                    <form id="myForm" action="<?php echo base_url() ?>site/login/signup" method="post" role="form">
                        <label><span class="require">*</span> First Name</label>
                        <input type="text" name="fname" value="<?php echo set_value('fname'); ?>" id="ValueFname" placeholder="First Name"/>
                        <div class="error"><?php echo form_error('fname'); ?></div>
                        <label><span class="require">*</span> Last Name</label>
                        <input type="text" name="lname" id="ValueLname" value="<?php echo set_value('lname'); ?>" placeholder="Last Name"/>
                        <div class="error"><?php echo form_error('lname'); ?></div>
                        <label><span class="require">*</span> Email</label>
                        <input type="email" name="email1" id="ValueEmail" value="<?php echo set_value('email1'); ?>" placeholder="Email Address"/>
                        <div class="error"><?php echo form_error('email1'); ?></div>
                        <label><span class="require">*</span> Password</label>
                        <input type="password" name="password1" id="ValuePassword" placeholder="Password"/>
                        <div class="error"><?php echo form_error('password1'); ?></div>
                        <label><span class="require">*</span>Confirm Password</label>
                        <input type="password" name="cpassword" id="ValueCpassword" placeholder=" Confirm Password"/>
                        <div class="require"><?php echo form_error('cpassword'); ?></div>
                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section>
