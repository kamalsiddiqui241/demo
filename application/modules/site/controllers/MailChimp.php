<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MailChimp extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('MCAPI');
    }
    /**
     * used to add news mail to mailchimp
     */
    function news_letter(){  
        $id = ITEM_ID; 
       $email_address= $this->input->post('email');
//       echo $email_address;exit;
        $retval = $this->mcapi->listSubscribe($id, $email_address,NULL, 'html', true, false, true, false); 
//       echo $this->mcapi->errorMessage;
//        var_dump($retval);
//        echo '==============';exit;
        redirect('site/home/index');
    }
}
