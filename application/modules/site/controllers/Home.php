<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class Admin_controller
 * @category HomeModel
 * @link localhost/admin/user
 */
class Home extends Site_Controller {  
    function __construct() {
        parent::__construct();
        $this->load->Model('BannerModel');
        $this->load->Model('CategoryModel');
        $this->load->Model('ProductModel');
        $this->load->library('cart');
    }   
    /**
     * to listing the category,sub_category
     * @param array $data
     */
    function index() {
//   echo CI_VERSION;exit;
//        echo "<pre>";
//        print_r($this->session->all_userdata());
        $data = array();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['banners'] = $this->BannerModel->get_banner();
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['banners_count'] = count($data['banners']);
        $data['categories'] = $this->get_category();
        $data['product'] = $this->get_product();
        $limit = 4;
        $data['limited_product']=$this->get_product_footer($limit);
        $data['parents_categories'] = $this->get_parent_categories();
        $data['js'] = array('site/cart.js');
        $this->render('index', $data);
    }
        /**
     * used to get limited product details to show in footer
     * @param integer $limit
     * @return array
     */
    function get_product_footer($limit){
      return $this->ProductModel->get_product($limit);    
    }
    /**
     * used to get the category and corresponding sub_category 
     * @return array $result
     */
    function get_category() {
        $categories =$this->CategoryModel->get_category();
        $result = array();
        foreach ($categories as $cat) {
            if ($cat['parent_id'] && array_key_exists($cat['parent_id'], $result)) {
                $result[$cat['parent_id']]['sub_categories'][] = $cat;
            } else {
                $result[$cat['id']] = $cat;
            }
        }
        return $result;
    }
    /**
     * used to get complete product list
     * @return array to index method
     */
    function get_product(){
         return $this->ProductModel->get_product();   
    }
    /**
     * used to get category_name
     * @return array
     */
    function get_parent_categories(){
        return $this->CategoryModel->get_parent_category();  
    }
}
