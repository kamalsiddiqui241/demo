<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category ProductModel
 * @link localhost/site/product
 */
class Product extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('ProductModel');
        $this->load->Model('CategoryModel');
        $this->load->Model('BannerModel');
        $this->load->Model('UserModel');
        $this->load->Model('EmailModel');
        $this->load->library('Paypal_lib');
        $this->load->library('cart');
        $this->load->helper('date');
    }
    /**
     * used to listing product information 
     * @param integer $id
     */
    function index($id) {
        $data = array();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['childCategory'] = $this->ProductModel->get_child_category($id);
        $data['categories'] = $this->get_category();
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['parents_categories'] = $this->get_parent_category();
        $data['product'] = $this->get_product_info();
        $limit = 4;
        $data['limited_product']=$this->get_product_footer($limit);
        $data['banners'] = $this->get_banner();
        $data['banners_count'] = count($data['banners']);
        $data['js'] = array('site/cart.js');
        $this->render('index', $data);
    }
    /**
     * used to get some product information
     * @param integer $limit
     * @return array
     */
    function get_product_footer($limit){
      return $this->ProductModel->get_product($limit);    
    }
    /**
     * used to get information
     * @return array
     */
     function get_product_info(){
         return $this->ProductModel->get_product();   
    }
    /**
     * @return array @category 
     */
    function get_parent_category() {
        return $this->CategoryModel->get_parent_category();
    }
    /**
     * used to list the category
     * @return array $result
     */
    function get_category() {
        $categories = $this->CategoryModel->get_category();
        $result = array();
        foreach ($categories as $cat) {
            if ($cat['parent_id'] && array_key_exists($cat['parent_id'], $result)) {
                $result[$cat['parent_id']]['sub_categories'][] = $cat;
            } else {
                $result[$cat['id']] = $cat;
            }
        }
        return $result;
    }
    /**
     * used to get banner information
     * @return array banners
     */
    function get_banner() {
        return $this->BannerModel->get_banner();
    }
    /**
     * used to listing product which in cart
     * @param integer $id
     */
    function get_product($id) {
        $data = array();
//        pr($this->cart->contents());
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['product_details'] = $this->ProductModel->get_detail($id);
        $data['childCategory'] = $this->ProductModel->get_child_category($id);
        $data['categories'] = $this->get_category();
        $data['parents_categories'] = $this->get_parent_category();
        $data['js'] = array('site/cart.js');
        $this->render('detail', $data);
    }
    /**
     * used to insert to cart
     * return array $data
     */
    public function insert_to_cart() {
        $data = array(
            'image' => $this->input->post('image'),
            'id' => $this->input->post('id'),
            'qty' => $this->input->post('quantity'),
            'price' => $this->input->post('price'),
            'name' => $this->input->post('name'),
        ); 
        $product_id = $this->input->post('id');
        $this->cart->insert($data);
        $result = $this->ProductModel->delete_bulk_Wishlist($product_id);
        $value = $this->ProductModel->count_wishlist_item();
        $status = array('status' => TRUE, 'cart_count' => $this->cart->total_items(),'quantity'=> $value->count); 
        echo json_encode($status);exit;
    }
    /**
     * used to add multiple items to cart
     * @param array $data
     */
    public function bulk_add_to_cart(){
        $wish_list = $this->ProductModel->wishlist_listing();
        foreach($wish_list as $list){
            $data = array(
            'image' => $list->image,
            'id' => $list->product_id,
            'qty' => 1,
            'price' => $list->price,
            'name' => $list->name,
        ); 
            $product_id = $list->product_id;
        $this->cart->insert($data);
        $result = $this->ProductModel->delete_bulk_Wishlist($product_id);
        }
        redirect('site/product/wishlist_listing');
    }
    /**
     * used to display cart
     * return array $data
     */
    function display_cart() {
        $data['js'] = array('site/cart.js');
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $this->render('cart', $data);
    }

    /**
     * used to update cart
     * return true
     */
    function updateCart() {
        $id = $this->input->post('id');
        $data = array(
            'rowid' => $this->input->post('rowid'),
            'qty' => $this->input->post('quantity'),
        );
        $result = $this->ProductModel->get_quantity($id);
        if($result->quantity>=$data['qty']){
        $this->cart->update($data);
        $status = array('status' => TRUE,'cart_count' => $this->cart->total_items());
        echo json_encode($status);exit;
        }else{
        $status = array('status' => FALSE);
        echo json_encode($status);exit;
        }
    }

    /**
     * used to delete cart
     * return true
     */
    function deleteCart() {
        $rowid = $this->input->post('rowid');
        $sub_total = $this->input->post('sub_total');
        $total = 0;
        foreach($this->cart->contents() as $items){
            if($rowid==$items['rowid']){
             $total = ($items['qty'] * $items['price']);
           }
        }
         $sub_total = $sub_total-$total;
//         echo $sub_total;exit;
        $this->cart->remove($rowid);
        $status = array('status' => TRUE,'sub_total'=> $sub_total ,'cart_count' => $this->cart->total_items());       
        echo json_encode($status);exit;
    }
    /**
     * insert items into wish list
     * return true or false
     */
    function insert_to_wishList() {
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $data['user_id'] = $this->session->userdata('customer')['id'];
            $data['product_id'] = $this->input->post('product_id'); 
            $result = $this->ProductModel->insert($data);
            $value = $this->ProductModel->count_wishlist_item();
            $data['header_info'] = $this->BannerModel->get_header_contact();
            if ($result) {
               $status = array('status' => TRUE,'quantity'=>$value->count);
               echo json_encode($status);
            } 
        }
    }
    /**
     * used to listing the wishlist items
     * @param  array $data
     */
    function wishlist_listing() {
        $data = array();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['wish_list'] = $this->ProductModel->wishlist_listing();
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['js'] = array('site/cart.js');
        $this->render('wishlist', $data);
    }

    /**
     * used to delete the wishlist items
     * return true or false
     */
    function deleteWishlist() {
        $id = $this->input->post('id');
        $result = $this->ProductModel->deleteWishlist($id);
        $value = $this->ProductModel->count_wishlist_item();
        if ($result) {
               $status = array('status' => TRUE,'quantity'=>$value->count);
               echo json_encode($status);
//               exit;
        } 
    }
    /**
     * used to display checkout
     * @param array $data
     */
    function display_checkout() {
        $data = array();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        if ($this->session->userdata('customer') == TRUE) {
            if ($this->cart->total_items() > '0') {
                $user_id = $this->session->userdata('customer')['id'];
                $data['header_info'] = $this->BannerModel->get_header_contact();
                $data['user_data'] = $this->ProductModel->checkoutListing($user_id);
                $data['countries'] = $this->UserModel->get_countries();
                $data['payment_mode'] = $this->ProductModel->paymentInfo_listing();
                $data['states'] = $this->UserModel->get_states();
                $data['js'] = array('site/cart.js');
                $this->render('checkout', $data);
            } else {
                redirect('site/product/display_cart');
            }
        } else {
            redirect('site/login/index');
        }
    }
    /**
     * to update the checkout 
     * @param array $data 
     */
    function update_checkout_address() {
        $data = array();
        $page = array();
        $order = array();
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('fname', 'First Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('address1', 'Address', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('city', 'City', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('phone', 'Phone Number', 'numeric|required|exact_length[10]');
        $this->form_validation->set_rules('pin', 'Pin Code', 'numeric|required');
        if ($this->form_validation->run()) {
            $data['company_name'] = $this->input->post('company_name');
            $data['firstname'] = $this->input->post('fname');
            $data['middle_name'] = $this->input->post('mname');
            $data['lastname'] = $this->input->post('lname');
            $data['address'] = $this->input->post('address1');
            $data['address1'] = $this->input->post('address2');
            $data['city'] = $this->input->post('city');
            $data['user_id'] = $this->session->userdata('customer')['id'];
            $data['state_id'] = $this->input->post('state');
            $data['country_id'] = $this->input->post('country');
            $data['mobile_number'] = $this->input->post('phone');
            $data['pin_code'] = $this->input->post('pin');
            $result = $this->ProductModel->edit($data);
            $page['user_id'] = $this->session->userdata('customer')['id'];
            $page['billing_address_id'] = $result;
            $page['shipping_address_id'] = $result;
            $page['shopping_note'] = $this->input->post('message');
            $page['payment_gateway_id'] = $this->input->post('payment_mode');
            if ($page['payment_gateway_id'] == 1) {
            $page['status'] = 'O';
            }
            if ($page['payment_gateway_id'] == 2) {
            $page['status'] = 'P';
            }
            $page['grand_total'] = $this->input->post('grand_total');
            $page['shipping_charges'] = $this->input->post('shopping_charges');         
            $page['coupon_id'] = $this->input->post('coupon_id_name');
            $last_id = $this->ProductModel->insert_to_checkout($page);    
            foreach ($this->cart->contents() as $items) {
                $order['product_id'] = $items['id'];
                $order['amount'] = $items['price'];
                $order['quantity'] = $items['qty'];
                $order['order_id'] = $last_id;
                $this->ProductModel->insert_into_order($order);
            }
            if ($page['payment_gateway_id'] == 2) {
                $information = array();
                $information = $this->ProductModel->get_information();
                $returnURL = base_url() . 'site/product/success'; //payment success url
                $cancelURL = base_url() . $information[4]->value; //payment cancel url
                $this->paypal_lib->add_field('business', $information[3]->value);
                $this->paypal_lib->add_field('return', $returnURL);
                $this->paypal_lib->add_field('cancel_return', $cancelURL);
//            $this->paypal_lib->add_field('notify_url', $notifyURL);
                $this->paypal_lib->add_field('custom', $last_id);
                $i = 1;
                $this->paypal_lib->add_field('cmd', '_cart');
                $this->paypal_lib->add_field('upload', '1');
                $this->paypal_lib->add_field('no_note', '0');
                foreach ($this->cart->contents() as $items) {
//                $this->paypal_lib->add_field('item_image_'.$i,  $i);
                    $this->paypal_lib->add_field('item_number_' . $i, $i);
                    $this->paypal_lib->add_field('item_name_' . $i, $items['name']);
                    $this->paypal_lib->add_field('quantity_' . $i, $items['qty']);
                    $this->paypal_lib->add_field('amount_' . $i, $items['price']);
                    $i++;
                }
                if ($page['grand_total'] < 500) {
                    $this->paypal_lib->add_field('shipping_1', 50);
                }
                $discount_percent = $this->input->post('discount');
//                echo  $discount_percent ;exit;
                $this->paypal_lib->add_field('discount_amount_cart', $discount_percent);
//        pr($this->paypal_lib->fields);
                $this->paypal_lib->paypal_auto_form();
            }
            if ($page['payment_gateway_id'] == 1) {
                $this->my_order($order['order_id']);
            }
        }else{
//                redirect('site/product/display_checkout');
                $user_id = $this->session->userdata('customer')['id'];
                $data['header_info'] = $this->BannerModel->get_header_contact();
                $data['user_data'] = $this->ProductModel->checkoutListing($user_id);
                $data['countries'] = $this->UserModel->get_countries();
                $data['payment_mode'] = $this->ProductModel->paymentInfo_listing();
                $data['states'] = $this->UserModel->get_states();
                $data['js'] = array('site/cart.js');
                $data['quantity'] = $this->ProductModel->count_wishlist_item();
                $this->render('checkout', $data);
        }
    }
    /**
     * get coupon information 
     * @param array $data
     */
    function get_coupon_info(){
        $data = array();
        $code = $this->input->post('code');
        $sub_total = $this->input->post('sub_total');
        $data = $this->ProductModel->get_coupon_info($code);
        $percent=$data->percent_off;
        $data->discount = ($sub_total * $percent * .01);
        $data->sub_total = $sub_total - ($data->discount);
        if ($data->sub_total > 500) {
            $data->shopping_cost = 0;
        } else {
            $data->sub_total = $data->sub_total + 50;
            $data->shopping_cost = 50;
        }
        if ($data) {
            echo json_encode($data);
        }
    }
    /**
     * cancel coupon info
     * @param array 
     */
    function cancel_coupon(){
      $data = array();
        $sub_total = $this->input->post('sub_total');
        $code = $this->input->post('code');
        $data = $this->ProductModel->get_coupon_info($code); 
        $data->sub_total = $sub_total;
//        echo   $data->sub_total;exit;
        $data->discount= 0;
        if ($data->sub_total > 500) {
            $data->shopping_cost = 0;
        } else {
            $data->sub_total = $data->sub_total + 50;
            $data->shopping_cost = 50;
        }
//        pr($data);
        if ($data) {
            echo json_encode($data);
        }
    }             
    function success() {
        $paypalInfo = $this->input->post();
//         pr($paypalInfo);
        $txn_id = $paypalInfo['txn_id'];
        $last_id = $paypalInfo['custom'];
        $status = 'O';
        $this->ProductModel->insert_transaction_id($txn_id, $last_id, $status);      
        $this->my_order($last_id);
    }
    /**
     * used to listing the order
     * @param int $last_id
     */
    function my_order($last_id) {   
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $data['order_information'] = $this->ProductModel->get_info($last_id);
            $data['product_information'] = $this->ProductModel->get_product_info($last_id);
            $product_information = $this->ProductModel->get_product_info($last_id);
            $this->send_email_to_customer($data,$last_id); 
            $title ='new_order_detail_admin';
            $email_template = $this->EmailModel->template($title);
            $email = $this->ProductModel->get_admin_email();
            $string[0]='Administrator';
            $string[1] = $this->load->view('product/orderEmail',$data,true);
            $replace[0]='{admin}';
            $replace[1] = '{orderdetails}';
            $body = str_replace($replace, $string, $email_template->content);
            $send_data = array(
                    'body' => $body, 
                    'to_email' => $email->value,
                    'subject'=> $email_template->subject,
                );
//            pr($send_data);
            $this->EmailModel->send_mail($send_data);
             $this->cart-> destroy();
            redirect(base_url().'site/invoice/'.$last_id); 
        } else {
            redirect('site/login/index');
        }
    }
     /**
      * 
      * @param array $data
      * @param integer $last_id
      * @return boolean
      */
    function send_email_to_customer($data,$last_id){
        $order_information = $this->ProductModel->get_info($last_id);
        $product_information = $this->ProductModel->get_product_info($last_id);
//          pr($product_information);
        $title ='new_order_detail';
        $email_template = $this->EmailModel->template($title);
//        pr( $email_template );
        $string[0]=$this->session->userdata('customer')['firstname'].' '.$this->session->userdata('customer')['lastname'];
        $string[1] = $this->load->view('product/orderEmail',$data,true); 
        $replace[0]='{username}';
        $replace[1] = '{orderdetails}';
        $body = str_replace($replace, $string, $email_template->content);
//         pr( $body );
         $send_data = array(
                'body' => $body,
                'subject'=> $email_template->subject,
                'to_email' => $this->session->userdata('customer')['email'],
            );
//         pr( $send_data);
        $this->EmailModel->send_mail($send_data);
        return true;
    }
    /**
     * used to display final order receipt
     * @param integer $last_id
     */
    function invoice($last_id = '0'){  
        $data = array();
        $data['order_information'] = $this->ProductModel->get_info($last_id);
        $data['product_information'] = $this->ProductModel->get_product_info($last_id);
        $data['product_details'] =  $this->ProductModel->get_product_quantity($last_id);
        $data['header_info'] = $this->BannerModel->get_header_contact(); 
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('orderReview', $data);
        $this->ProductModel->decreased_item_quantity($data['product_details']);
        if($data['order_information']->code!=''){
        $this->ProductModel->decreased_coupon_used($data['order_information']);
        $data = $this->ProductModel->get_coupon_info($data['order_information']->code);
        $this->ProductModel->insert_coupon_used($data);
        }            
    }
    /**
      * used to display the account 
      * @param array $data
      */
    function my_account_listing() {
        $data = array();
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['my_profile'] = $this->ProductModel->my_account();
        $this->render('my_account', $data);
    }
   /**
    * used to edit the account
    * @param array $data
    */
    function my_account_edit() {
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|alpha_numeric_spaces');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|alpha_numeric_spaces');
            if ($this->form_validation->run()) {               
                $data['firstname'] = $this->input->post('firstname');
                $data['lastname'] = $this->input->post('lastname');
                $data['email'] = $this->input->post('email');
                $this->ProductModel->edit_my_account($data);
                redirect('site/home/index');
            }
            $data['my_profile'] = $this->ProductModel->my_account();
            $data['header_info'] = $this->BannerModel->get_header_contact();
             $data['quantity'] = $this->ProductModel->count_wishlist_item();
            $this->render('my_account', $data);
        } else {
            redirect('site/login/index');
        }
    }
    /**
     * change the password 
     * @param array $data
     */
    function change_password() {
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric|min_length[8]');
            $this->form_validation->set_rules('change_password', 'Password', 'trim|required|alpha_numeric|min_length[8]');
            $this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|matches[change_password]');
            if ($this->form_validation->run()) {
                $data['password'] = $this->input->post('password');
                $data['new_password'] = $this->input->post('change_password');
                $data['new_password'] = $this->input->post('c_password');
                $this->ProductModel->change_password($data);
                $this->session->unset_userdata('customer');
                redirect('site/login/index');
            }
            $data['header_info'] = $this->BannerModel->get_header_contact();
             $data['quantity'] = $this->ProductModel->count_wishlist_item();
            $this->render('change_password', $data);
        } else {
            redirect('site/login/index');
        }
    }

    /**
     * listing my_order
     */
    function my_orders() {
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $data['order_info'] = $this->ProductModel->get_order_info();
            $data['header_info'] = $this->BannerModel->get_header_contact();
             $data['quantity'] = $this->ProductModel->count_wishlist_item();
            $this->render('my_orders', $data);
        } else {
            redirect('site/login/index');
        }
    }

    /**
     * to listing contact_us
     * @param array $data
     */
    function contact_us() {
        $data = array();
        $this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('contact', 'Contact No', 'required|numeric');
        $this->form_validation->set_rules('message', 'Message', 'required');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['contact_no'] = $this->input->post('contact');
            $data['message'] = $this->input->post('message');
            $data['created_by'] = $this->session->userdata('customer')['id'];         
            $this->ProductModel->contact_us($data);
            $title = 'contact_us_user';
            $email_template = $this->EmailModel->template($title);
                $string[0] = $data['name'];
                $string[1] = $data['contact_no'];
                $string[2] = $data['email'];
                $string[3] = $data['message'];
                $replace[0] = '{name}';
                $replace[1] = '{c.no}';
                $replace[2] = '{email}';
                $replace[3] = '{message}';
                $body = str_replace($replace, $string, $email_template->content);
                $email = $this->ProductModel->get_admin_email();
                $send_data = array(
                    'body' => $body,
                    'subject'=> $email_template->subject,
                    'to_email' => $email->value,
                );
            $result = $this->EmailModel->send_mail($send_data);
            redirect('site/home/index');
        }
         $data['header_info'] = $this->BannerModel->get_header_contact();
          $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('contact_us', $data);
    }
    /**
     * used to get sub category
     * return array $result
     */
    function get_sub_category() {
        $id = $this->input->post('id');
        $limit = '5';
        $result = $this->ProductModel->get_child_category($id, $limit);
        $product_html = '';
        foreach ($result as $pro){
                   $product_html .= '<span class="col-sm-3">
                                     <img class="img3" src='.base_url().'public/Image/'.$pro->image.' alt="No Image Found" />
                                                    <h2>$'.$pro->price.'</h2>
                                                    <a href='.base_url().'site/product/get_product/'.$pro->id.'><p>'.$pro->name.'</p></a>';
//                    pr($product_html);
                    if ($pro->quantity == 0) {   
                        $product_html .= '<button type="button"  class="btn btn-fefault textcolor label-danger">Out Of Stock</button>';    
//                        pr($product_html);
                                        } else {
                                            $product_html .='<button type="button" ';
                                            $product_html .='id="cartbutton'.$pro->id.'" '; 
                                            $product_html .='class="btn btn-fefault add-to-cart cart ';
//                                             pr($product_html);
                                            if (is_added($pro->id)) {
                                               $product_html .= 'disabled" ';
//                                                pr($product_html);
                                            }
                                             $product_html .= '"data-id="'.$pro->id.'';
                                             $product_html .= '"data-name="'.$pro->name.''; 
                                             $product_html .= '"data-image="'.$pro->image.'';
                                             $product_html .= '"data-price="'.$pro->price;
                                             $product_html .='">';
//                                             pr($product_html);
                                               if (is_added($pro->id)) {
                                                     $product_html .= '<i class="fa fa-shopping-cart"></i>
                                                    Added to cart';
                                                } else { 
                                                    $product_html .= '<i class="fa fa-shopping-cart"></i>
                                                    Add to cart';
                                                } 
                                             $product_html .= '</button>'; 
                                         } 
                                       $product_html .= '</span>';
                                         }   
        if ($result) {
            echo json_encode($product_html);
        }else{
            $result = "false";
            echo json_encode($result);
        }
    }
    /**
     * used to show data listing
     * @param integer $id
     */
    function view_order($id) {
        $data = array();
        $data['order_details'] = $this->ProductModel->get_order_details($id);
        $data['header_info'] = $this->BannerModel->get_header_contact();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('view_order', $data);
    }
    /**
     * used to track order
     */
    function track_order() {
        if ($this->session->userdata('customer') == TRUE) {
            $data = array();
            $data['js'] = array('site/cart.js');
            $data['header_info'] = $this->BannerModel->get_header_contact();
            $data['quantity'] = $this->ProductModel->count_wishlist_item();
            $this->render('track_order', $data);
        } else {
            redirect('site/login/index');
        }
    }
    /**
     * to display track order
     * return array $data
     */
    function track_order_display() {
        $data = array();
        $data['email'] = $this->input->post('email');
        $data['order_id'] = $this->input->post('order_id');
        $data['result'] = $this->ProductModel->track_order($data);
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        if ($data['result']->status) {
         
            switch ($data['result']->status) {
                case 'P':
                    $class_status = "c0";
                    break;
                case 'O':
                    $class_status = "c1";
                    break;
                case 'S':
                    $class_status = 'c2';
                    break;
                case 'B':$class_status='error';
                      break;               
                default:
                    $class_status = 'c3';
                    break;
            }
            echo json_encode($class_status);
        }
    }
}