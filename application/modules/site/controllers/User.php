<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class Admin_controller
 * @category UserModel
 * @link localhost/site/user
 */
class User extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('UserModel');
        $this->load->Model('BannerModel');
        $this->load->Model('ProductModel');
    }
    /**
     * used to listing the user address
     */
    function index() {
         $data = array();
         $data['address'] = $this->UserModel->listing();
         $data['header_info'] = $this->BannerModel->get_header_contact();
          $data['quantity'] = $this->ProductModel->count_wishlist_item();
         $this->render('index',$data);
    }  
    /**
     * used to add new address
     * @param array $data
     */
    function add(){
        $data = array();
        $this->form_validation->set_rules('cname', 'Company Name', 'required|trim');
        $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('city', 'City', 'alpha|required|trim');
        $this->form_validation->set_rules('address', 'Address', 'required|trim');
        $this->form_validation->set_rules('city', 'City', 'alpha|required|trim');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('mobile_number', 'Phone_number', 'numeric|required|trim');
        $this->form_validation->set_rules('pin', 'Pin Code', 'numeric|required|trim');
        if ($this->form_validation->run()) {
            $data['company_name'] = $this->input->post('cname');
            $data['firstname'] = $this->input->post('firstname');
            $data['middle_name'] = $this->input->post('middlename');
            $data['lastname'] = $this->input->post('lastname');
            $data['address'] = $this->input->post('address');
            $data['address1'] = $this->input->post('address1');
            $data['city'] = $this->input->post('city');
            $data['user_id']=$this->session->userdata('customer')['id'];
            $data['state_id'] = $this->input->post('state');
            $data['country_id'] = $this->input->post('country');
            $data['mobile_number'] = $this->input->post('mobile_number');
            $data['pin_code'] = $this->input->post('pin');
            $data['primary_address'] = $this->input->post('optionsAdd');  
            $this->UserModel->insert($data);
            redirect('site/user/index');
        }
        $data['countries']=$this->UserModel->get_countries();
        $data['states']=$this->UserModel->get_states();
        $data['header_info'] = $this->BannerModel->get_header_contact();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['js'] = array('site/cart.js');
        $this->render('add',$data);
    }
    /**
     * used to edit user address
     * @param integer $id
     */
      function edit($id){
        $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|alpha_numeric_spaces');
        $this->form_validation->set_rules('address', 'Address', 'required|trim');
        $this->form_validation->set_rules('city', 'City', 'alpha|required|trim');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('pin', 'Pin Code', 'numeric|required|trim');
        if ($this->form_validation->run()) {
            $data['company_name'] = $this->input->post('cname');
            $data['firstname'] = $this->input->post('firstname');
            $data['middle_name'] = $this->input->post('middlename');
            $data['lastname'] = $this->input->post('lastname');
            $data['address'] = $this->input->post('address');
            $data['address1'] = $this->input->post('address1');
            $data['city'] = $this->input->post('city');
            $data['user_id']=$this->session->userdata('customer')['id'];
            $data['state_id'] = $this->input->post('state');
            $data['country_id'] = $this->input->post('country');
            $data['mobile_number'] = $this->input->post('mobile_number');
            $data['pin_code'] = $this->input->post('pin');
            $data['primary_address'] = $this->input->post('optionsAdd');
            $this->UserModel->edit($data,$id);
            redirect('site/user/index');
        }
        if ($id) {
            $data['address'] = $this->UserModel->get_address($id);
//            pr($data);
        }
        $data['countries']=$this->UserModel->get_countries();
        $data['states']=$this->UserModel->get_states();
        $data['header_info'] = $this->BannerModel->get_header_contact();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['js'] = array('site/cart.js');
        $this->render('edit', $data);
    }
    /**
     * used to delete address
     * @param integer $id
     */
       function delete($id) {
        if ($id) {
            $this->UserModel->delete($id);
            redirect('site/user/index');
        }
       }
       /**
        * used to find specific states of specific country
        * @return array
        */
       function get_specific_state(){
           $country_id = $this->input->post('id');
//           echo $country_id;exit;
           $data = array();
           $data = $this->UserModel->get_specific_state($country_id);
           echo json_encode($data);
       }
}
