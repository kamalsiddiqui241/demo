<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category LoginModel
 */
class Login extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('BannerModel');
        $this->load->Model('LoginModel');
        $this->load->Model('EmailModel'); 
        $this->load->Model('ProductModel');
        $this->load->library('cart');
        $this->load->library('FacebookConnect');
    }

    /**
     * used to check password correct or not
     * @param array $result
     */
    function index() {
//        pr($this->session->all_userdata());
        $data = array();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric');
        if ($this->form_validation->run()) {
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $result = $this->LoginModel->login($data);
            if ($result) {
                $datasession = array(
                    'customer' => array(
                        'email' => $result->email,
                        'lastname' => $result->lastname,
                        'firstname' => $result->firstname,
                        'id' => $result->id,
                        'is_logged_in' => true
                ));
                $this->session->set_userdata($datasession);
                redirect('site/home/index');
            } else {
                $this->session->set_flashdata('message', 'Invalid email or password  ');
            }
        }
        $data['header_info'] = $this->BannerModel->get_header_contact();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('index', $data);
    }

    /**
     * used to register the new user
     * @param array $result
     */
    function signup() {
        $data = array();
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('email1', 'Email', 'trim|required|valid_email|is_unique[d_users.email]');
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|alpha_numeric|min_length[8]');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password1]');
        if ($this->form_validation->run()) {
            $data['firstname'] = $this->input->post('fname');
            $data['lastname'] = $this->input->post('lname');
            $data['email'] = $this->input->post('email1');
            $data['password'] = $this->input->post('password1');
            $data['password'] = $this->input->post('cpassword');
            $result = $this->LoginModel->insert_data($data);
            if ($result) {
                $title ='new_user_registration';
                $email_template = $this->EmailModel->template($title);
                $string[0] = $data['firstname'] . " " . $data['lastname'];
                $string[1] = $data['email'];
                $string[2] = $data['password'];
                $replace[0] = '{username}';
                $replace[1] = '{email}';
                $replace[2] = '{password}';
                $body = str_replace($replace, $string, $email_template->content);
                $send_data = array(
                    'body' => $body,
                    'subject'=> $email_template->subject,
                    'to_email' => $data['email'],
                );
                 $result = $this->EmailModel->send_mail($send_data);
                if ($result){  
                    $title ='new_user_registration_admin';
                    $email_template = $this->EmailModel->template($title);
                    $email = $this->ProductModel->get_admin_email();
                    $string[0] = $data['firstname'] . " " . $data['lastname'];
                    $string[1] = $data['email'];
                    $replace[0] = '{username}';
                    $replace[1] = '{email}';
                    $body = str_replace($replace, $string, $email_template->content);                  
                    $send_data = array(
                    'body' => $body,
                    'subject'=> $email_template->subject,
                    'to_email' => $email->value,
                   );
                 $result = $this->EmailModel->send_mail($send_data);
                    $this->session->set_flashdata('success', 'Successfully Registration');
                }
            }
            redirect('site/login/index');
        }
        $data['header_info'] = $this->BannerModel->get_header_contact();
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('index', $data);
    }
    /**
     * used to logout from the page 
     */
     public function fb_login(){
		// Include the facebook api php libraries
//        	echo include_once APPPATH."libraries/facebook_api/facebook.php";
//		
//		// Facebook API Configuration
//         pr($this->session->all_userdata());
//		
//		//Call Facebook API
		$facebook = new Facebook(array(
		  'appId'  => FACEBOOK_APP_ID,
		  'secret' => FACEBOOK_APP_SECRET
		));
		$fbuser = $facebook->getUser();
//                echo '================';
//                echo $fbuser;exit;
//		
        if ($fbuser) {
//            pr($fbuser);
                      $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture'); 
//                           // Preparing data for database insertion
//			$userData['oauth_provider'] = 'facebook';
			$userData['fb_token'] = $userProfile['id'];
                        $fb_token = $userProfile['id'];
                        $userData['firstname'] = $userProfile['first_name'];
                        $userData['lastname'] = $userProfile['last_name'];
                        $userData['email'] = $userProfile['email'];
                        $email = $userProfile['email'];
//			// Insert or update user data
                       $check_email = $this->LoginModel->checkUser($email);
//                       echo $userId;exit;
                        $this->load->helper('string', 6);
                        $new_password= random_string('alnum', 8);
                       if($check_email=='true'){
                             $this->LoginModel->insert_fb_token($email,$fb_token);
                            }else{                            
                                 $this->LoginModel->insert_data_fb($userData,$new_password); 
                                   $title = 'forget_password';
                                   $email_template =$this->EmailModel->template($title);
                                   $string[0] = $login_data->firstname." ". $login_data->lastname;
                                   $string[1] =  $new_password ;  
                                   $replace[0]    =   '{username}';
                                   $replace[1]    =   '{password}';
                 
                                  $body = str_replace($replace,$string,$email_template->content);
//                pr($body);
                               $send_data = array(
                                'body' => $body,
                                'subject'=> $email_template->subject,
                                'to_email' =>  $email,
                     
                               );
                               $result = $this->EmailModel->send_mail($send_data);
                            }
                           $login_data = $this->LoginModel->get_user_data($email);
//                           pr($login_data);
                               $datasession = array(
                                   'customer' => array(
                                    'email' => $email,
                                   'lastname' => $login_data->lastname,
                                   'firstname' => $login_data->firstname,
                                   'id' => $login_data->id,
                                   'is_logged_in' => true
                             ));
                             $this->session->set_userdata($datasession);
//                             pr($this->session->userdata);
                             redirect('site/home/index');
             } else {        
			$fbuser = '';
                        $data = $facebook->getLoginUrl(array('redirect_uri'=>REDIRECT_URL,'scope'=>FB_PERMISSION));
                        redirect ($data);
        } 
        }
        /**
         * used to login with google
         */
       public function google_login(){
//           pr($this->session->all_userdata());
        include_once APPPATH."libraries/google-api/Google_Client.php";
        include_once APPPATH."libraries/google-api/contrib/Google_Oauth2Service.php";  
        // Google Project API Credentials
        // Google Client Configuration
        $gClient = new Google_Client();
        $gClient->setApplicationName('E-Shoppers');
        $gClient->setClientId(CLIENT_ID);
        $gClient->setClientSecret(CLIENT_SECRET);
        $gClient->setRedirectUri(REDIRECT_URL_GOOGLE);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }

        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
//            pr($userProfile);
            // Preparing data for database insertion
            $userData['google_token'] = $userProfile['id'];
            $userData['firstname'] = $userProfile['given_name'];
            $userData['lastname'] = $userProfile['family_name'];
            $userData['email'] = $userProfile['email'];
            $google_token = $userProfile['id'];
            $email = $userProfile['email'];
            // Insert or update user data
                       $check_email = $this->LoginModel->checkUser($email);
                        $this->load->helper('string', 6);
                        $new_password= random_string('alnum', 8);
                    if($check_email=='true'){
                             $this->LoginModel->insert_google_token($email,$google_token);
                      }else{                            
                                  $this->LoginModel->insert_data_fb_google($userData,$new_password); 
                                   $title = 'forget_password';
                                   $email_template =$this->EmailModel->template($title);
                                   $string[0] = $login_data->firstname." ". $login_data->lastname;
                                   $string[1] =  $new_password ;  
                                   $replace[0]    =   '{username}';
                                   $replace[1]    =   '{password}';
                 
                                  $body = str_replace($replace,$string,$email_template->content);
//                pr($body);
                               $send_data = array(
                                'body' => $body,
                                'subject'=> $email_template->subject,
                                'to_email' =>  $email,
                     
                               );
                               $result = $this->EmailModel->send_mail($send_data);
                            }
                           $login_data = $this->LoginModel->get_user_data($email);
//                           pr($login_data);
                               $datasession = array(
                                   'customer' => array(
                                    'email' => $email,
                                   'lastname' => $login_data->lastname,
                                   'firstname' => $login_data->firstname,
                                   'id' => $login_data->id,
                                   'is_logged_in' => true
                             ));
                             $this->session->set_userdata($datasession);
//                             pr($this->session->all_userdata());
                             redirect('site/home/index');
        } else {
            $data = $gClient->createAuthUrl();
            redirect($data);
        }
    }  
	/**
         * used to logout the user
         */
    function logout() {       
        $this->session->unset_userdata('customer');
        $facebook = new Facebook(array(
		  'appId'  => FACEBOOK_APP_ID,
		  'secret' => FACEBOOK_APP_SECRET	
		));
          if($facebook->getUser()){
             $facebook->destroySession();
             $facebook->setAccessToken('');    
        }
        $this->session->unset_userdata('token');
        redirect('site/home/index');
    }
}
