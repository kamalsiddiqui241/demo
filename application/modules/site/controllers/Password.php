<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class Admin_controller
 * @category PasswordModel
 * @link localhost/site/login
 */
class Password extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('PasswordModel');
        $this->load->Model('EmailModel');
        $this->load->Model('BannerModel');
        $this->load->Model('ProductModel');
    }
    /*
     * used to check email is valid or not 
     * @param text $email 
     */
    function index(){
        $data = array();
        $this->form_validation->set_rules('email','Existing Email','required|trim|valid_email');
        if($this->form_validation->run()){
            $email = $this->input->post('email'); 
            $result = $this->PasswordModel->get($email);
            if($result){
                 $title = 'forget_password';
                 $this->load->helper('string', 6);
                 $new_password= random_string('alnum', 8);
                 $this->PasswordModel->set($new_password,$email);
//                 $this->EmailModel->send_mail($new_password,$result,$title);
                 $email_template =$this->EmailModel->template($title);
                 $string[0] = $result->firstname." ". $result->lastname;
                 $string[1] =  $new_password ;  
                 $replace[0]    =   '{username}';
                 $replace[1]    =   '{password}';
                 
                 $body = str_replace($replace,$string,$email_template->content);
//                pr($body);
                 $send_data = array(
                     'body' => $body,
                     'to_email' =>  $result->email,
                     'subject'=> $email_template->subject,
                 );
                 $result = $this->EmailModel->send_mail($send_data);
                 if($result){
                   $this->session->set_flashdata('success', 'New Password send to ur email address');
                   redirect('site/password/index');  
                 }
            }else{
                $this->session->set_flashdata('message', 'email is not valid');
            }  
        }
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $data['js']= array('site/cart.js');
        $this->render('index',$data);
    }
}
