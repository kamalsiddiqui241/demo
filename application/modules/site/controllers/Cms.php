<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Super class Admin_controller
 * @category CmsModel
 * @link localhost/site/cms
 */
class Cms extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('CmsModel');
        $this->load->Model('BannerModel');
        $this->load->Model('ProductModel');
    }
/**
     * used to listing terms 
     * @param text $page_name
     * @param array $data
     */
    function index($page_name){
        $data = array(); 
        $data['header_info'] = $this->BannerModel->get_header_contact();
        $data['page_name']= $this->CmsModel->listing_cms($page_name);
         $data['quantity'] = $this->ProductModel->count_wishlist_item();
        $this->render('index',$data);
    }
}