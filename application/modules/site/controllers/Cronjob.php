<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Super class Admin_controller
 * @category LoginModel
 */
class Cronjob extends Site_Controller {
    function __construct() {
        parent::__construct();
        $this->load->Model('EmailModel');
        $this->load->Model('ProductModel');
    }
    /**
     * used to listing the per day product order by user
     * @param array $send_data
     */
    function index(){       
            $data['order_information']= $this->ProductModel->product_per_day();
            $title ='new_order_detail_admin';
            $email_template = $this->EmailModel->template($title);
            $email = $this->ProductModel->get_admin_email();
            $string[0]='Administrator';
            $string[1] = $this->load->view('cronjob/index',$data,true); 
            $replace[0]='{admin}';
            $replace[1] = '{orderdetails}';
            $body = str_replace($replace, $string, $email_template->content);
            $send_data = array(
                    'body' => $body,
                    'to_email' => $email->value,
                    'subject'=>'Order Per day',
                );
//            pr($send_data);
            $this->EmailModel->send_mail($send_data);
    }
    /**
     * used to display the complete wishlist
     * @param array $send_data 
     */
    function display_wishlist(){
            $data['wish_list']= $this->ProductModel->complete_wish_list();
            $title ='user_wish_list';
            $email_template = $this->EmailModel->template($title);
            $email = $this->ProductModel->get_admin_email();
            $string[0]='Administrator';
            $string[1] = $this->load->view('cronjob/wishlist',$data,true); 
            $replace[0]='{admin}';
            $replace[1] = '{orderdetails}';
            $body = str_replace($replace, $string, $email_template->content);
            $send_data = array(
                    'body' => $body,
                    'to_email' => $email->value,
                    'subject'=>$email_template->subject,
                );
//            pr($send_data);
            $this->EmailModel->send_mail($send_data);
            }
}