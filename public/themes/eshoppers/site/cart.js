function add_to_cart(){
    $('.cart').on('click',function (e) {
        var id = $(this).data('id');
        var image = $(this).data('image');
        var price = $(this).data('price');
        var name = $(this).data('name');
        var quantity = $("#cartQuantity" + id).val();
//        console.log(id+'**'+image+'--'+price+'++'+name+'=='+quantity);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: add_cart_url,
            data: {'price': price, 'image': image, 'name': name, ' quantity': quantity, 'id': id},
            success: function (response) {
                if (response.status) {
                    $("#cartbutton" + id).addClass("disabled");
                    $("#cartbutton" + id).html("<i class='fa fa-shopping-cart'></i>Added to cart");
                    $("#cartbutton1" + id).addClass("disabled");
                    $("#cartbutton1" + id).html("<i class='fa fa-shopping-cart'></i>Added to cart");
                    $("#cartbutton2" + id).addClass("disabled");
                    $("#cartbutton2" + id).html("<i class='fa fa-shopping-cart'></i>Added to cart");
                    $('.display_cart').html('<i class="fa fa-shopping-cart"></i>Cart(' + response.cart_count + ')');
                    $('.wishlist_listing').html('<i class="fa fa-star"></i> Wishlist('+response.quantity+')</a></li>');
                    $("#cartbutton_wishlist" + id).closest('tr').fadeOut("fast");
                      $.growl({
	                   title: "",
	                   message: "Product added to cart" });
                     if(response.quantity== 0){
                     $('.empty_list').html('<div id="content" class="col-sm-12 empty_list"><p class="text_color">Your WishList is empty!</p></div>');
                     $(".display_list").hide();
                    }
                }
            }
        });

    });
}
$(document).ready(function () {
    add_to_cart();
    $('.cart_quantity_up').click(function () {
        var id = $(this).data('id');
        var sub_total = $('#sub_total').data('sub_total');
        var price = $(this).data('price');
        var quantity = $("#quant" + id).val();
        quantity = parseInt(quantity) + 1;
        var rowid = $(this).data('rowid');   
        var total_price = parseFloat(parseInt(quantity) * parseFloat(price));
        sub_total = parseFloat(sub_total + price);
        update_cart(rowid, quantity, id, total_price, sub_total);    
    });
    $('.cart_quantity_down').click(function () {
        var id = $(this).data('id');
        var sub_total = $('#sub_total').data('sub_total');
        var price = $(this).data('price');
        var quantity = $("#quant" + id).val();
        if (quantity > 0) {
            quantity = parseInt(quantity) - 1;
        }
        var rowid = $(this).data('rowid');
        if(quantity>0){
        var total_price = parseFloat(parseInt(quantity) * parseFloat(price));
        sub_total = parseFloat(sub_total - price);
        update_cart(rowid, quantity, id, total_price, sub_total);
       }
    });
    function update_cart(rowid, quantity, id, total_price, sub_total) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: cart_update_url,
            data: {'quantity': quantity,'id': id, 'rowid': rowid},
            success: function (res) {
                if (res.status == true) {
                    $("#quant" + id).val(quantity);
                    $("#totalprice" + id).text(total_price);
                    $("#sub_total").text(sub_total);
                    $("#sub_total").attr('data-sub_total', sub_total);
                    $("#sub_total").data('sub_total', sub_total);
                    $("#sub_total1").text(sub_total);
                     $('.cart_quantity_up').show();
                     $('.display_cart').html('<i class="fa fa-shopping-cart"></i>Cart(' + res.cart_count + ')');
                }else{
                    alert('Quantity is more than available quantity');   
                }
            }
        });
    }
    $('.cart_quantity_delete').click(function () {
        var rowid = $(this).data('rowid');
        var sub_total=$('#sub_total').data('sub_total');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: cart_delete_url,
            data: {'rowid': rowid,'sub_total':sub_total},
            success: function (res) {
                if (res.status == true) {
                    $("#delete" + rowid).closest('tr').fadeOut("fast");
                    $("#sub_total").text(res.sub_total);
                    $("#sub_total").attr('data-sub_total', res.sub_total);
                    $("#sub_total").data('sub_total', res.sub_total);
                    $("#sub_total1").text(res.sub_total);
                    $('.display_cart').html('<i class="fa fa-shopping-cart"></i>Cart(' + res.cart_count + ')');
                }
               if(res.cart_count==0){
                     $('.display_shopping_cart').html('<div id="content" class="col-sm-12 display_shopping_cart"><p class="text_color">Your shopping cart is empty!</p></div>');
                     $('.cart_info').hide();
                     $('.display_cart').html('<i class="fa fa-shopping-cart"></i>Cart(' + res.cart_count + ')');
                 }
            }
        })
    });
    $('.wish').click(function () {
        var product_id = $(this).data('id');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: add_wish_url,
            data: {'product_id': product_id},
            success: function (res) {
                if (res.status == true) {
//                    console.log(res.quantity);
                    $("#wish" + product_id).addClass("disabled");
                    $('.wishlist_listing').html('<i class="fa fa-star"></i> Wishlist('+res.quantity+')</a></li>');
                    $.growl.notice({
	                   title: "",
	                   message: "Item has been added to wishlist" });
                }
            }
        })
    });
    $('.wish_quantity_delete').click(function () {
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: delete_wish_url,
            data: {'id': id},
            success: function (res) {
                if (res.status == true) {
                    $("#delete" + id).closest('tr').fadeOut("fast");
                    $('.wishlist_listing').html('<i class="fa fa-star"></i> Wishlist('+res.quantity+')</a></li>');
                    if(res.quantity== 0){
                     $('.empty_list').html('<div id="content" class="col-sm-12 empty_list"><p class="text_color">Your WishList is empty!</p></div>');
                     $(".display_list").hide();
                    }
                }
            }
        })
    });
    $('.coupon_success').hide();
    $('.coupon_cancel').hide();
    $('.coupon').click(function () {
        var code = $("#coupon_code").val();
        var sub_total = $('#sub_total').data('sub_total');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: cart_coupon_url,
            data: {'code': code, 'sub_total': sub_total},
            success: function (res) {
                $("#sub_total1").text(res.sub_total);
                $("#coupon_discount").text(res.discount);
                $("#coupon_id").val(res.id);
                $("#discount").val(res.discount);
                $("#sub_total2").val(res.sub_total);
                $('.coupon').hide();
                $('.coupon_success').show();
                $('.coupon_cancel').show();
            },
            error: function (res) {
                $("#error").empty().text('Coupon Code is invalid');
            }
        });

    });
     $('.coupon_cancel').click(function () {
        var sub_total = $('#sub_total').data('sub_total');
//        console.log(sub_total);
         var code = $("#coupon_code").val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: cart_coupon_cancel_url,
            data: {'code': code, 'sub_total': sub_total},
            success: function (res) {
//                console.log(res);
                $("#sub_total1").text(res.sub_total);
                $("#coupon_discount").text(res.discount);
                $("#coupon_id").val(res.id);
                $("#discount").val(res.discount);
                $("#sub_total2").val(res.sub_total);
                $('.coupon_cancel').hide();
                $('.coupon').show();
                $('.coupon_success').hide();
            },
            error: function (res) {
                $("#error").empty().text('Coupon Code is invalid');
            }
        });
    });
    $('.error_cat').hide();
    $('.sub_category').click(function () {
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: category_url,
            data: {'id': id},
            success: function (res) {
//                console.log(res);
                if (res == "false") {
                    $(".error_cat").show();
                    $(".sub_cat").hide();
                } else {
                    $(".error_cat").hide();
                    $(".sub_cat").show();   
                    $(".sub_cat ").html(res);
                    add_to_cart();      
                }
            }
        });
    });
    $('.track_display').hide();
    $('.track').click(function () {
        var order_id = $('#order_id').val();
        var email = $('#email').val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: track_url,
            data: {'email': email, 'order_id': order_id},
            success: function (res) {
                if (res == 'error') {
                    $('.error_order_id').html('Order Id or email is invalid');
                    $('.track_display').hide();
                } else {
                    $('.track_display').show();
                    $(".status_value").addClass(res);
                    $('.error_order_id').hide();
                }
            }
        });

    });
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    });
    $('.first_time_state').hide();
    $('#input-country').on('change', function() {
        var id = $('#input-country').val();
        var state_html = "";
//        console.log(id);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: state_url,
            data: { 'id': id},
            success: function (res) {
//                console.log(res);
                 $('.first_time_state').show();
               $.each(res, function (key, value){
             state_html += "<option value='"+value.id+"'>"+value.name+"</option>";
            });
             $(".state_name").html(state_html);
            }
        });

    }); 
});