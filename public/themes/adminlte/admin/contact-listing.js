$('textarea').ckeditor();
var table = $('#listing').DataTable({
    "paging": true,
    "ordering": true,
    "info": true,
    "lengthMenu": [[2, 5, 10, 25, -1], [2, 5, 10, 25, "All"]],
    "pageLength": 5,
    "order": [[1, "asc"]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": datatable_url,
        "type": "POST"
    },
    "columnDefs": [
        {"orderable": false, "targets": [0]},
        {"orderable": true, "targets": [1]},
        {"orderable":false,"targets": [2]},
        {"orderable":false,"targets": [3]},
        {"orderable":false,"targets": [4]},
        {"orderable":false,"targets": [5]},
    ],
    "columns": [
        {"data": "id"},
        {"data": "name"},
        {"data":"email"},
        {"data":"contact"},
        {"data":"message"},
        {"data":"id"},
    ],
    "rowCallback": function (row, data,index) {    
        row_num = index+1;
        $('td:eq(0)', row).html(
            '<span>'+row_num+'</span>'
    )
            $('td:last-of-type', row).html(
                '<a id="id_value" class="btn btn-xs btn-success contact-response" data-request-id="' + data['id'] + '" href="#" >Response</a>'
                );
    },
});
