var table = $('#listing').DataTable({
    "paging": true,
    "ordering": true,
    "info": true,
    "lengthMenu": [[2, 5, 10, 25, -1], [2, 5, 10, 25, "All"]],
    "pageLength": 5,
    "order": [[1, "asc"]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": datatable_url,
        "type": "POST"
    },
    "columnDefs": [
        {"orderable": false, "targets": [0]},
        {"orderable": true, "targets": [1]},
        {"orderable":false,"targets": [2]},
        {"orderable":false,"targets": [3]},
        {"orderable":false,"targets": [4]},
        {"orderable":false,"targets": [5]},
    ],
    "columns": [
        {"data": "order_id"},
        {"data": "date"},
        {"data":"status"},
        {"data":"payment_gateway"},
        {"data":"shipping_charges"},
        {"data":"id"},
    ],
    "rowCallback": function (row, data,index) {    
        //View/Edit/Delete Link
        $('td:last-of-type', row).html(
            '<a class="btn btn-xs btn-success" href="'+datatable_update_url+'/' + data['id'] + '">Edit Status</a>'+   
            '  <a class="btn btn-xs btn-success" href="'+datatable_view_url+'/' + data['id'] + '">View</a>' 
        );
    },
});