var table = $('#listing').DataTable({
    "paging": true,
    "ordering": true,
    "info": true,
    "lengthMenu": [[2, 5, 10, 25, -1], [2, 5, 10, 25, "All"]],
    "pageLength": 5,
    "order": [[1, "asc"]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": datatable_url,
        "type": "POST"
    },
    "columnDefs": [
        {"orderable": false, "targets": [0]},
        {"orderable": true, "targets": [1]},
        {"orderable":false,"targets": [2]},
        {"orderable":false,"targets": [3]},
         {"orderable":false,"targets": [4]},
    ],
    "columns": [
        {"data": "id"},
        {"data": "name"},
        {"data":"status"},
        {"data":"image"},
        {"data":"id"},
    ],
    "rowCallback": function (row, data,index) {    
        row_num = index+1;
        $('td:eq(0)', row).html(
            '<span>'+row_num+'</span>'
        );
        $('td:eq(3)',row).html(
                     '<img alt="Image Not Found" src="'+datatable_image_url+'/'+data['image']+'"  width="90" height="70">'
                )
        //View/Edit/Delete Link
        $('td:last-of-type', row).html(
            '<a class="btn btn-xs btn-success" href="'+datatable_edit_url+'/' + data['id'] + '">Edit</a>' +
            '  <a class="btn btn-xs btn-danger delete-category" onclick="return doconfirm()" href="'+datatable_delete_url+'/' + data['id'] + '">Delete</a>'
        );
    },
});
function doconfirm()
{
    var job = confirm("Are you sure to delete permanently?");

    if (job != true)
    {
        return false;
    }
}