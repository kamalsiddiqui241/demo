
$(document).ready(function () {
    $("#myForm").validate({
        onkeyup: false,
        onfocusout: function (element) { $(element).valid(); },
        errorElement:'span',
        wrapper:'span',
        errorPlacement: function (error, element) {
            if($(element).attr('id') == 'male') {
                $('#gender-help-block').empty();
                error.appendTo($('#gender-help-block'));
            } else if($(element).attr('id') == 'interest_biking') {
                $('#interest-help-block').empty();
                error.appendTo($('#interest-help-block'));
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name:{
                required: true,
            },
            fname: {
                required: true,
            },
            firstname: {
                required: true,
            },
            lname: {
                required: true,
            },
            cname: {
                required: true,
            },
           lastname: {
                required: true,
            },
            subject: {
                required: true,
            },
            address: {
                required: true,
            },
            title:{
                required:true,
            },
            content:{
                required: true,  
            },
            pname: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
           email1: {
             required: true,
             email: true
            },
            password: {
                required: true,
                alphanumeric: true

            },
           password1: {
                required: true,
                alphanumeric: true

            },
            co_password: {
                required: true,
                equalTo: "#inputPassword"
            },
            change_password: {
                required: true,
                alphanumeric: true

            },
            c_password: {
                required: true,
                equalTo: "#inputPassword"
            },
             cpassword: {
                required: true,
                equalTo: "#ValueCpassword"
            },
            respond:{
                 required: true,
            },
            role: {
                required: true,
            },
            price: {
                required: true, 
                number: true,
            },
            quantity: {
                required: true, 
                number: true,
            },
          parent: {
                required: {
                depends: function(element) {
                    return $("#residence").val() == '';
                }
            }
           },
            state: {
                required: {
                depends: function(element) {
                    return $("#input-zone").val() == '';
                }
            }
           },
            country: {
                required: {
                depends: function(element) {
                    return $("#input-country").val() == '';
                }
            }
           },
           contact: {
                required: true,
                number: true,
                exactlength: 10,
            },
           mobile_number: {
                required: true,
                number: true,
                exactlength: 10,
            },
            fValue: {
                required: true,
            },
            code: {
                required: true,
            },
            percent_off: {
                required: true,
                 number: true,
            },
           myimage: {
                             required: true,
                             extension: "jpg,png,jpeg,gif",
                            },
            uses: {
                required: true,
                 number: true,
            },
            description: {
                required: true,
            },
            message: {
                required: true,
            },
             city: {
                required: true,
            },
            pin: {
                required: true,
            },
        },
        messages: {
            name: "Please enter name",
            fname: "Please enter your firstname",
            lname: "Please enter your lastname",
            firstname: "Please enter your firstname",
            cname: "Please enter your lastname",
            lastname: "Please enter your lastname",
            content: "Please Enter Content",
            pname:'Please enter Page Name',
            address:'Please enter address',
            city:'Please Enter city',
            country:'Please select country',
            state:'Please select State',
            subject: {
                required: "Please enter Subject",

            },
            office: {
                required: "Please provide office number",
            },
            message:"Please enter message field",
            email: "Please enter a valid email address",
            email1: "Please enter a valid email address",
            password:"Please enter Password and length Should minimum 8 alpha numeric character",
            password1:"Please enter Password and length Should minimum 8 alpha numeric character",
            cpassword: {
                required: "Confirm Password is same as Password",
                equalTo: "Please enter the same password as above"
            },
             cpassword: {
                required: "Confirm Password is same as Password",
                equalTo: "Please enter the same password as above"
            },
            contact: {
                required: "Please enter Phone Number",
                minlength: "Your Phone Number must consist 10  digits",
                maxlength: "Your Phone Number must consist 10  digits"

            },
             mobile_number: {
                required: "Please enter Phone Number",
                minlength: "Your Phone Number must consist 10  digits",
                maxlength: "Your Phone Number must consist 10  digits"

            },
            role:"Role is required",
            gender: "Gender must be selected",
            percent_off: "Please enter percent in decimal",
            fValue: "Please enter this field",
            code: "Please Enter Alpha Numeric value",
            price:"Please enter Price in decimal",
            quantity:"Please enter quantity",
            parent: 'Please select a  Category',
            uses:"Please Enter number of uses",
            title:"Please Enter Title",
           description : "must be enter  Description"
        },
    });
      $("#myForm1").validate({
           onkeyup: false,
        onfocusout: function (element) { $(element).valid(); },
        errorElement:'span',
        wrapper:'span',
        errorPlacement: function (error, element) {
            if($(element).attr('id') == 'male') {
                $('#gender-help-block').empty();
                error.appendTo($('#gender-help-block'));
            } else if($(element).attr('id') == 'interest_biking') {
                $('#interest-help-block').empty();
                error.appendTo($('#interest-help-block'));
            } else {
                error.insertAfter(element);
            }
        },
          rules:{
            email: {
             required: true,
             email: true
            },
            password: {
                required: true,
                alphanumeric: true

            }, 
          },
          messages:{
               email: "Please enter a valid email address",
               password:"Please enter Password and length Should minimum 8 alpha numeric character",
          },
      });
});
